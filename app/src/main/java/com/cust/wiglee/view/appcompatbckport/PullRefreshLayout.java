package com.cust.wiglee.view.appcompatbckport;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

public class PullRefreshLayout extends SwipeRefreshLayoutLinear {
	private ListView mListView;
	public PullRefreshLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	public PullRefreshLayout(Context context) {
		super(context);
	}
	public void setRefreshListView(ListView listView) {
		mListView = listView;
	}
	@Override
	public boolean canChildScrollUp() {
		boolean canScrollUp = true;
        if(mListView != null)
		if(mListView.getChildAt(0) != null){
				if(mListView.getFirstVisiblePosition() ==0){		
					if((mListView.getChildAt(0).getTop()) == 0)	
						canScrollUp = false;
					else
						canScrollUp = true;
				}	
		}
		return canScrollUp;
	}	
}
