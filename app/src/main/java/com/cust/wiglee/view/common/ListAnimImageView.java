package com.cust.wiglee.view.common;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import com.cust.wiglee.listner.ClickTouchListner;
import com.facebook.rebound.Spring;
import com.facebook.rebound.SpringConfig;
import com.facebook.rebound.SpringListener;
import com.facebook.rebound.SpringSystem;

public class ListAnimImageView extends ImageView implements View.OnTouchListener, SpringListener{
	 	private static double TENSION = 400;
	    private static double DAMPER = 30; //friction
	    private SpringSystem mSpringSystem;
	    private Spring mSpring;
	    protected ClickTouchListner callback;
	    public void setOnClickTouchListener(ClickTouchListner listner) {	       
	        callback = listner;
	    }
	    

	public ListAnimImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		 mSpringSystem = SpringSystem.create();			
	     mSpring = mSpringSystem.createSpring();
	     mSpring.addListener(this);	
	     SpringConfig config = new SpringConfig(TENSION, DAMPER);
	     mSpring.setSpringConfig(config);	
	}
   @Override
   public boolean onTouch(View v, MotionEvent event) {
       switch (event.getAction()) {
           case MotionEvent.ACTION_DOWN:
               mSpring.setEndValue(1f);
               onActionDown();
               return true;
           case MotionEvent.ACTION_UP:
               mSpring.setEndValue(0f);
               onCallBack();
               return true;
       }
      return false;
   }
   
   private void onActionDown() {	   
	   if(mSpring.getEndValue() == 1){
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                  @Override
                  public void run() {
                	  mSpring.setEndValue(0f);
                  }
                }, 350);
	   }
}

public void onCallBack() {
   	callback.onTouchComplete();
   }   
   
   @Override
   public void onSpringUpdate(Spring spring) {
       float value = (float) spring.getCurrentValue();
       //float scale = 1f - (value * 0.5f);
       float scale = 1f - (value * 0.6f);
       this.setScaleX(scale);
       this.setScaleY(scale);
       this.invalidate();
   }

   @Override
   public void onSpringAtRest(Spring spring) {   	
   }

   @Override
   public void onSpringActivate(Spring spring) {
   }

   @Override
   public void onSpringEndStateChange(Spring spring) {
   }
}
