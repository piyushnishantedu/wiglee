package com.cust.wiglee.view.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;
import com.cust.wiglee.R;

public class FontTextView extends TextView {
    public FontTextView(Context context) {
        super(context);
    }

    public FontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            init(context, attrs);
        }
    }

    public FontTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if (!isInEditMode()) {
            init(context, attrs);
        }
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes( attrs, R.styleable.FontTextView, 0, 0);
        try {
            String fontName = getFontName(a.getInteger(R.styleable.FontTextView_textFont, 0));
            if (!fontName.equals("")) {
                try {
                    setTypeface(Typeface.createFromAsset(context.getAssets(), fontName));
                } catch (Exception e) {
                    Log.e("CustomFontTextView", e.getMessage());
                }
            }
        } finally {
            a.recycle();
        }
    }

    private String getFontName(int index) {
        switch (index) {                
            case 0 :
                return "Roboto-Bold.ttf";  
            case 1 :
                return "Roboto-Light.ttf";    
            case 2 :
                return "Roboto-Medium.ttf";
            case 3 :
                return "Roboto-Regular.ttf";
            default:
                return "";
        }
    }
}
