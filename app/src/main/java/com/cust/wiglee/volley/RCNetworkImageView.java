package com.cust.wiglee.volley;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import com.android.volley.toolbox.NetworkImageView;
import com.cust.wiglee.R;

public class RCNetworkImageView extends NetworkImageView {
	/** The m radius. */
	private int mRadius;
	/** The is circular. */
	private boolean isCircular;
	/** The drawable. */
	private StreamDrawable mDrawable;
	/** The m margin. */
	private int mMargin;
	/** The is shadowed. */
	private boolean isShadowed;
	private Paint mCirclePaint;
	private boolean drawCircle;
	private int mCircleColor;

	/**
	 * Instantiates a new rounded corner network image view.<br/>
	 * Initializes <code>RCNetworkImageView</code> with default radius of 4px, margin 0px and with no shadowing.
	 *
	 * @param context the context
	 */
	public RCNetworkImageView(Context context) {
		super(context);
		mRadius = 4;
		mMargin = 0;
		isCircular = false;
		isShadowed = false;
		drawCircle = false;
		mCircleColor = Color.WHITE;
		setWillNotDraw(false);
		mCirclePaint = new Paint();
		mCirclePaint.setStrokeWidth(4f);
		mCirclePaint.setAntiAlias(true);
		mCirclePaint.setColor(mCircleColor);
		mCirclePaint.setStyle(Style.STROKE);
		if(isCircular)
			isShadowed = false;
	}

	/**
	 * Instantiates a new rounded corner network image view.
	 * Initializes <code>RCNetworkImageView</code> with radius provided in <code>radius</code> attribute in <code>XML</code> or default 4px,<br/> margin with <code>radius</code> attribute in <code>XML</code> default 0px,<br/>
	 * if <code>isShadowing</code> attribute is true shadows will be drawn on image <br/>and if <code>isCircular</code> attribute is true radius will be ignored and a circular image will be drawn.
	 *
	 * @param context the context
	 * @param attribs the AttributeSet
	 */
	public RCNetworkImageView(Context context, AttributeSet attribs) {
		super(context, attribs);
		TypedArray a=getContext().obtainStyledAttributes(
				attribs,
				R.styleable.RCNetworkImageView);
		mRadius = a.getInt(
				R.styleable.RCNetworkImageView_radius,4);
		mMargin = a.getInt(
				R.styleable.RCNetworkImageView_margin,0);
		isShadowed = a.getBoolean(R.styleable.RCNetworkImageView_isShadowPresent,false);
		isCircular = a.getBoolean(R.styleable.RCNetworkImageView_isCircular, false);
		drawCircle = a.getBoolean(R.styleable.RCNetworkImageView_drawCircle, false);
		mCircleColor = a.getColor(R.styleable.RCNetworkImageView_circleColor, Color.WHITE);
		a.recycle();
		if(isCircular)
			isShadowed = false;
		setWillNotDraw(false);
		mCirclePaint = new Paint();
		mCirclePaint.setStrokeWidth(4f);
		mCirclePaint.setAntiAlias(true);
		mCirclePaint.setColor(mCircleColor);
		mCirclePaint.setStyle(Style.STROKE);
	}

	/**
	 * Instantiates a new rounded corner network image view.
	 * Initializes <code>RCNetworkImageView</code> with radius provided in <code>radius</code> attribute in <code>XML</code> or default 4px,<br/> margin with <code>radius</code> attribute in <code>XML</code> default 0px,<br/>
	 * if <code>isShadowing</code> attribute is true shadows will be drawn on image, <br/>if <code>isCircular</code> attribute is true radius will be ignored and a circular image will be drawn.<br/>
	 * and with defStyle provided.
	 * @param context the context
	 * @param attribs the AttributeSet
	 * @param defStyle the defStyle
	 */
	public RCNetworkImageView(Context context, AttributeSet attribs, int defStyle) {
		super(context, attribs, defStyle);
		TypedArray a=getContext().obtainStyledAttributes(
				attribs,
				R.styleable.RCNetworkImageView);
		mRadius = a.getInt(
				R.styleable.RCNetworkImageView_radius,4);
		mMargin = a.getInt(
				R.styleable.RCNetworkImageView_margin,0);
		isCircular = a.getBoolean(R.styleable.RCNetworkImageView_isCircular, false);
		isShadowed = a.getBoolean(R.styleable.RCNetworkImageView_isShadowPresent,false);
		drawCircle = a.getBoolean(R.styleable.RCNetworkImageView_drawCircle, false);
		mCircleColor = a.getColor(R.styleable.RCNetworkImageView_circleColor, Color.WHITE);
		a.recycle();
		setWillNotDraw(false);
		mCirclePaint = new Paint();
		mCirclePaint.setStrokeWidth(4f);
		mCirclePaint.setAntiAlias(true);
		mCirclePaint.setColor(mCircleColor);
		mCirclePaint.setStyle(Style.STROKE);
		if(isCircular)
			isShadowed = false;
	}

	/**
	 * Sets the radius.
	 *
	 * @param radius the new radius
	 */
	public void setRadius(int radius){
		mRadius = radius;
	}

	/**
	 * Sets the isCircular.
	 *
	 * @param isCircular true for circular ImageView
	 */
	public void setCircular(boolean isCircular){
		this.isCircular = isCircular;
	}

	/**
	 * Gets the radius.
	 *
	 * @return the radius
	 */
	public int getRadius() {
		return mRadius;
	}

	/**
	 * Checks if is circular.
	 *
	 * @return true, if is circular
	 */
	public boolean isCircular() {
		return isCircular;
	}

	/**
	 * Gets the margin.
	 *
	 * @return the m margin
	 */
	public int getmMargin() {
		return mMargin;
	}

	/**
	 * Sets the margin.
	 *
	 * @param margin the new margin
	 */
	public void setmMargin(int margin) {
		this.mMargin = margin;
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if(isCircular && drawCircle)
			canvas.drawCircle((this.getWidth()*1.0f)/2, (this.getHeight()*1.0f)/2, (this.getHeight()*1.0f)/2-1.8f, mCirclePaint);
	}

	/* (non-Javadoc)
	 * @see android.widget.ImageView#setImageBitmap(android.graphics.Bitmap)
	 */
	@Override
	public void setImageBitmap(Bitmap bm) {
		if (bm != null) {
			if(isCircular){
				mRadius = (int) Math.max(this.getWidth()/2, this.getHeight()/2);
			}
			mDrawable = new StreamDrawable(bm, mRadius, mMargin,isShadowed,this.getScaleType());
		} else {
			mDrawable = null;
		}
		super.setImageDrawable(mDrawable);
	}

	@Override
	public void setImageResource(int resId) {
		setImageBitmap(BitmapFactory.decodeResource(getResources(), resId));
	}
	
	@Override
	public void setImageDrawable(Drawable drawable) {
		if(drawable instanceof BitmapDrawable){
			setImageBitmap(((BitmapDrawable)drawable).getBitmap());
		}else if(drawable instanceof StreamDrawable){
			super.setImageDrawable(drawable);
		}else if(drawable instanceof ColorDrawable){
			Bitmap bitmap = Bitmap.createBitmap(this.getWidth(), this.getHeight(), Config.ARGB_8888);
		    Canvas canvas = new Canvas(bitmap); 
		    drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
		    drawable.draw(canvas);
		    setImageBitmap(bitmap);
		}else{
			Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Config.ARGB_8888);
		    Canvas canvas = new Canvas(bitmap); 
		    drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
		    drawable.draw(canvas);
			setImageBitmap(bitmap);
		}
	}
	@Override
    public void setPressed(boolean pressed) {
        if (pressed) {
            setColorFilter(0x55304ffe, PorterDuff.Mode.SRC_ATOP);
        }else {
            clearColorFilter();
        }
        super.setPressed(pressed);
    }
}
