package com.cust.wiglee.modals;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by piyushnishant on 26/09/16.
 */
public class FoodOptionQuestion {
    //FoodOptionAnswer
    private String foodOptionQuestionId;
    private String foodOptionquestionText;
    private List<FoodOptionAnswer> foodOptionAnswer;
    public FoodOptionQuestion(String foodOptionID,String foodQuestionText){
        foodOptionAnswer = new ArrayList<>();
        foodOptionQuestionId = foodOptionID;
        foodOptionquestionText = foodQuestionText;
    }

    public String getFoodOptionQuestionId() {
        return foodOptionQuestionId;
    }

    public void setFoodOptionQuestionId(String foodOptionQuestionId) {
        this.foodOptionQuestionId = foodOptionQuestionId;
    }

    public String getFoodOptionquestionText() {
        return foodOptionquestionText;
    }

    public void setFoodOptionquestionText(String foodOptionquestionText) {
        this.foodOptionquestionText = foodOptionquestionText;
    }

    public List<FoodOptionAnswer> getFoodOptionAnswer() {
        return foodOptionAnswer;
    }

    public void setFoodOptionAnswer(List<FoodOptionAnswer> foodOptionAnswer) {
        this.foodOptionAnswer = foodOptionAnswer;
    }
}
