package com.cust.wiglee.modals;

import java.util.HashMap;

/**
 * Created by piyushnishant on 13/09/16.
 */
public class FoodItemModal {
    private  String mfodname;
    private String mfoodId;
    private String mfoodDetail;
    private String mfoodPrice;
    private String mfoodtype;

    public FoodItemModal(HashMap<String,String> fooditemmap){

        this.mfodname = fooditemmap.get("meal_name");
        this.mfoodDetail = fooditemmap.get("meal_description");
        this.mfoodId = fooditemmap.get("meal_id");
        this.mfoodPrice = fooditemmap.get("meal_price");
        this.mfoodtype = fooditemmap.get("meal_type");

    }

    public String getFoodtype() {
        return mfoodtype;
    }

    public void setFoodtype(String foodtype) {
        this.mfoodtype = foodtype;
    }

    public String getMfodname() {
        return mfodname;
    }

    public void setMfodname(String mfodname) {
        this.mfodname = mfodname;
    }

    public String getMfoodId() {
        return mfoodId;
    }

    public void setMfoodId(String mfoodId) {
        this.mfoodId = mfoodId;
    }

    public String getMfoodDetail() {
        return mfoodDetail;
    }

    public void setMfoodDetail(String mfoodDetail) {
        this.mfoodDetail = mfoodDetail;
    }

    public String getMfoodPrice() {
        return mfoodPrice;
    }

    public void setMfoodPrice(String mfoodPrice) {
        this.mfoodPrice = mfoodPrice;
    }
}
