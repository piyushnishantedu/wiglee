package com.cust.wiglee.modals;

/**
 * Created by piyushnishant on 26/09/16.
 */
public class FoodOptionAnswer {
    private String answerid;
    private String answerText;
    private String price;

    public FoodOptionAnswer(String answerid, String answerText, String price) {
        this.answerid = answerid;
        this.answerText = answerText;
        this.price = price;
    }

    public String getAnswerid() {
        return answerid;
    }

    public void setAnswerid(String answerid) {
        this.answerid = answerid;
    }

    public String getAnswerText() {
        return answerText;
    }

    public void setAnswerText(String answerText) {
        this.answerText = answerText;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
