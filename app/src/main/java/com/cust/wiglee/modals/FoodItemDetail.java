package com.cust.wiglee.modals;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by piyushnishant on 08/09/16.
 */
public class FoodItemDetail {
    private String foodItemDetails;
    private String FoodItemId;
    private String foodType;
    private String foodPrice;
    private String foodImageUrl;
    private String foodItemName;
    private List<FoodItemDetailOption> foodOptions;
    public FoodItemDetail(){
        foodOptions = new ArrayList<>();
    }

    public String getFoodItemDetails() {
        return foodItemDetails;
    }

    public void setFoodItemDetails(String foodItemDetails) {
        this.foodItemDetails = foodItemDetails;
    }

    public String getFoodItemId() {
        return FoodItemId;
    }

    public void setFoodItemId(String foodItemId) {
        FoodItemId = foodItemId;
    }

    public String getFoodType() {
        return foodType;
    }

    public void setFoodType(String foodType) {
        this.foodType = foodType;
    }

    public String getFoodPrice() {
        return foodPrice;
    }

    public void setFoodPrice(String foodPrice) {
        this.foodPrice = foodPrice;
    }

    public String getFoodImageUrl() {
        return foodImageUrl;
    }

    public void setFoodImageUrl(String foodImageUrl) {
        this.foodImageUrl = foodImageUrl;
    }

    public String getFoodItemName() {
        return foodItemName;
    }

    public void setFoodItemName(String foodItemName) {
        this.foodItemName = foodItemName;
    }

    public List<FoodItemDetailOption> getFoodOptions() {
        return foodOptions;
    }

    public void setFoodOptions(List<FoodItemDetailOption> foodOptions) {
        this.foodOptions = foodOptions;
    }
}
