package com.cust.wiglee.modals;

/**
 * Created by piyushnishant on 21/09/16.
 */
public class FoodItemDetailOption {
    private String foodOptionName;
    private String foodOptionid;


    public FoodItemDetailOption(String foodOptionName, String foodOptionid) {
        this.foodOptionName = foodOptionName;
        this.foodOptionid = foodOptionid;
    }

    public String getFoodOptionName() {
        return foodOptionName;
    }

    public void setFoodOptionName(String foodOptionName) {
        this.foodOptionName = foodOptionName;
    }

    public String getFoodOptionid() {
        return foodOptionid;
    }

    public void setFoodOptionid(String foodOptionid) {
        this.foodOptionid = foodOptionid;
    }
}
