package com.cust.wiglee.adapters;

import java.util.ArrayList;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.cust.wiglee.R;

public class AreaSpinnerAdapter  extends ArrayAdapter<String> {

    private Context context;
    private ArrayList<String> data;
    public Resources res;
    private LayoutInflater inflater;
    private boolean isOrderPage;

    public AreaSpinnerAdapter(Context context, ArrayList<String> objects,boolean isOrderPage) {
        super(context, R.layout.spinner_row, objects);
        this.isOrderPage = isOrderPage;
        this.context = context;
        data = objects;
        inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    // This funtion called for each row ( Called data.size() times )
    public View getCustomView(int position, View convertView, ViewGroup parent) {
        View row = null;
        if(isOrderPage)
            row = inflater.inflate(R.layout.spinner_row_order, parent, false);
        else
            row = inflater.inflate(R.layout.spinner_row, parent, false);

        //View row = inflater.inflate(R.layout.spinner_row, parent, false);
        TextView tvCategory = (TextView) row.findViewById(R.id.tvArea);
        tvCategory.setText(data.get(position).toString());
        return row;
    }
}