package com.cust.wiglee.adapters;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.cust.wiglee.R;
import com.cust.wiglee.activity.ShowRestActivity;
import com.cust.wiglee.app.WAppConstants;
import com.cust.wiglee.app.WAppController;
import com.cust.wiglee.bean.ResturantBean;
import com.cust.wiglee.view.common.FontTextView;
import com.cust.wiglee.volley.CustomPostRequest;
import com.cust.wiglee.volley.FrameFeedImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@SuppressLint("ClickableViewAccessibility")
public class ViewResturantListAdapter extends BaseAdapter {
	private String TAG = WAppConstants.APP_TAG;
	private AppCompatActivity activity;
	private ArrayList<ResturantBean> mList;
	private SharedPreferences mPrefs;
	private ImageLoader imageLoader = WAppController.getInstance().getImageLoader();
	private FrameLayout mAnchor;
	//private BottomSheet mBottomSheet;


	@SuppressLint("NewApi")
	public ViewResturantListAdapter(AppCompatActivity activity, ArrayList<ResturantBean> mList, FrameLayout anchor) {
		this.activity = activity;
		this.mList = mList;
		mPrefs = WAppController.getPreferences(activity);
		Display display = this.activity.getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		mAnchor = anchor;
	}

	@Override
	public int getCount() {
		return mList.size();
	}

	@Override
	public Object getItem(int location) {
		return mList.get(location);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	private static class ViewHolder {
		public LinearLayout mRestTitleHolder;
		public FrameFeedImageView mFivRestPic;
		public FontTextView mTvRestTitle;
		public FontTextView mTvRestLoc;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		if (imageLoader == null)
			imageLoader = WAppController.getInstance().getImageLoader();
		if (convertView == null || convertView.getTag() == null) {
			convertView = LayoutInflater.from(activity).inflate(R.layout.item_resturant, parent, false);
			holder = new ViewHolder();
			holder.mRestTitleHolder = (LinearLayout) convertView.findViewById(R.id.ll_rest_title);
			holder.mFivRestPic = (FrameFeedImageView) convertView.findViewById(R.id.fiv_rest_pic);
			holder.mTvRestTitle = (FontTextView) convertView.findViewById(R.id.ftv_rest_title);
			holder.mTvRestLoc = (FontTextView) convertView.findViewById(R.id.ftv_rest_desc);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		final ResturantBean item = mList.get(position);
		Log.d(TAG, "M name " + item.getMerchantName());
		holder.mTvRestTitle.setText(item.getMerchantName());
		holder.mTvRestLoc.setText(item.getMerchantLoc());
		holder.mFivRestPic.setResponseObserver(new FrameFeedImageView.ResponseObserver() {
			@Override
			public void onError() {
			}

			@Override
			public void onSuccess() {
			}

			@Override
			public void onSetParams(LayoutParams params) {
				//int h = params.height;
				//LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, h);
				//holder.mFivEventPicHolder.setLayoutParams(lp);
			}
		});
		holder.mFivRestPic.setErrorImageResId(R.drawable.resturant);
		holder.mFivRestPic.setDefaultImageResId(R.drawable.resturant);
		holder.mFivRestPic.setImageUrl(String.format(WAppConstants.URL_MERCHANT_PIC, item.getMerchantImageId() + "", 80 + ""), imageLoader);
		holder.mFivRestPic.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				int[] startingLocation = new int[2];
				v.getLocationOnScreen(startingLocation);
				startingLocation[0] += v.getWidth() / 2;
				ShowRestActivity.startShowRestActivityFromLocation(startingLocation, item.getMerchantId() + "", activity);
				activity.overridePendingTransition(0, 0);
			}
		});
		holder.mRestTitleHolder.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				int[] startingLocation = new int[2];
				v.getLocationOnScreen(startingLocation);
				startingLocation[0] += v.getWidth() / 2;
				ShowRestActivity.startShowRestActivityFromLocation(startingLocation, item.getMerchantId() + "", activity);
				activity.overridePendingTransition(0, 0);
			}
		});

		return convertView;
	}

	public void setFeeds(ArrayList<ResturantBean> mList) {
		this.mList = mList;
		notifyDataSetChanged();
	}
}
