package com.cust.wiglee.adapters;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.android.volley.toolbox.ImageLoader;
import com.cust.wiglee.R;
import com.cust.wiglee.activity.ShowRestActivity;
import com.cust.wiglee.activity.WWebViewActivity;
import com.cust.wiglee.app.WAppConstants;
import com.cust.wiglee.app.WAppController;
import com.cust.wiglee.bean.MyOrderBean;
import com.cust.wiglee.bean.PlaceOrderBean;
import com.cust.wiglee.view.common.FontTextView;

import java.util.ArrayList;

@SuppressLint("ClickableViewAccessibility")
public class MyOrderListAdapter extends BaseAdapter{
    private String TAG = WAppConstants.APP_TAG;
	private AppCompatActivity activity;
	private ArrayList<MyOrderBean> mList;
	private SharedPreferences mPrefs;
	private ImageLoader imageLoader = WAppController.getInstance().getImageLoader();
	private FrameLayout mAnchor;
	//private BottomSheet mBottomSheet;

	@SuppressLint("NewApi")
	public MyOrderListAdapter(AppCompatActivity activity, ArrayList<MyOrderBean> mList, FrameLayout anchor) {
		this.activity = activity;
		this.mList = mList;
		mPrefs = WAppController.getPreferences(activity);
		mAnchor = anchor;
	}

	@Override
	public int getCount() {
		return mList.size();
	}

	@Override
	public Object getItem(int location) {
		return mList.get(location);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	private static class ViewHolder {
		public LinearLayout mLlOrderHolder;
        public FontTextView mTvOrderId;
        public FontTextView mTvOrderStatus;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		if (imageLoader == null)
			imageLoader = WAppController.getInstance().getImageLoader();
		if (convertView == null || convertView.getTag() == null) {
			convertView = LayoutInflater.from(activity).inflate(R.layout.item_my_order, parent, false);
			holder = new ViewHolder();
			holder.mLlOrderHolder = (LinearLayout) convertView.findViewById(R.id.ll_my_rder_holder);
            holder.mTvOrderId = (FontTextView) convertView.findViewById(R.id.ftv_order_no);
            holder.mTvOrderStatus = (FontTextView) convertView.findViewById(R.id.ftv_order_status);
			convertView.setTag(holder);
		}else {
			holder = (ViewHolder)convertView.getTag();
		}
		final MyOrderBean item = mList.get(position);
		holder.mTvOrderId.setText("Order# "+item.getOrderId());
        holder.mTvOrderStatus.setText(item.getOrderStatus());
		holder.mLlOrderHolder.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String url = String.format(activity.getString(R.string.my_order_url),item.getOrderId(),mPrefs.getString(WAppConstants.LOGGED_IN_USER_ID,"1"), mPrefs.getString(WAppConstants.LOGGED_IN_USER_SEC_CODE, ""));
				Log.d(TAG,"Order url "+url);
				String fullUrl = WAppConstants.SERVER_URL+url;
				Log.d(TAG,"Order fullUrl "+fullUrl);
				Intent webIntent = new Intent(activity, WWebViewActivity.class);
				webIntent.putExtra("passed_url", fullUrl);
				webIntent.putExtra("passed_title", "Order# "+item.getOrderId());
				activity.startActivity(webIntent);
				activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
			}
		});
		return convertView;
	}

    public void setFeeds(ArrayList<MyOrderBean> mList){
		this.mList=mList;
		notifyDataSetChanged();
	}
}