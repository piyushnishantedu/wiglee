package com.cust.wiglee.adapters;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.cust.wiglee.R;
import com.cust.wiglee.app.WAppConstants;
import com.cust.wiglee.app.WAppController;
import com.cust.wiglee.bean.PlaceOrderBean;
import com.cust.wiglee.view.common.FontTextView;
import com.cust.wiglee.volley.CustomPostRequest;
import com.cust.wiglee.volley.FrameFeedImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@SuppressLint("ClickableViewAccessibility")
public class PlaceOrderListAdapter extends BaseAdapter{
    private String TAG = WAppConstants.APP_TAG;
	private AppCompatActivity activity;
	private ArrayList<PlaceOrderBean> mList;
	private SharedPreferences mPrefs;
	private ImageLoader imageLoader = WAppController.getInstance().getImageLoader();
	private FrameLayout mAnchor;
	//private BottomSheet mBottomSheet;

	@SuppressLint("NewApi")
	public PlaceOrderListAdapter(AppCompatActivity activity, ArrayList<PlaceOrderBean> mList, FrameLayout anchor) {
		this.activity = activity;
		this.mList = mList;
		mPrefs = WAppController.getPreferences(activity);
		mAnchor = anchor;
	}

	@Override
	public int getCount() {
		return mList.size();
	}

	@Override
	public Object getItem(int location) {
		return mList.get(location);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	private static class ViewHolder {
        public FontTextView mTvMealsTitle;
        public FontTextView mTvMealsQntyPrice;
        public FontTextView mTvMealTotal;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		if (imageLoader == null)
			imageLoader = WAppController.getInstance().getImageLoader();
		if (convertView == null || convertView.getTag() == null) {
			convertView = LayoutInflater.from(activity).inflate(R.layout.item_place_order, parent, false);
			holder = new ViewHolder();
            holder.mTvMealsTitle = (FontTextView) convertView.findViewById(R.id.ftv_meals_title);
            holder.mTvMealsQntyPrice = (FontTextView) convertView.findViewById(R.id.ftv_meals_price);
            holder.mTvMealTotal = (FontTextView) convertView.findViewById(R.id.ftv_meals_total);
			convertView.setTag(holder);
		}else {
			holder = (ViewHolder)convertView.getTag();
		}
		final PlaceOrderBean item = mList.get(position);
		holder.mTvMealsTitle.setText(item.getMealName());
        holder.mTvMealsQntyPrice.setText(String.format(activity.getString(R.string.meals_price_qnty),item.getMealPrice(),item.getQuantity()));
        holder.mTvMealTotal.setText(String.format(activity.getString(R.string.string_price),item.getMealTotal()+""));
		return convertView;
	}

    public void setFeeds(ArrayList<PlaceOrderBean> mList){
		this.mList=mList;
		notifyDataSetChanged();
	}
}