package com.cust.wiglee.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cust.wiglee.R;
import com.cust.wiglee.listner.FoodItemClickistener;
import com.cust.wiglee.modals.FoodItemModal;

import java.util.List;

/**
 * Created by piyushnishant on 20/09/16.
 */
public class FoodItemListAdapter extends RecyclerView.Adapter<FoodItemListAdapter.FoodItemViewHolder>{
    private List<FoodItemModal> foodItemList;
    private FoodItemClickistener fooditemclicklistener;
    public class FoodItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView foodName,foodDetail,foodPrice;
        public FoodItemViewHolder(View view) {
            super(view);
            foodName = (TextView) view.findViewById(R.id.food_item_name);
            foodDetail = (TextView) view.findViewById(R.id.food_item_detail);
            foodPrice = (TextView) view.findViewById(R.id.food_item_price);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (fooditemclicklistener != null){
                int position = getPosition();
                FoodItemModal foodmodalObject = foodItemList.get(position);

                fooditemclicklistener.onfoodItemClick(position,foodmodalObject);
            }


        }
    }


    public FoodItemListAdapter(List<FoodItemModal> foodlist) {
        this.foodItemList = foodlist;
    }

    public void setFoodItemClickListener(FoodItemClickistener fooditemlistener){
        this.fooditemclicklistener = fooditemlistener;

    }

    @Override
    public FoodItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.food_row_item, parent, false);

        return new FoodItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FoodItemViewHolder holder, final int position) {

        final FoodItemModal foodItemObject = foodItemList.get(position);
        holder.foodName.setText(foodItemObject.getMfodname());
        holder.foodDetail.setText(foodItemObject.getMfoodDetail());
        holder.foodPrice.setText(foodItemObject.getMfoodPrice());

        /*Intent foodDetailIntent = new Intent(WHomeActivity.this, FoodTabbarActivity.class);
                WHomeActivity.this.startActivity(mealsIntent);
                WHomeActivity.this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);*/

    }

    @Override
    public int getItemCount() {
        return foodItemList.size();
    }

}
