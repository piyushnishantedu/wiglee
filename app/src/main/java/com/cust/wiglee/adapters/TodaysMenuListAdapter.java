package com.cust.wiglee.adapters;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.cust.wiglee.R;
import com.cust.wiglee.app.WAppConstants;
import com.cust.wiglee.app.WAppController;
import com.cust.wiglee.bean.MenuBean;
import com.cust.wiglee.view.common.FontTextView;
import com.cust.wiglee.volley.CustomPostRequest;
import com.cust.wiglee.volley.FrameFeedImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@SuppressLint("ClickableViewAccessibility")
public class TodaysMenuListAdapter extends BaseAdapter{
    private String TAG = WAppConstants.APP_TAG;
	private AppCompatActivity activity;
	private ArrayList<MenuBean> mMealsList;
	private SharedPreferences mPrefs;
	private ImageLoader imageLoader = WAppController.getInstance().getImageLoader();
	private FrameLayout mAnchor;
	//private boolean iSWidthLoaded = false;
	//private BottomSheet mBottomSheet;
	private AdapterCallback mAdapterCallback;


	@SuppressLint("NewApi")
	public TodaysMenuListAdapter(AppCompatActivity activity, ArrayList<MenuBean> mMealsList,FrameLayout anchor) {
		this.activity = activity;
		this.mMealsList = mMealsList;
		mPrefs = WAppController.getPreferences(activity);
		Display display = this.activity.getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		mAnchor = anchor;
		try {
			this.mAdapterCallback = ((AdapterCallback) activity);
		} catch (ClassCastException e) {
			throw new ClassCastException("Activity must implement AdapterCallback.");
		}
	}

	@Override
	public int getCount() {
		return mMealsList.size();
	}

	@Override
	public Object getItem(int location) {
		return mMealsList.get(location);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	private static class ViewHolder {
        public FrameFeedImageView mFivMealsPic;
        public FontTextView mTvMealsTitle;
        public FontTextView mTvMealsDesc;
        public FontTextView mTvMealsSubTxt;
        public TextView mTvMealsPrice;
        public Button mAddMealsBtn;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		if (imageLoader == null)
			imageLoader = WAppController.getInstance().getImageLoader();
		if (convertView == null || convertView.getTag() == null) {
			convertView = LayoutInflater.from(activity).inflate(R.layout.item_meals, parent, false);
			holder = new ViewHolder();

            holder.mFivMealsPic = (FrameFeedImageView) convertView.findViewById(R.id.fiv_meals_pic);
            holder.mTvMealsTitle = (FontTextView) convertView.findViewById(R.id.ftv_meals_title);
            holder.mTvMealsDesc = (FontTextView) convertView.findViewById(R.id.ftv_meals_desc);
            holder.mTvMealsSubTxt = (FontTextView) convertView.findViewById(R.id.ftv_meals_sub_txt);
            holder.mTvMealsPrice = (TextView) convertView.findViewById(R.id.tv_meals_price);
            holder.mAddMealsBtn= (Button) convertView.findViewById(R.id.btn_add_meal);
			convertView.setTag(holder);
		}else {
			holder = (ViewHolder)convertView.getTag();
		}
		final MenuBean item = mMealsList.get(position);

		holder.mTvMealsTitle.setText(item.getMealName());
        holder.mTvMealsDesc.setText(item.getMealDesc());
        holder.mTvMealsPrice.setText(String.format(activity.getString(R.string.meals_price), item.getMealPrice()));

        holder.mFivMealsPic.setResponseObserver(new FrameFeedImageView.ResponseObserver() {
			@Override
			public void onError() {
			}

			@Override
			public void onSuccess() {
			}

			@Override
			public void onSetParams(LayoutParams params) {
				//int h = params.height;
				//LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, h);
				//holder.mFivEventPicHolder.setLayoutParams(lp);
			}
		});
		holder.mFivMealsPic.setErrorImageResId(R.drawable.meal);
		holder.mFivMealsPic.setDefaultImageResId(R.drawable.meal);
        holder.mFivMealsPic.setImageUrl(String.format(WAppConstants.URL_MEALS_PIC, item.getMealImageId() + "", 80 + ""), imageLoader);


		Log.d(TAG, "Qnty " + item.getMealQnty());
		//holder.mAddMealsBtn.setFocusable(false);
		if(item.getMealQnty()>0){
			holder.mAddMealsBtn.setText("Add");
			//holder.mAddMealsBtn.setEnabled(true);

		}else{
			holder.mAddMealsBtn.setText("Sold Out");
			holder.mAddMealsBtn.setEnabled(false);
			holder.mAddMealsBtn.setText("Add");
			holder.mAddMealsBtn.setEnabled(true);
		}






		holder.mAddMealsBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
					addMeal(item);
            }
        });
		return convertView;
	}

    public void setFeeds(ArrayList<MenuBean> mMealsList){
		this.mMealsList=mMealsList;
		notifyDataSetChanged();
	}

   private void addMeal(final MenuBean item) {
		CustomPostRequest jsonReq = new CustomPostRequest(Request.Method.POST,
				WAppConstants.URL_ADD_MEAL, null, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response) {
				Log.d(TAG, "Add to cart response : "+response);
				if (response != null) {
					try {
						String result = response.getString("status");
						if(result.equals("success")){
							Snackbar mSnackbar = Snackbar.make(mAnchor, "Successfully added to cart", Snackbar.LENGTH_LONG);
							mSnackbar.show();
							try {
								mAdapterCallback.onMethodCallbackAddToCart(1,item);
							} catch (Exception exception) {}
						}else{
							Snackbar mSnackbar = Snackbar.make(mAnchor, "Failed to add to cart", Snackbar.LENGTH_LONG);
							mSnackbar.show();
						}
					} catch (JSONException e) {e.printStackTrace();}
				}
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				Log.e(TAG, "Error: " + error.getMessage());
			}
		}){
			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				Map<String, String> params = new HashMap<String, String>();
				params.put("meal_id", item.getMealId()+"");
				params.put("user_id", mPrefs.getString(WAppConstants.LOGGED_IN_USER_ID, ""));
				params.put("sec_code", mPrefs.getString(WAppConstants.LOGGED_IN_USER_SEC_CODE,""));
				return params;
			}
		};
		WAppController.getInstance().addToRequestQueue(jsonReq);
	}
    private SpannableStringBuilder applyBoldStyleTitle(String text) {
        SpannableStringBuilder ss = new SpannableStringBuilder(text);
        ss.setSpan(new StyleSpan(Typeface.BOLD), 0, text.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(new AbsoluteSizeSpan((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 16,
                activity.getResources().getDisplayMetrics())), 0, ss.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return ss;
    }
    private SpannableStringBuilder applyBoldStyleTime(String text) {
        SpannableStringBuilder ss = new SpannableStringBuilder(text);
        ss.setSpan(new AbsoluteSizeSpan((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 13,
                activity.getResources().getDisplayMetrics())), 0, ss.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return ss;
    }

	public static interface AdapterCallback {
		void onMethodCallbackAddToCart(int count,MenuBean item);
	}



}
//http://stackoverflow.com/questions/2150078/how-to-check-visibility-of-software-keyboard-in-android
