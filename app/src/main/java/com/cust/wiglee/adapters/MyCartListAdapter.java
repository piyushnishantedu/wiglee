package com.cust.wiglee.adapters;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.cust.wiglee.R;
import com.cust.wiglee.app.WAppConstants;
import com.cust.wiglee.app.WAppController;
import com.cust.wiglee.bean.CartBean;
import com.cust.wiglee.bean.MenuBean;
import com.cust.wiglee.view.common.FontTextView;
import com.cust.wiglee.volley.CustomPostRequest;
import com.cust.wiglee.volley.FrameFeedImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@SuppressLint("ClickableViewAccessibility")
public class MyCartListAdapter extends BaseAdapter{
    private String TAG = WAppConstants.APP_TAG;
	private AppCompatActivity activity;
	private ArrayList<CartBean> mCartList;
	private SharedPreferences mPrefs;
	private ImageLoader imageLoader = WAppController.getInstance().getImageLoader();
	private FrameLayout mAnchor;
	//private boolean iSWidthLoaded = false;
	//private BottomSheet mBottomSheet;
	private AdapterCallback mAdapterCallback;

	@SuppressLint("NewApi")
	public MyCartListAdapter(AppCompatActivity activity, ArrayList<CartBean> mCartList, FrameLayout anchor) {
		this.activity = activity;
		this.mCartList = mCartList;
		mPrefs = WAppController.getPreferences(activity);
		Display display = this.activity.getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		mAnchor = anchor;
		try {
			this.mAdapterCallback = ((AdapterCallback) activity);
		} catch (ClassCastException e) {
			throw new ClassCastException("Activity must implement AdapterCallback.");
		}
	}

	@Override
	public int getCount() {
		return mCartList.size();
	}

	@Override
	public Object getItem(int location) {
		return mCartList.get(location);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	private static class ViewHolder {
        public FrameFeedImageView mFivMealsPic;
        public FontTextView mTvMealsTitle;
        public FontTextView mTvMealsDesc;
        public FontTextView mTvMealsSubTxt;
        public TextView mTvMealsPrice;
		public TextView mTvCartQnty;
        public ImageView mAddQnty;
		public ImageView mSubsQnty;

		public TextView mTvRemove;
		public Button mBtnremove;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		if (imageLoader == null)
			imageLoader = WAppController.getInstance().getImageLoader();
		if (convertView == null || convertView.getTag() == null) {
			convertView = LayoutInflater.from(activity).inflate(R.layout.item_cart, parent, false);
			holder = new ViewHolder();

            holder.mFivMealsPic = (FrameFeedImageView) convertView.findViewById(R.id.fiv_meals_pic);
            holder.mTvMealsTitle = (FontTextView) convertView.findViewById(R.id.ftv_meals_title);
            holder.mTvMealsDesc = (FontTextView) convertView.findViewById(R.id.ftv_meals_desc);
            holder.mTvMealsSubTxt = (FontTextView) convertView.findViewById(R.id.ftv_meals_sub_txt);
            holder.mTvMealsPrice = (TextView) convertView.findViewById(R.id.tv_meals_price);
			holder.mTvCartQnty = (TextView) convertView.findViewById(R.id.tv_qnty);
            holder.mAddQnty= (ImageView) convertView.findViewById(R.id.btn_add_qnty);
			holder.mSubsQnty= (ImageView) convertView.findViewById(R.id.btn_subs_qnty);
			holder.mTvRemove = (TextView) convertView.findViewById(R.id.tv_cart_del);
			holder.mBtnremove = (Button) convertView.findViewById(R.id.btn_cart_del);
			convertView.setTag(holder);
		}else {
			holder = (ViewHolder)convertView.getTag();
		}
		final CartBean item = mCartList.get(position);

		holder.mTvMealsTitle.setText(item.getMealName());
        holder.mTvMealsDesc.setText(item.getMealDesc());
        holder.mTvCartQnty.setText(item.getQuantity()+"");
		holder.mTvMealsPrice.setText(String.format(activity.getString(R.string.meals_price),item.getMealPrice()));
        holder.mFivMealsPic.setResponseObserver(new FrameFeedImageView.ResponseObserver() {
            @Override
            public void onError() {
            }

            @Override
            public void onSuccess() {
            }

            @Override
            public void onSetParams(LayoutParams params) {
                //int h = params.height;
                //LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, h);
                //holder.mFivEventPicHolder.setLayoutParams(lp);
            }
        });
		holder.mFivMealsPic.setErrorImageResId(R.drawable.meal);
		holder.mFivMealsPic.setDefaultImageResId(R.drawable.meal);
        holder.mFivMealsPic.setImageUrl(String.format(WAppConstants.URL_MEALS_PIC, item.getMealImageId()+"", 80+""), imageLoader);
        holder.mAddQnty.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
				item.setQuantity(item.getQuantity()+1);
				holder.mTvCartQnty.setText(item.getQuantity()+"");
				notifyDataSetChanged();
				chkMealQnty(item);
            }
        });
		holder.mSubsQnty.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(item.getQuantity() > 1) {
					item.setQuantity(item.getQuantity() - 1);
					holder.mTvCartQnty.setText(item.getQuantity() + "");
					notifyDataSetChanged();
				}
			}
		});

		holder.mTvRemove.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mCartList.remove(item);
				notifyDataSetChanged();
				removeFrmCart(item);
				try {
					mAdapterCallback.onMethodCallbackUpdateCart();
				} catch (Exception exception) {}
			}
		});
		holder.mBtnremove.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mCartList.remove(item);
				notifyDataSetChanged();
				removeFrmCart(item);
				try {
					mAdapterCallback.onMethodCallbackUpdateCart();
				} catch (Exception exception) {}
			}
		});
		return convertView;
	}

    public void setFeeds(ArrayList<CartBean> mCartList){
		this.mCartList=mCartList;
		notifyDataSetChanged();
	}

   private void removeFrmCart(final CartBean item) {
		CustomPostRequest jsonReq = new CustomPostRequest(Request.Method.POST,
				WAppConstants.URL_CART_REMOVE, null, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response) {
				Log.d(TAG, "Remove from cart response : "+response);
				if (response != null) {

				}
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				Log.e(TAG, "Error: " + error.getMessage());
			}
		}){
			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				Map<String, String> params = new HashMap<String, String>();
				params.put("cart_id", item.getCartId()+"");
				params.put("user_id", mPrefs.getString(WAppConstants.LOGGED_IN_USER_ID, ""));
				params.put("sec_code", mPrefs.getString(WAppConstants.LOGGED_IN_USER_SEC_CODE,""));
				return params;
			}
		};
		WAppController.getInstance().addToRequestQueue(jsonReq);
	}
	private void chkMealQnty(final CartBean item) {
		CustomPostRequest jsonReq = new CustomPostRequest(Request.Method.POST,
				WAppConstants.URL_DO_CHK_MEAL_QNTY, null, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response) {
				Log.d(TAG, "Add to cart response : "+response);
				if (response != null) {
					try {
						String result = response.getString("status");
						if(result.equals("success")){
							int qnty = response.getInt("quantity");
							if(item.getQuantity() > qnty ){
								item.setQuantity(item.getQuantity() - 1);
								notifyDataSetChanged();
								Snackbar mSnackbar = Snackbar.make(mAnchor, "Requested quantity not available.", Snackbar.LENGTH_LONG);
								mSnackbar.show();
							}
						}else{

						}
					} catch (JSONException e) {e.printStackTrace();}
				}
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				Log.e(TAG, "Error: " + error.getMessage());
			}
		}){
			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				Map<String, String> params = new HashMap<String, String>();
				params.put("meal_id", item.getMealId()+"");
				params.put("user_id", mPrefs.getString(WAppConstants.LOGGED_IN_USER_ID, ""));
				params.put("sec_code", mPrefs.getString(WAppConstants.LOGGED_IN_USER_SEC_CODE,""));
				return params;
			}
		};
		WAppController.getInstance().addToRequestQueue(jsonReq);
	}
	public static interface AdapterCallback {
		void onMethodCallbackUpdateCart();
	}
}
