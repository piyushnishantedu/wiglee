package com.cust.wiglee.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.cust.wiglee.tabbarfragments.VegFoodFragment;

/**
 * Created by piyushnishant on 13/09/16.
 */
public class FoodTypePagerAdapter extends FragmentStatePagerAdapter {
     int mNumOfTabs;
   // private String tabTitles[] = new String[] { "Veg", "Non Veg", "Both" };

    public FoodTypePagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                VegFoodFragment tab1 = new VegFoodFragment();
                return tab1;
            case 1:
                VegFoodFragment tab2 = new VegFoodFragment();
                return tab2;
            case 2:
                VegFoodFragment tab3 = new VegFoodFragment();
                return tab3;
            case 3:
                VegFoodFragment tab4 = new VegFoodFragment();
                return tab4;
            case 4:
                VegFoodFragment tab5 = new VegFoodFragment();
                return tab5;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

}
