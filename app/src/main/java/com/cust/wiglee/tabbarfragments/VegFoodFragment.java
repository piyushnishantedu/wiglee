package com.cust.wiglee.tabbarfragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.cust.wiglee.R;
import com.cust.wiglee.activity.FoodDetailActivity;
import com.cust.wiglee.adapters.FoodItemListAdapter;
import com.cust.wiglee.app.WAppConstants;
import com.cust.wiglee.app.WAppController;
import com.cust.wiglee.listner.FoodItemClickistener;
import com.cust.wiglee.modals.FoodItemModal;
import com.cust.wiglee.volley.CustomPostRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by piyushnishant on 13/09/16.
 */
public class VegFoodFragment extends Fragment  {
    private RecyclerView foofItemListRecylerObj;
    private FoodItemListAdapter foodItemListadapterObj;
    private List<FoodItemModal> foodItemList = new ArrayList<>();
    private SharedPreferences mPrefs;
    private String TAG = WAppConstants.APP_TAG;
    private LinearLayout mLoading;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mPrefs = WAppController.getPreferences(getActivity());

        View rootView = inflater.inflate(R.layout.food_item_list, container, false);
        mLoading = (LinearLayout) rootView.findViewById(R.id.loading_meal_list);
        foofItemListRecylerObj = (RecyclerView) rootView.findViewById(R.id.food_list_recylerview);
        foodItemListadapterObj = new FoodItemListAdapter(foodItemList);
        //foodcategoryAndItemadapterObject.setCustom
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.getContext());
        foofItemListRecylerObj.setLayoutManager(mLayoutManager);
        foofItemListRecylerObj.setItemAnimator(new DefaultItemAnimator());
        foofItemListRecylerObj.setAdapter(foodItemListadapterObj);
        foodItemListadapterObj.setFoodItemClickListener(new FoodItemClickistener() {
            @Override
            public void onfoodItemClick(int position, FoodItemModal foodItemObject) {
                Log.i("uyee","yayay");
                Intent foodDetailIntent = new Intent(getActivity(), FoodDetailActivity.class);
                //foodDetailIntent.putExtra("fooditemObject", foodItemObject);
                foodDetailIntent.putExtra("fooditemID",foodItemObject.getMfoodId());
                foodDetailIntent.putExtra("foodName",foodItemObject.getMfodname());
                getActivity().startActivity(foodDetailIntent);
                getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });


        prepareFoodItemModaldata(false);
        return rootView;
    }




    private void prepareFoodItemModaldata(final boolean isRefreshReq){
        /*FoodItemModal f1 = new FoodItemModal();
        FoodItemModal f2 = new FoodItemModal();
        FoodItemModal f3 = new FoodItemModal();
        FoodItemModal f4 = new FoodItemModal();
        FoodItemModal f5 = new FoodItemModal();
        f1.setMfodname("f1");
        f1.setMfoodDetail("f1 detail");
        f1.setMfoodId("1");
        f1.setMfoodPrice("$50");
        foodItemList.add(f1);
        foodItemList.add(f2);
        foodItemList.add(f3);
        foodItemList.add(f4);
        foodItemList.add(f5);
        foodItemListadapterObj.notifyDataSetChanged();*/
        if(isRefreshReq){
            mLoading.setVisibility(View.GONE);
        }else{
            mLoading.setVisibility(View.VISIBLE);
        }
        CustomPostRequest jsonReq = new CustomPostRequest(Request.Method.POST,
                WAppConstants.URL_MEAL_LIST, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "GET_MY_ORDER Response: " + response.toString());
                if (response != null) {
                    Log.i("success","Yesy");
                    parseFoodItemListJsonResponse(response);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error",""+error);
                if(!isRefreshReq){
                    mLoading.setVisibility(View.GONE);
//                    mEmptyErrMsg.setVisibility(View.VISIBLE);
//                    mNoConnIcon.setVisibility(View.VISIBLE);
//                    mEmptyErrMsgTv.setText(getResources().getString(R.string.no_conn_retry));
                }else{
                    //Snackbar.make("", "Failed to refresh", Snackbar.LENGTH_SHORT).show();
                    //Snackbar.
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", mPrefs.getString(WAppConstants.LOGGED_IN_USER_ID,"1"));
                params.put("sec_code", mPrefs.getString(WAppConstants.LOGGED_IN_USER_SEC_CODE, ""));
//                params.put("meal_type","n");
//                params.put("user_id", "78");
//                params.put("sec_code","ecd5c7fffd40c33035bec8cc4ec7febe");
                params.put("meal_type",WAppConstants.FOOD_TYPE_NONVEG);
                return params;
            }
        };
        WAppController.getInstance().addToRequestQueue(jsonReq);


    }
    private void parseFoodItemListJsonResponse(JSONObject response){

        try{
            Log.i("res",""+response);
            JSONArray postJsonArray = response.getJSONArray("data");
            Log.i("code",""+response.getString("status_code"));
            Log.i("data",""+postJsonArray);
            if(postJsonArray.length()>0) {
                for (int i = 0; i < postJsonArray.length(); i++) {
                    JSONObject feedObj = (JSONObject) postJsonArray.get(i);
                    HashMap<String,String> foodmap = new HashMap<String,String>();
                    foodmap.put("meal_id",feedObj.getString("meal_id"));
                    foodmap.put("meal_name",feedObj.getString("meal_name"));
                    foodmap.put("meal_description",feedObj.getString("meal_description"));
                    foodmap.put("meal_type",feedObj.getString("meal_type"));
                    foodmap.put("meal_price",feedObj.getString("meal_price"));
                    FoodItemModal fooditemModalObject = new FoodItemModal(foodmap);
                    foodItemList.add(fooditemModalObject);

                }
                foodItemListadapterObj.notifyDataSetChanged();

            }
            mLoading.setVisibility(View.GONE);


        }catch(JSONException e){

            e.printStackTrace();
        }
    }


}
