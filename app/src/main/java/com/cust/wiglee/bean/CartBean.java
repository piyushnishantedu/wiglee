package com.cust.wiglee.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class CartBean implements Parcelable{
	private int cartId;
	private int mealId;
	private int quantity;
	private String mealName;
	private String mealDesc;
	private String mealType;
	private String mealPrice;
	private String mealImageId;
	private String processFlag;

	public CartBean() {
	}


	public int getCartId() {
		return cartId;
	}

	public void setCartId(int cartId) {
		this.cartId = cartId;
	}

	public int getMealId() {
		return mealId;
	}

	public void setMealId(int mealId) {
		this.mealId = mealId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getMealName() {
		return mealName;
	}

	public void setMealName(String mealName) {
		this.mealName = mealName;
	}

	public String getMealDesc() {
		return mealDesc;
	}

	public void setMealDesc(String mealDesc) {
		this.mealDesc = mealDesc;
	}

	public String getMealType() {
		return mealType;
	}

	public void setMealType(String mealType) {
		this.mealType = mealType;
	}

	public String getMealPrice() {
		return mealPrice;
	}

	public void setMealPrice(String mealPrice) {
		this.mealPrice = mealPrice;
	}

	public String getMealImageId() {
		return mealImageId;
	}

	public void setMealImageId(String mealImageId) {
		this.mealImageId = mealImageId;
	}

	public String getProcessFlag() {
		return processFlag;
	}

	public void setProcessFlag(String processFlag) {
		this.processFlag = processFlag;
	}

	public static final Creator<CartBean> CREATOR = new Creator<CartBean>() {
		 public CartBean createFromParcel(Parcel in) {
			 return new CartBean(in);
		 }
		 public CartBean[] newArray(int size) {
			 return new CartBean[size];
		 }
	 };

	public CartBean(Parcel input) {
		this.cartId = input.readInt();
		this.mealId = input.readInt();
		this.quantity = input.readInt();
		this.mealName = input.readString();
		this.mealDesc = input.readString();
		this.mealType = input.readString();
		this.mealPrice = input.readString();
		this.mealImageId = input.readString();
		this.processFlag = input.readString();
	}
	
	@Override
	public int describeContents() {	
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(cartId);
		dest.writeInt(mealId);
		dest.writeInt(quantity);
		dest.writeString(mealName);
		dest.writeString(mealDesc);
		dest.writeString(mealType);
		dest.writeString(mealPrice);
		dest.writeString(mealImageId);
		dest.writeString(processFlag);
	}
}

