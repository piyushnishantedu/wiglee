package com.cust.wiglee.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class MenuBean implements Parcelable{
	private int mealId;
	private String mealName;
	private String mealDesc;
	private String mealType;
	private String mealPrice;
	private int mealQnty;
	private String mealImageId;
	private double mealImageAR;

	public MenuBean() {
	}

    public static final Creator<MenuBean> CREATOR = new Creator<MenuBean>() {
		 public MenuBean createFromParcel(Parcel in) {
			 return new MenuBean(in);
		 }
		 public MenuBean[] newArray(int size) {
			 return new MenuBean[size];
		 }
	 };


	public int getMealId() {
		return mealId;
	}

	public void setMealId(int mealId) {
		this.mealId = mealId;
	}

	public String getMealName() {
		return mealName;
	}

	public void setMealName(String mealName) {
		this.mealName = mealName;
	}

	public String getMealDesc() {
		return mealDesc;
	}

	public void setMealDesc(String mealDesc) {
		this.mealDesc = mealDesc;
	}

	public String getMealType() {
		return mealType;
	}

	public void setMealType(String mealType) {
		this.mealType = mealType;
	}

	public String getMealPrice() {
		return mealPrice;
	}

	public void setMealPrice(String mealPrice) {
		this.mealPrice = mealPrice;
	}

	public String getMealImageId() {
		return mealImageId;
	}

	public void setMealImageId(String mealImageId) {
		this.mealImageId = mealImageId;
	}

	public double getMealImageAR() {
		return mealImageAR;
	}

	public int getMealQnty() {
		return mealQnty;
	}

	public void setMealQnty(int mealQnty) {
		this.mealQnty = mealQnty;
	}

	public void setMealImageAR(double mealImageAR) {
		this.mealImageAR = mealImageAR;
	}

	public MenuBean(Parcel input) {
		this.mealId = input.readInt();
		this.mealName = input.readString();
		this.mealDesc = input.readString();
		this.mealType = input.readString();
		this.mealPrice = input.readString();
		this.mealQnty = input.readInt();
		this.mealImageId = input.readString();
		this.mealImageAR = input.readDouble();
	}
	
	@Override
	public int describeContents() {	
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(mealId);
		dest.writeString(mealName);
		dest.writeString(mealDesc);
		dest.writeString(mealType);
		dest.writeString(mealPrice);
		dest.writeInt(mealQnty);
		dest.writeString(mealImageId);
		dest.writeDouble(mealImageAR);
	}
}

