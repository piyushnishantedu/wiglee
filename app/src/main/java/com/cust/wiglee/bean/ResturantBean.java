package com.cust.wiglee.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class ResturantBean {
	private int merchantId;
	private String merchantName;
	private String merchantLoc;
	private String merchantWebsite;
	private String merchantPh;
	private String merchantDesc;
	private int merchantImageId;
	private double merchantImageAR;

	public ResturantBean() {
	}

	public double getMerchantImageAR() {
		return merchantImageAR;
	}

	public void setMerchantImageAR(double merchantImageAR) {
		this.merchantImageAR = merchantImageAR;
	}

	public int getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getMerchantLoc() {
		return merchantLoc;
	}

	public void setMerchantLoc(String merchantLoc) {
		this.merchantLoc = merchantLoc;
	}

	public String getMerchantWebsite() {
		return merchantWebsite;
	}

	public void setMerchantWebsite(String merchantWebsite) {
		this.merchantWebsite = merchantWebsite;
	}

	public String getMerchantPh() {
		return merchantPh;
	}

	public void setMerchantPh(String merchantPh) {
		this.merchantPh = merchantPh;
	}

	public String getMerchantDesc() {
		return merchantDesc;
	}

	public void setMerchantDesc(String merchantDesc) {
		this.merchantDesc = merchantDesc;
	}

	public int getMerchantImageId() {
		return merchantImageId;
	}

	public void setMerchantImageId(int merchantImageId) {
		this.merchantImageId = merchantImageId;
	}
}

