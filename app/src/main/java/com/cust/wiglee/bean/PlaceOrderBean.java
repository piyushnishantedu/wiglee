package com.cust.wiglee.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class PlaceOrderBean{
	private int cartId;
	private int mealId;
	private int quantity;
	private String mealName;
	private int mealPrice;
	private int mealTotal;
	private String mealImageId;
	private String processFlag;

	public PlaceOrderBean() {
	}

	public int getMealTotal() {
		return mealPrice*quantity;
	}

	public int getCartId() {
		return cartId;
	}

	public void setCartId(int cartId) {
		this.cartId = cartId;
	}

	public int getMealId() {
		return mealId;
	}

	public void setMealId(int mealId) {
		this.mealId = mealId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getMealName() {
		return mealName;
	}

	public void setMealName(String mealName) {
		this.mealName = mealName;
	}

	public int getMealPrice() {
		return mealPrice;
	}

	public void setMealPrice(int mealPrice) {
		this.mealPrice = mealPrice;
	}

	public String getMealImageId() {
		return mealImageId;
	}

	public void setMealImageId(String mealImageId) {
		this.mealImageId = mealImageId;
	}

	public String getProcessFlag() {
		return processFlag;
	}

	public void setProcessFlag(String processFlag) {
		this.processFlag = processFlag;
	}
}

