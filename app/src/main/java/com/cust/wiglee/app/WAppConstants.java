package com.cust.wiglee.app;

public class WAppConstants {
    public static final String APP_TAG = "Wiglee";
    //private static String SERVER_URL = "http://10.0.0.3/wiglee/api/";
    public static String SERVER_URL = "http://wiglee.in/api/";
    public static final String LOGGED_IN_USER_ID = "logged_in_user_id";
    public static final String LOGGED_IN_USER_NAME = "logged_in_user_name";
    public static final String LOGGED_IN_USER_EMAIL = "logged_in_user_email";
    public static final String LOGGED_IN_USER_MOBILE_NO = "logged_in_user_mobile";
    public static final String LOGGED_IN_USER_SEC_CODE = "logged_in_user_sec_token";
    public static final String LOGGED_IN_USER_PROF_PIC_ID = "logged_in_user_prof_pic_id";
    public static final String LOGGED_IN_USER_AGE = "logged_in_user_age";
    public static final String LOGGED_IN_USER_DOB = "logged_in_user_dob";
    public static final String LOGGED_IN_USER_GENDER = "logged_in_user_gender";
    public static final String LOGGED_IN_USER_ADDRESS = "logged_in_user_address";
    public static final String LOGGED_IN_USER_ADDRESS_OFFICE = "logged_in_user_address_office";
    public static final String LOGGED_IN_USER_ABT = "logged_in_user_abt";
    public static final String IS_USER_VALIDATED = "logged_in_user_validated";
    public static final String URL_DO_LOGIN = SERVER_URL+"doLogin.php";
    public static final String URL_DO_SIGN_UP = SERVER_URL+"doSignUp.php?";
    public static final String URL_DO_EMAIL_VALIDATION = SERVER_URL+"doEmailValidation.php";
    public static final String URL_DO_RESET_PWD = SERVER_URL+"doForgotPassword.php?";
    public static final String URL_MY_PROFILE_DETAILS = SERVER_URL+"getMyProfileDetails.php";
    public static final String URL_CHANGE_PWD = SERVER_URL+"doChangePwd.php";
    public static final String URL_GET_ALL_MEALS = SERVER_URL+"getAllMeals.php";
    public static final String URL_MEALS_PIC = SERVER_URL+"getMealsPic.php?id=%s&quality=%s";
    public static final String URL_MERCHANT_PIC = SERVER_URL+"getMerchantPic.php?id=%s&quality=%s";
    public static final String URL_PROF_PIC = SERVER_URL+"getProfPic.php?id=%s&width=%s&height=%s";
    public static final String URL_DO_MY_SAVE_ADDRESS = SERVER_URL+"doSaveAddress.php";
    public static final String URL_DO_EDIT_PROF = SERVER_URL+"doProfileEdit.php";
    public static final String URL_ADD_MEAL = SERVER_URL+"doAddMealToCart.php";
    public static final String URL_GET_MY_CART = SERVER_URL+"getUserCartDetails.php";
    public static final String URL_GET_PLACE_ORDER = SERVER_URL+"getItemDetailsInOrderPage.php";
    public static final String URL_GET_MY_ORDER = SERVER_URL+"getOrderList.php";
    public static final String URL_CART_REMOVE = SERVER_URL+"doRemoveFromCart.php";
    public static final String URL_DO_CHK_OUT = SERVER_URL+"doUpdateCart.php";
    public static final String URL_DO_ORDER = SERVER_URL+"doPlaceOrder.php";
    public static final String URL_DO_CHK_MEAL_QNTY = SERVER_URL+"getAvailableMealQuantity.php";
    public static final String URL_GET_CART_COUNT = SERVER_URL+"getCartItemCount.php";
    public static final String URL_GET_ALL_MERCHANTS = SERVER_URL+"getAllRestaurants.php";
    public static final String URL_GET_ALL_AREAS = SERVER_URL+"getAllAreas.php";
    public static final String URL_GET_SINGLE_MERCHANT_DETAILS = SERVER_URL+"getSingleRestaurantDetails.php";
    public static final String SMS_SEND_URL = "http://sms.indiansmsgateway.in/SendSMS/sendmsg.php";
    public static final String SMS_SENDER_USER_NAME = "vrwgle";
    public static final String SMS_SENDER_USER_PASSWORD = "n@5Hj$9R";
    public static final int discountPercentage = 30;
    public static final String URL_MEAL_LIST = "http://192.168.0.101:8888/wiglee/api/getMeallist.php";
    public static final String URL_MEAL_DETAILS = "http://192.168.0.101:8888/wiglee/api/getMealdetails.php";
    public static final String URL_GET_MEAL_QUESTION_LIST = "http://192.168.0.101:8888/wiglee/api/getMealquestion.php";
    public static final String FOOD_TYPE_VEG = "v";
    public static final String FOOD_TYPE_NONVEG = "n";
    public static final String FOOD_TYPE_VEG_NONVEG = "both";


}
