package com.cust.wiglee.app;

import android.app.backup.BackupAgentHelper;
import android.app.backup.SharedPreferencesBackupHelper;

public class WAppBackupAgent extends BackupAgentHelper {
	private static final String BACKUP_KEY = "bumped_prefs";
	@Override
	public void onCreate(){
		// This is the preference name used by PreferenceManager.getDefaultSharedPreferences
		String prefs = getPackageName() + "_wiglee_preferences";
		addHelper(BACKUP_KEY, new SharedPreferencesBackupHelper(this, prefs));
	}
}

