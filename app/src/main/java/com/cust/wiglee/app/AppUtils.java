package com.cust.wiglee.app;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.util.Linkify;
import android.text.util.Linkify.TransformFilter;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AppUtils {
	public static final int MY_PERMISSIONS_REQUEST_GET_ACCOUNT = 1;
    public static float density = 1;
    private static Dictionary<Integer, Integer> sListViewItemHeights = new Hashtable<Integer, Integer>();
    static {
        density = WAppController.applicationContext.getResources().getDisplayMetrics().density;
    }
    public static int getScrollY(ListView lv) {
        View c = lv.getChildAt(0);
        if (c == null) {
            return 0;
        }
        int firstVisiblePosition = lv.getFirstVisiblePosition();
        int top = c.getTop();
        int scrollY = -top + firstVisiblePosition * c.getHeight();
        return scrollY;
    }

    public static int getScrollY(AbsListView lv) {
        View c = lv.getChildAt(0);
        if (c == null) {
            return 0;
        }
        int firstVisiblePosition = lv.getFirstVisiblePosition();
        int scrollY = -(c.getTop());
        sListViewItemHeights.put(lv.getFirstVisiblePosition(), c.getHeight());
        if(scrollY<0)
            scrollY = 0;

        for (int i = 0; i < firstVisiblePosition; ++i) {
            if (sListViewItemHeights.get(i) != null) // (this is a sanity check)
                scrollY += sListViewItemHeights.get(i); //add all heights of the views that are gone
        }
        return scrollY;
    }

	public static boolean isNetworkAvailable(Context context){
	    ConnectivityManager localConnectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    if (localConnectivityManager != null)
	    {
	      NetworkInfo[] arrayOfNetworkInfo = localConnectivityManager.getAllNetworkInfo();
	      if (arrayOfNetworkInfo != null) {
	        for (int i = 0; i < arrayOfNetworkInfo.length; i++) {
	          if (arrayOfNetworkInfo[i].getState() == NetworkInfo.State.CONNECTED) {
	            return true;
	          }
	        }
	      }
	    }
	    return false;
	}

	public static String stringReplacer(String str) {
		if(str!=null){
			str=str.replaceAll("&amp;#039;","'");
			str=str.replaceAll("&amp;amp;","&");
			str=str.replaceAll("&amp;quot;","\"");
			str=str.replaceAll("&amp;lrm;","");
			str=str.replaceAll("&rdquo;","\"");
			str = Html.fromHtml(str).toString();
			return str;
		}else{
			return "";
		}
	}
	public static void addHashTag(TextView textView) {
		Pattern tagMatcher = Pattern.compile("[#]+[A-Za-z0-9-_]+\\b");
        //String mScheme = "content://com.xprezia.hashtags.tagactivity/";
        String mScheme = "http://twitter.com/";
        Linkify.addLinks(textView, Linkify.ALL);
        Linkify.addLinks(textView, tagMatcher, mScheme);
	}

	public static void addAllTag(TextView textView) {
		TransformFilter filter = new TransformFilter() {
		    public final String transformUrl(final Matcher match, String url) {
		        return match.group();
		    }
		};
		Linkify.addLinks(textView, Linkify.ALL);
		Pattern mentionPattern = Pattern.compile("@([A-Za-z0-9_-]+)");
		String mentionScheme = "http://www.twitter.com/";
		Linkify.addLinks(textView, mentionPattern, mentionScheme, null, filter);
		Pattern hashtagPattern = Pattern.compile("#([A-Za-z0-9_-]+)");
		String hashtagScheme = "http://www.twitter.com/";
		Linkify.addLinks(textView, hashtagPattern, hashtagScheme, null, filter);
		/*Pattern urlPattern = Patterns.WEB_URL;
		Linkify.addLinks(textView, urlPattern, null, null, filter);
		Pattern emailPattern = Patterns.EMAIL_ADDRESS;
		Linkify.addLinks(textView, emailPattern, null, null, filter);*/
	}
	/**
	 * Lightens a color by a given factor.
	 *
	 * @param color
	 *            The color to lighten
	 * @param factor
	 *            The factor to lighten the color. 0 will make the color unchanged. 1 will make the
	 *            color white.
	 * @return lighter version of the specified color.
	 */
	public static int lighter(int color, float factor) {
	    int red = (int) ((Color.red(color) * (1 - factor) / 255 + factor) * 255);
	    int green = (int) ((Color.green(color) * (1 - factor) / 255 + factor) * 255);
	    int blue = (int) ((Color.blue(color) * (1 - factor) / 255 + factor) * 255);
	    return Color.argb(Color.alpha(color), red, green, blue);
	}
	/**
	 * Lightens a color by a given factor.
	 *
	 * @param color
	 *            The color to lighten
	 * @return lighter version of the specified color.
	 */
	public static int lighterHSV(int color) {
		  float[] hsv = new float[3];
		  Color.colorToHSV(color, hsv);
		  //hsv[2] = 1.0f - 0.8f * (1.0f - hsv[2]);
		  hsv[2] = 1.0f - 0.6f * (1.0f - hsv[2]);
		  color = Color.HSVToColor(hsv);
		  return color;
	}

    public static void hideSoftKeyboard(View view) {
        if (view == null) {
            return;
        }
        InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (!imm.isActive()) {
            return;
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

	public static void showSoftInput(View v){
		Context context = v.getContext();
		if(context != null){
			InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
			v.requestFocus();
			imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
		}
	}

	public static int dpToPx(float value) {
        if (value == 0) {
            return 0;
        }
        return (int)Math.ceil(density * value);
    }

    public static Bitmap rotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }
	public static boolean isGetAccountPermissionGranted(Context context) {
		if (Build.VERSION.SDK_INT >= 23) {
			if (ContextCompat.checkSelfPermission(context, Manifest.permission.GET_ACCOUNTS)
					== PackageManager.PERMISSION_GRANTED ){
				return true;
			} else {
				ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.GET_ACCOUNTS}, MY_PERMISSIONS_REQUEST_GET_ACCOUNT);
				return false;
			}
		}
		else { //permission is automatically granted on sdk<23 upon installation
			return true;
		}
	}

}
