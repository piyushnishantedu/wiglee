package com.cust.wiglee.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.cust.wiglee.R;
import com.cust.wiglee.adapters.TodaysMenuListAdapter;
import com.cust.wiglee.app.AppUtils;
import com.cust.wiglee.app.WAppConstants;
import com.cust.wiglee.app.WAppController;
import com.cust.wiglee.bean.MenuBean;
import com.cust.wiglee.view.appcompatbckport.PullRefreshLayout;
import com.cust.wiglee.view.appcompatbckport.SwipeRefreshHintLayout;
import com.cust.wiglee.view.appcompatbckport.SwipeRefreshLayoutLinear;
import com.cust.wiglee.view.common.FontTextView;
import com.cust.wiglee.volley.CustomPostRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TodaysMenuActivity extends AppCompatActivity implements TodaysMenuListAdapter.AdapterCallback, SwipeRefreshLayoutLinear.OnRefreshListener{
    private String TAG = WAppConstants.APP_TAG;
    private static final String CURRENT_MEALS_PAGE = "current_meals_page";
    private static final String USER_ID = "USER_ID";
    private static final String IS_LOAD_MORE_REQ = "is_load_more_req";
    private static final String MEALS_LIST = "meals_list";
    private PullRefreshLayout mPullRefreshLayout;
    private ListView mListView;
    private FrameLayout mFooterView;
    private RelativeLayout mEmptyErrMsg;
    private LinearLayout mLoading;
    private FontTextView mEmptyErrMsgTv;
    private ImageView mNoConnIcon;
    private LinearLayout mFooterNoMorePost;
    private LinearLayout mFooterErrLoadingPost;
    private LinearLayout mFooterLoading;
    private ArrayList<MenuBean> mMealsList;
    private TodaysMenuListAdapter mMenuListAdapter;
    private SharedPreferences mPrefs;
    private int mPrevScrollY = 0;
    private int mCurrentMealsPage = 1;
    public volatile boolean isLoadingMore;
    public transient boolean isMoreEventAvailable;
    private volatile boolean mIsLoadMoreReq;
    //private BottomSheet mBottomSheet;
    private FrameLayout mAnchor;
    private TextView mTvCartBadge;
    private int mCartCount =0;
    HashMap<Integer, Integer> mapCart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPrefs = WAppController.getPreferences(this);
        mapCart = new HashMap<Integer, Integer>();
        isLoadingMore = false;
        isMoreEventAvailable = true;
        mIsLoadMoreReq = false;
        setContentView(R.layout.activity_view_todays_menu);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_view_meals);
        if (mToolbar != null) {
            mToolbar.getMenu().clear();
            mToolbar.setTitle("");
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            TextView mTvTitle = (TextView) mToolbar.findViewById(R.id.toolbar_view_meals_title);
            mTvTitle.setText("Today's Menu");
            RelativeLayout mIvCart = (RelativeLayout) mToolbar.findViewById(R.id.toolbar_cart);
            mIvCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent myCartIntent = new Intent(TodaysMenuActivity.this, MyCartActivity.class);
                    TodaysMenuActivity.this.startActivity(myCartIntent);
                    TodaysMenuActivity.this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });
            mTvCartBadge = (TextView) mToolbar.findViewById(R.id.tv_cart_count);


            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
        mAnchor = (FrameLayout) findViewById(R.id.view_meals_anchor);
        mEmptyErrMsg = (RelativeLayout) findViewById(R.id.view_meals_no_conn);
        mEmptyErrMsgTv = (FontTextView) findViewById(R.id.ftv_no_conn_msg_view_meals);
        mNoConnIcon = (ImageView) findViewById(R.id.iv_no_conn_view_meals);
        mLoading = (LinearLayout) findViewById(R.id.loading_view_meals);
        mListView = (ListView) findViewById(R.id.view_meals_list);


        ///////
        mFooterView = (FrameLayout) LayoutInflater.from(this).inflate(R.layout.meals_footer, null);
        mFooterNoMorePost = (LinearLayout) mFooterView.findViewById(R.id.ll_no_more_event);
        mFooterLoading = (LinearLayout) mFooterView.findViewById(R.id.ll_load_more_event);
        mFooterErrLoadingPost = (LinearLayout) mFooterView.findViewById(R.id.ll_err_load_event);
        mFooterErrLoadingPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isMoreEventAvailable = true;
                isLoadingMore = true;
                mIsLoadMoreReq = true;
                loadMenu(mIsLoadMoreReq, false, false);
            }
        });
        mFooterLoading.setVisibility(View.GONE);
        mListView.addFooterView(mFooterView, null, false);
        mMealsList = new ArrayList<MenuBean>();
        mMenuListAdapter = new TodaysMenuListAdapter(this, mMealsList,mAnchor);
        mListView.setAdapter(mMenuListAdapter);
        mPullRefreshLayout = (PullRefreshLayout) findViewById(R.id.ptr_layout_view_meals);
        mPullRefreshLayout.setOnRefreshListener(this);
        mPullRefreshLayout.setRefreshListView(mListView);
        mPullRefreshLayout.setColorScheme(R.color.holo_purple_dark, R.color.holo_blue_bright, R.color.holo_red_light, R.color.holo_green_dark);
        SwipeRefreshHintLayout mSwipeRefreshHintLayout = (SwipeRefreshHintLayout) findViewById(R.id.swipe_hint_view_meals);
        mSwipeRefreshHintLayout.setSwipeLayoutTarget(mPullRefreshLayout);
        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int scrollY = AppUtils.getScrollY(mListView);
                int diff = mPrevScrollY - scrollY;
                if (diff != 0) {
                    //what is the bottom iten that is visible
                    int lastInScreen = firstVisibleItem + visibleItemCount;
                    //is the bottom item visible & not loading more already ? Load more
                    if (AppUtils.isNetworkAvailable(TodaysMenuActivity.this)) {
                        if ((lastInScreen == totalItemCount) && !(isLoadingMore)) {
                            onLoadingMore();
                        }
                    } else {
                        if ((lastInScreen == totalItemCount) && !(isLoadingMore)) {
                            showPopupErrMsg();
                        } else {

                        }
                    }
                }
                mPrevScrollY = scrollY;
            }
        });


//        mListView.setOnItemSelectedListener(new OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                Log.d("clickitem",""+position);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//                Log.d("clickitem","Kuchh nahi");
//            }
//        });


        if (savedInstanceState != null) {
            mLoading.setVisibility(View.GONE);
            mCurrentMealsPage = savedInstanceState.getInt(CURRENT_MEALS_PAGE);
            mIsLoadMoreReq = savedInstanceState.getBoolean(IS_LOAD_MORE_REQ);
            mMealsList = savedInstanceState.getParcelableArrayList(MEALS_LIST);
            mMenuListAdapter.setFeeds(mMealsList);
            if(mMealsList.size() == 0){
                loadMenu(false, false, false);
            }
        } else {
            loadMenu(mIsLoadMoreReq, false, false);
        }
        mEmptyErrMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppUtils.isNetworkAvailable(TodaysMenuActivity.this)) {
                    mEmptyErrMsg.setVisibility(View.GONE);
                    mLoading.setVisibility(View.VISIBLE);
                    loadMenu(false, false, true);
                }
            }
        });
        loadCart();
        setCartBadgeCount();
    }

    private void setCartBadgeCount() {
        CustomPostRequest getCustomRequest = new CustomPostRequest(Request.Method.POST,
                WAppConstants.URL_GET_CART_COUNT, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "URL_GET_CART_COUNT Response: " + response.toString());
                if (response != null) {
                    try {
                        int count = response.getInt("count");
                        mCartCount = count;
                        setCartBadge(count);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        mTvCartBadge.setVisibility(View.GONE);
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", mPrefs.getString(WAppConstants.LOGGED_IN_USER_ID, ""));
                params.put("sec_code", mPrefs.getString(WAppConstants.LOGGED_IN_USER_SEC_CODE,""));

                return params;
            }
        };
        WAppController.getInstance().addToRequestQueue(getCustomRequest);
    }

    private void loadCart() {
        CustomPostRequest jsonAllMealsReq = new CustomPostRequest(Request.Method.POST,
                WAppConstants.URL_GET_MY_CART, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "My Cart Response: " + response.toString());
                if (response != null) {
                    parseCartJsonResponse(response);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", mPrefs.getString(WAppConstants.LOGGED_IN_USER_ID,"1"));
                params.put("sec_code", mPrefs.getString(WAppConstants.LOGGED_IN_USER_SEC_CODE, "ASD"));
                return params;
            }
        };
        WAppController.getInstance().addToRequestQueue(jsonAllMealsReq);
    }
    private void parseCartJsonResponse(JSONObject response) {
        try {
            JSONArray postJsonArray = response.getJSONArray("user_cart_feed");
            if(postJsonArray.length()>0){
                for (int i = 0; i < postJsonArray.length(); i++) {
                    JSONObject feedObj = (JSONObject) postJsonArray.get(i);
                    if (!mapCart.containsKey(feedObj.getInt("meal_id"))) {
                        mapCart.put(feedObj.getInt("meal_id"), feedObj.getInt("meal_id"));
                    }
                }
            }
        } catch (JSONException e) {
            mLoading.setVisibility(View.GONE);
            mPullRefreshLayout.setRefreshing(false);
            e.printStackTrace();
        }
    }

    private void setCartBadge(int count){
        if(count < 1){
            mTvCartBadge.setVisibility(View.GONE);
        }else{
            mTvCartBadge.setVisibility(View.VISIBLE);
            if(count > 9)
                mTvCartBadge.setText(9+"+");
            else
                mTvCartBadge.setText(count+"");
        }
    }
    @Override
    public void onMethodCallbackAddToCart(int count,MenuBean item) {
        if (!mapCart.containsKey(item.getMealId())) {
            mapCart.put(item.getMealId(), item.getMealId());
            mCartCount = mCartCount +count;
            setCartBadge(mCartCount);
        }else{

        }
    }

    public void onLoadingMore() {
        TodaysMenuActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!isLoadingMore && isMoreEventAvailable) {
                    isMoreEventAvailable = true;
                    isLoadingMore = true;
                    mIsLoadMoreReq = true;
                    loadMenu(mIsLoadMoreReq, false, false);
                }
            }
        });
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mCurrentMealsPage = 1;
                isMoreEventAvailable = true;
                mIsLoadMoreReq = false;
                loadMenu(mIsLoadMoreReq, true, false);
            }
        }, 100);
    }

    public void showPopupErrMsg(){
        mFooterErrLoadingPost.setVisibility(View.VISIBLE);
        mFooterNoMorePost.setVisibility(View.GONE);
        mFooterLoading.setVisibility(View.GONE);
    }

    private void loadMenu(final boolean isLoadMoreReq, final boolean isRefreshReq, final boolean isAllEventReq) {
        if(isLoadMoreReq){
            mFooterLoading.setVisibility(View.VISIBLE);
            mFooterNoMorePost.setVisibility(View.GONE);
            mFooterErrLoadingPost.setVisibility(View.GONE);
        }else{
            if(isRefreshReq){
                mLoading.setVisibility(View.GONE);
                mFooterErrLoadingPost.setVisibility(View.GONE);
            }else{
                mLoading.setVisibility(View.VISIBLE);
                mFooterErrLoadingPost.setVisibility(View.GONE);
            }
            if(isAllEventReq) {
                mLoading.setVisibility(View.VISIBLE);
                mMealsList.clear();
                mMenuListAdapter.notifyDataSetChanged();
            }
            mEmptyErrMsg.setVisibility(View.GONE);
            mCurrentMealsPage = 1;
            mIsLoadMoreReq = false;
        }
        Log.d(TAG, "URL(POST): " + WAppConstants.URL_GET_ALL_MEALS+":: "+WAppConstants.URL_GET_ALL_MEALS);
        CustomPostRequest jsonAllMealsReq = new CustomPostRequest(Request.Method.POST,
                WAppConstants.URL_GET_ALL_MEALS, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "Response: " + response.toString());
                if (response != null) {
                    parseMenuJsonResponse(response, isLoadMoreReq);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(!isRefreshReq){
                    if(isLoadMoreReq){
                        mFooterErrLoadingPost.setVisibility(View.VISIBLE);
                        mFooterNoMorePost.setVisibility(View.GONE);
                        mFooterLoading.setVisibility(View.GONE);
                    }else{
                        mLoading.setVisibility(View.GONE);
                        mEmptyErrMsg.setVisibility(View.VISIBLE);
                        mNoConnIcon.setVisibility(View.VISIBLE);
                        mEmptyErrMsgTv.setText(getResources().getString(R.string.no_conn_retry));
                        mFooterLoading.setVisibility(View.GONE);
                        mFooterNoMorePost.setVisibility(View.GONE);
                        mFooterErrLoadingPost.setVisibility(View.GONE);
                    }
                }else{
                    mPullRefreshLayout.setRefreshing(false);
                    Snackbar.make(mAnchor, "Failed to refresh", Snackbar.LENGTH_SHORT).show();
                    mFooterErrLoadingPost.setVisibility(View.VISIBLE);
                    mFooterNoMorePost.setVisibility(View.GONE);
                    mFooterLoading.setVisibility(View.GONE);
                    isLoadingMore = false;
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                if(isLoadMoreReq){
                    mCurrentMealsPage = mCurrentMealsPage +1;
                    params.put("page", mCurrentMealsPage + "");
                    mCurrentMealsPage = mCurrentMealsPage -1;
                }else{
                    params.put("page", mCurrentMealsPage +"");
                }
                params.put("user_id", mPrefs.getString(WAppConstants.LOGGED_IN_USER_ID,"1"));
                params.put("sec_code", mPrefs.getString(WAppConstants.LOGGED_IN_USER_SEC_CODE, "ASD"));
                return params;
            }
        };
        if(AppUtils.isNetworkAvailable(this)){
            WAppController.getInstance().addToRequestQueue(jsonAllMealsReq);
        }else{
            if(isRefreshReq){
                mPullRefreshLayout.setRefreshing(false);
                mFooterErrLoadingPost.setVisibility(View.VISIBLE);
                mFooterNoMorePost.setVisibility(View.GONE);
                mFooterLoading.setVisibility(View.GONE);
                isLoadingMore = false;
            }else{
                mLoading.setVisibility(View.GONE);
                if(mMealsList.size() == 0){
                    mEmptyErrMsg.setVisibility(View.VISIBLE);
                    mNoConnIcon.setVisibility(View.VISIBLE);
                    mEmptyErrMsgTv.setText(getResources().getString(R.string.no_conn_retry));
                }else{
                    mFooterLoading.setVisibility(View.GONE);
                    mFooterNoMorePost.setVisibility(View.GONE);
                    mFooterErrLoadingPost.setVisibility(View.VISIBLE);
                }
            }
        }
    }
    private void parseMenuJsonResponse(JSONObject response, boolean isLoadMoreReq) {
        try {
            if(!isLoadMoreReq){
                mMealsList.clear();
                mMenuListAdapter.notifyDataSetChanged();
            }
            JSONArray postJsonArray = response.getJSONArray("meals_feed");
            if(postJsonArray.length()>0){
                for (int i = 0; i < postJsonArray.length(); i++) {
                    JSONObject feedObj = (JSONObject) postJsonArray.get(i);
                    MenuBean item = new MenuBean();
                    item.setMealId(feedObj.getInt("meal_id"));
                    item.setMealName(feedObj.getString("meal_name"));
                    item.setMealDesc(feedObj.getString("meal_description"));
                    item.setMealType(feedObj.getString("meal_type"));
                    item.setMealPrice(feedObj.getString("meal_price"));
                    item.setMealQnty(feedObj.getInt("meal_quantity"));
                    item.setMealImageId(feedObj.getString("meal_image_id"));
                    item.setMealImageAR(feedObj.getDouble("image_aspect_ratio"));
                    mMealsList.add(item);
                }
                //mFooterLoading.setVisibility(View.VISIBLE);
                mFooterNoMorePost.setVisibility(View.GONE);
                mFooterErrLoadingPost.setVisibility(View.GONE);
                mPullRefreshLayout.setRefreshing(false);
            }else{
                isMoreEventAvailable = false;
                mFooterLoading.setVisibility(View.GONE);
                mFooterNoMorePost.setVisibility(View.VISIBLE);
                mFooterErrLoadingPost.setVisibility(View.GONE);
            }
            if(mMealsList.size() < 1){
                mFooterLoading.setVisibility(View.GONE);
                mFooterNoMorePost.setVisibility(View.GONE);
                mFooterErrLoadingPost.setVisibility(View.GONE);
                mLoading.setVisibility(View.GONE);
                mEmptyErrMsg.setVisibility(View.VISIBLE);
                mNoConnIcon.setVisibility(View.GONE);
                mEmptyErrMsgTv.setText("0pss!! No meal found");
            }else{
                mLoading.setVisibility(View.GONE);
                mEmptyErrMsg.setVisibility(View.GONE);
            }
            mMenuListAdapter.notifyDataSetChanged();
            isLoadingMore = false;
            if(isLoadMoreReq) {
                mCurrentMealsPage = mCurrentMealsPage + 1;
            }else{
                //mListView.smoothScrollToPosition(0);
            }
        } catch (JSONException e) {
            isLoadingMore = false;
            mLoading.setVisibility(View.GONE);
            mPullRefreshLayout.setRefreshing(false);
            e.printStackTrace();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(MEALS_LIST, mMealsList);
        outState.putInt(CURRENT_MEALS_PAGE, mCurrentMealsPage);
        outState.putBoolean(IS_LOAD_MORE_REQ, mIsLoadMoreReq);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }
    @Override
    protected void onStart() {
        super.onStart();
        isLoadingMore = false;
        //GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }
    @Override
    protected void onStop() {
        super.onStop();
        //GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }
    //private SweetAlertDialog mSADialog;
    @Override
    public void onDestroy(){
        /*if(mBottomSheet != null && mBottomSheet.isShowing()){
            mBottomSheet.dismiss();
        }
        if(mSADialog != null && mSADialog.isShowing()){
            mSADialog.dismiss();
        }*/
        super.onDestroy();
    }
    @Override
    protected void onResume() {
        isLoadingMore = false;
        super.onResume();
    }
    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        //Intent intent = new Intent();
        //intent.putExtra("request_approved", mRequestApproved);
        //setResult(RESULT_OK, intent);
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

}