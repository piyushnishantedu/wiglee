package com.cust.wiglee.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.cust.wiglee.R;
import com.cust.wiglee.app.AppUtils;
import com.cust.wiglee.app.WAppConstants;
import com.cust.wiglee.app.WAppController;
import com.cust.wiglee.view.common.FontTextView;
import com.cust.wiglee.view.common.RevealBackgroundView;
import com.cust.wiglee.view.indicators.AVLoadingIndicatorView;
import com.cust.wiglee.volley.CustomPostRequest;
import com.cust.wiglee.volley.ProfImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import pl.aprilapps.easyphotopicker.EasyImage;

public class ShowRestActivity extends AppCompatActivity implements RevealBackgroundView.OnStateChangeListener{
    private String TAG = WAppConstants.APP_TAG;
    public static final String ARG_REVEAL_START_LOCATION = "reveal_start_location";
    private SharedPreferences mPrefs;
    private Animation mFabShowAnimation;
    private ProfImageView mProfImageView;
    private AVLoadingIndicatorView mProgressWheel;
    private LinearLayout mErrLayout;
    private FontTextView mErrMsgFtv;
    private RevealBackgroundView vRevealBackground;
    private NestedScrollView vBody;
    private CollapsingToolbarLayout mCollapToolbar;
    private LinearLayout mWhiteLayout;
    private TextView mTvRestLoc;
    private TextView mTvRestDesc;
    private TextView mTvRestAddress;
    private TextView mTvRestWebsite;
    private TextView mTvRestPhone;
    private TextView mTvRestOffers;
    private CardView mCrdBasicInfo;
    private CardView mCrdRestOffers;
    private String mRestId;
    private String mRestPicId;
    private String mRestName;

    public static void startShowRestActivityFromLocation(int[] startingLocation,String userId, AppCompatActivity startingActivity) {
        Intent intent = new Intent(startingActivity, ShowRestActivity.class);
        intent.putExtra(ARG_REVEAL_START_LOCATION, startingLocation);
        intent.putExtra("rest_id", userId);
        startingActivity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_rest);
        mPrefs = WAppController.getPreferences(this);
        Intent intent = getIntent();
        mRestId = intent.getStringExtra("rest_id");
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_show_rest);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mCollapToolbar = (CollapsingToolbarLayout)findViewById(R.id.collapsing_toolbar_layout);
        mCollapToolbar.setCollapsedTitleTextColor(Color.parseColor("#FFFFFF"));
        mCollapToolbar.setExpandedTitleColor(Color.parseColor("#FFFFFF"));
        mCollapToolbar.setTitle("");

        vBody = (NestedScrollView) findViewById(R.id.nsv_show_rest);
        mWhiteLayout = (LinearLayout) findViewById(R.id.ll_white_bkg);
        vRevealBackground = (RevealBackgroundView) findViewById(R.id.rv_bkg);
        mProfImageView = (ProfImageView) findViewById(R.id.piv_prof_pic);
        mProfImageView.setEnabled(false);

        mProgressWheel = (AVLoadingIndicatorView) findViewById(R.id.aiv_loading);
        mErrLayout = (LinearLayout) findViewById(R.id.ll_err_show_rest);
        mErrMsgFtv = (FontTextView) findViewById(R.id.ftv_err_show_rest);
        mErrLayout.setVisibility(View.GONE);
        mErrLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mErrLayout.setVisibility(View.GONE);
                getRestaurantDetails();
            }
        });
        mCrdBasicInfo  = (CardView) findViewById(R.id.crd_basic_info);
        mCrdRestOffers  = (CardView) findViewById(R.id.crd_offers_info);
        mTvRestLoc = (TextView) findViewById(R.id.rest_loc);
        mTvRestDesc = (TextView) findViewById(R.id.rest_desc);
        mTvRestAddress = (TextView) findViewById(R.id.rest_address);
        mTvRestWebsite = (TextView) findViewById(R.id.contact_info_website);
        mTvRestPhone = (TextView) findViewById(R.id.contact_info_phone);
        mTvRestOffers = (TextView) findViewById(R.id.offers_info_desc);
        getRestaurantDetails();
        setupRevealBackground(savedInstanceState);

    }

    private void setupRevealBackground(Bundle savedInstanceState) {
        vRevealBackground.setOnStateChangeListener(this);
        if (savedInstanceState == null) {
            final int[] startingLocation = getIntent().getIntArrayExtra(ARG_REVEAL_START_LOCATION);
            vRevealBackground.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    vRevealBackground.getViewTreeObserver().removeOnPreDrawListener(this);
                    vRevealBackground.startFromLocation(startingLocation);
                    return false;
                }
            });
        } else {
            vBody.setVisibility(View.VISIBLE);
            mWhiteLayout.setVisibility(View.VISIBLE);
            vRevealBackground.setVisibility(View.GONE);
            vRevealBackground.setToFinishedFrame();
        }
    }
    @Override
    public void onStateChange(int state) {
        if (RevealBackgroundView.STATE_FINISHED == state) {
            if(vBody != null)
                vBody.setVisibility(View.VISIBLE);
            if(mWhiteLayout != null)
                mWhiteLayout.setVisibility(View.INVISIBLE);
            if(vRevealBackground != null)
                vRevealBackground.setVisibility(View.INVISIBLE);
            AlphaAnimation animation = new AlphaAnimation(0.1f, 1.0f);
            animation.setDuration(600);
            animation.setStartOffset(0);
            animation.setFillAfter(true);
            if(mProfImageView != null)
                mProfImageView.startAnimation(animation);
        } else {
            if(vBody != null)
                vBody.setVisibility(View.INVISIBLE);
            if(mWhiteLayout != null)
                mWhiteLayout.setVisibility(View.VISIBLE);
            if(vRevealBackground != null)
                vRevealBackground.setVisibility(View.VISIBLE);
        }
    }

    private CustomPostRequest jsonReq;
    public void getRestaurantDetails(){
        jsonReq = new CustomPostRequest(com.android.volley.Request.Method.POST,
                WAppConstants.URL_GET_SINGLE_MERCHANT_DETAILS, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "Response Single Rest: " + response.toString());
                if (response != null) {
                    try {
                        mCrdBasicInfo.setVisibility(View.VISIBLE);
                        JSONArray feedArray = response.getJSONArray("merchant_feed");
                        JSONObject resObj = (JSONObject) feedArray.get(0);
                        mRestId = resObj.getString("merchant_id")+"";
                        mRestName = resObj.getString("merchant_name")+"";
                        mRestPicId = resObj.getString("merchant_image_id")+"";
                        loadCoverPic();
                        mCollapToolbar.setTitle(mRestName);
                        String restLoc = resObj.getString("merchant_location")+"";
                        if(TextUtils.isEmpty(restLoc)) {
                            mTvRestLoc.setVisibility(View.GONE);
                        }else{
                            mTvRestLoc.setVisibility(View.VISIBLE);
                            mTvRestLoc.setText(mRestName+" - "+AppUtils.stringReplacer(restLoc));
                        }
                        String restDesc = resObj.getString("merchant_description")+"";
                        if(TextUtils.isEmpty(restDesc)) {
                            mTvRestDesc.setVisibility(View.GONE);
                        }else{
                            mTvRestDesc.setVisibility(View.VISIBLE);
                            mTvRestDesc.setText(AppUtils.stringReplacer(restDesc));
                        }
                        String restAddress = resObj.getString("merchant_address")+"";
                        if(TextUtils.isEmpty(restAddress)) {
                            mTvRestAddress.setVisibility(View.GONE);
                        }else{
                            mTvRestAddress.setVisibility(View.VISIBLE);
                            mTvRestAddress.setText(AppUtils.stringReplacer(restAddress));
                        }
                        String restWebsite = resObj.getString("merchant_website")+"";
                        if(TextUtils.isEmpty(restWebsite)) {
                            mTvRestWebsite.setVisibility(View.GONE);
                        }else{
                            mTvRestWebsite.setVisibility(View.VISIBLE);
                            mTvRestWebsite.setText(restWebsite);
                        }
                        String restPhone = resObj.getString("merchant_phone")+"";
                        if(TextUtils.isEmpty(restPhone)) {
                            mTvRestPhone.setVisibility(View.GONE);
                        }else{
                            mTvRestPhone.setVisibility(View.VISIBLE);
                            mTvRestPhone.setText("   "+restPhone);
                        }


                        String restOffers = resObj.getString("merchant_offer")+"";
                        if(TextUtils.isEmpty(restOffers)) {
                            mTvRestOffers.setVisibility(View.GONE);
                            mCrdRestOffers.setVisibility(View.GONE);
                        }else{
                            mTvRestOffers.setVisibility(View.VISIBLE);
                            mCrdRestOffers.setVisibility(View.VISIBLE);
                            mTvRestOffers.setText(""+restOffers);
                        }



                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", mPrefs.getString(WAppConstants.LOGGED_IN_USER_ID,""));
                params.put("merchant_id", mRestId);
                params.put("sec_code", mPrefs.getString(WAppConstants.LOGGED_IN_USER_SEC_CODE,""));
                return params;
            }
        };
        WAppController.getInstance().addToRequestQueue(jsonReq);
    }
    public void loadCoverPic(){
        mProfImageView.setImageUrl(String.format(WAppConstants.URL_MERCHANT_PIC, mRestPicId, 600, 600), WAppController.getInstance().getImageLoader());
        mProfImageView.setErrorImageResId(R.drawable.resturant);
        mProfImageView.setDefaultImageResId(R.drawable.resturant);
        mProfImageView.setResponseObserver(new ProfImageView.ResponseObserver() {
            @Override
            public void onError() {
                mProfImageView.setEnabled(false);
            }

            @Override
            public void onSuccess() {
                mProfImageView.setEnabled(true);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Bitmap bm = ((BitmapDrawable) mProfImageView.getDrawable()).getBitmap();
                    updateColors(bm);
                }
            }
        });
    }
    private void updateColors(Bitmap inputBitmap) {
        Palette.from(inputBitmap).maximumColorCount(32).generate(new Palette.PaletteAsyncListener() {
            @Override
            public void onGenerated(Palette palette) {
                actuallyUpdateColors(palette);
            }
        });
    }
    private int mStatusBarColr;
    @SuppressLint("NewApi")
    private void actuallyUpdateColors(Palette palette) {
        Palette.Swatch darkVibrant = palette.getDarkVibrantSwatch();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (darkVibrant != null && mCollapToolbar != null) {
            mCollapToolbar.setContentScrimColor(AppUtils.lighterHSV(darkVibrant.getRgb()));
        }
        if(darkVibrant != null) {
            mStatusBarColr = darkVibrant.getRgb();
            getWindow().setStatusBarColor(darkVibrant.getRgb());
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }
    @Override
    public void onBackPressed()	{
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
    @Override
    protected void onStart() {
        super.onStart();
    }
    @Override
    protected void onStop() {
        super.onStop();
    }
}
