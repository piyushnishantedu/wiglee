package com.cust.wiglee.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.cust.wiglee.R;
import com.cust.wiglee.adapters.AreaSpinnerAdapter;
import com.cust.wiglee.adapters.ViewResturantListAdapter;
import com.cust.wiglee.app.AppUtils;
import com.cust.wiglee.app.WAppConstants;
import com.cust.wiglee.app.WAppController;
import com.cust.wiglee.bean.MenuBean;
import com.cust.wiglee.bean.ResturantBean;
import com.cust.wiglee.view.appcompatbckport.PullRefreshLayout;
import com.cust.wiglee.view.appcompatbckport.SwipeRefreshHintLayout;
import com.cust.wiglee.view.appcompatbckport.SwipeRefreshLayoutLinear;
import com.cust.wiglee.view.common.FontTextView;
import com.cust.wiglee.volley.CustomPostRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ViewResturantActivity extends AppCompatActivity implements SwipeRefreshLayoutLinear.OnRefreshListener{
    private String TAG = WAppConstants.APP_TAG;
    private PullRefreshLayout mPullRefreshLayout;
    private ListView mListView;
    private FrameLayout mFooterView;
    private RelativeLayout mEmptyErrMsg;
    private LinearLayout mLoading;
    private FontTextView mEmptyErrMsgTv;
    private ImageView mNoConnIcon;
    private LinearLayout mFooterNoMorePost;
    private LinearLayout mFooterErrLoadingPost;
    private LinearLayout mFooterLoading;
    private ArrayList<ResturantBean> mList;
    private ViewResturantListAdapter mListAdapter;
    private SharedPreferences mPrefs;
    private int mPrevScrollY = 0;
    private int mCurrentMealsPage = 1;
    public volatile boolean isLoadingMore;
    public transient boolean isMoreEventAvailable;
    private volatile boolean mIsLoadMoreReq;
    private FrameLayout mAnchor;
    //private AppCompatEditText mETSearchTxt;
    //private ImageView mETSearchClear;
    private Spinner spinnerArea;
    private String isSearched = "N";
    private AreaSpinnerAdapter mSpinAdapter;
    ArrayList<String> mAreaList = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPrefs = WAppController.getPreferences(this);
        isLoadingMore = false;
        isMoreEventAvailable = true;
        mIsLoadMoreReq = false;
        setContentView(R.layout.activity_view_restuarant);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_view_rest);
        if (mToolbar != null) {
            mToolbar.getMenu().clear();
            mToolbar.setTitle("");
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            spinnerArea = (Spinner) findViewById(R.id.spinner_area);

            /*mETSearchTxt = (AppCompatEditText) mToolbar.findViewById(R.id.et_view_rest);
            mETSearchClear = (ImageView) mToolbar.findViewById(R.id.iv_remove_view_rest);
            mETSearchClear.setVisibility(View.GONE);
            mETSearchClear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mETSearchTxt.setText("");
                    mList.clear();
                    mListAdapter.notifyDataSetChanged();
                }
            });*/
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
        /*mETSearchTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                if (arg0.length() > 0) {
                    mETSearchClear.setVisibility(View.VISIBLE);
                } else {
                    mETSearchClear.setVisibility(View.GONE);
                }
                if (arg0.length() > 0) {
                    mCurrentMealsPage = 1;
                    isMoreEventAvailable = true;
                    mIsLoadMoreReq = false;
                    isSearched = "Y";
                    loadRest(mIsLoadMoreReq, false, false);
                } else if (arg0.length() == 0) {
                    mCurrentMealsPage = 1;
                    mList.clear();
                    mListAdapter.notifyDataSetChanged();
                    isSearched = "N";
                    loadRest(mIsLoadMoreReq, false, false);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {
            }
        });*/

        mSpinAdapter = new AreaSpinnerAdapter(getApplicationContext(), mAreaList,false);
        spinnerArea.setAdapter(mSpinAdapter);
        spinnerArea.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapter, View v,int position, long id) {
                // On selecting a spinner item
                String item = adapter.getItemAtPosition(position).toString();
                if(item.equals("All Location"))
                    isSearched = "N";
                else
                    isSearched = "Y";


                mCurrentMealsPage = 1;
                isMoreEventAvailable = true;
                mIsLoadMoreReq = false;
                loadRest(mIsLoadMoreReq, false, false);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });
        addItemsToSpinner();
        mAnchor = (FrameLayout) findViewById(R.id.view_rest_anchor);
        mEmptyErrMsg = (RelativeLayout) findViewById(R.id.view_rest_no_conn);
        mEmptyErrMsgTv = (FontTextView) findViewById(R.id.ftv_no_conn_msg_view_rest);
        mNoConnIcon = (ImageView) findViewById(R.id.iv_no_conn_view_rest);
        mLoading = (LinearLayout) findViewById(R.id.loading_view_rest);
        mListView = (ListView) findViewById(R.id.view_rest_listt);
        mFooterView = (FrameLayout) LayoutInflater.from(this).inflate(R.layout.meals_footer, null);
        mFooterNoMorePost = (LinearLayout) mFooterView.findViewById(R.id.ll_no_more_event);
        mFooterLoading = (LinearLayout) mFooterView.findViewById(R.id.ll_load_more_event);
        mFooterErrLoadingPost = (LinearLayout) mFooterView.findViewById(R.id.ll_err_load_event);
        mFooterErrLoadingPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isMoreEventAvailable = true;
                isLoadingMore = true;
                mIsLoadMoreReq = true;
                loadRest(mIsLoadMoreReq, false, false);
            }
        });
        mFooterLoading.setVisibility(View.GONE);
        mListView.addFooterView(mFooterView, null, false);
        mList = new ArrayList<ResturantBean>();
        mListAdapter = new ViewResturantListAdapter(this, mList,mAnchor);
        mListView.setAdapter(mListAdapter);
        mPullRefreshLayout = (PullRefreshLayout) findViewById(R.id.ptr_layout_view_rest);
        mPullRefreshLayout.setOnRefreshListener(this);
        mPullRefreshLayout.setRefreshListView(mListView);
        mPullRefreshLayout.setColorScheme(R.color.holo_purple_dark, R.color.holo_blue_bright, R.color.holo_red_light, R.color.holo_green_dark);
        SwipeRefreshHintLayout mSwipeRefreshHintLayout = (SwipeRefreshHintLayout) findViewById(R.id.swipe_hint_view_rest);
        mSwipeRefreshHintLayout.setSwipeLayoutTarget(mPullRefreshLayout);
        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int scrollY = AppUtils.getScrollY(mListView);
                int diff = mPrevScrollY - scrollY;
                if (diff != 0) {
                    //what is the bottom iten that is visible
                    int lastInScreen = firstVisibleItem + visibleItemCount;
                    //is the bottom item visible & not loading more already ? Load more
                    if (AppUtils.isNetworkAvailable(ViewResturantActivity.this)) {
                        if ((lastInScreen == totalItemCount) && !(isLoadingMore)) {
                            onLoadingMore();
                        }
                    } else {
                        if ((lastInScreen == totalItemCount) && !(isLoadingMore)) {
                            showPopupErrMsg();
                        } else {

                        }
                    }
                }
                mPrevScrollY = scrollY;
            }
        });

        loadRest(mIsLoadMoreReq, false, false);
        mEmptyErrMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppUtils.isNetworkAvailable(ViewResturantActivity.this)) {
                    mEmptyErrMsg.setVisibility(View.GONE);
                    mLoading.setVisibility(View.VISIBLE);
                    loadRest(false, false, true);
                }
            }
        });
    }

    public void addItemsToSpinner() {
        CustomPostRequest jsosReq = new CustomPostRequest(Request.Method.POST,
                WAppConstants.URL_GET_ALL_AREAS, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "URL_GET_ALL_AREAS Response: " + response.toString());
                if (response != null) {
                    JSONArray postJsonArray = null;
                    mAreaList.add("All Location");
                    try {
                        postJsonArray = response.getJSONArray("area_array");
                        if(postJsonArray.length()>0) {
                            for (int i = 0; i < postJsonArray.length(); i++) {
                                String area = (String) postJsonArray.get(i);
                                mAreaList.add(area);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mSpinAdapter.notifyDataSetChanged();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", mPrefs.getString(WAppConstants.LOGGED_IN_USER_ID,"1"));
                params.put("sec_code", mPrefs.getString(WAppConstants.LOGGED_IN_USER_SEC_CODE, "ASD"));
                return params;
            }
        };
        WAppController.getInstance().addToRequestQueue(jsosReq);
    }

    public void onLoadingMore() {
        ViewResturantActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!isLoadingMore && isMoreEventAvailable) {
                    isMoreEventAvailable = true;
                    isLoadingMore = true;
                    mIsLoadMoreReq = true;
                    loadRest(mIsLoadMoreReq, false, false);
                }
            }
        });
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mCurrentMealsPage = 1;
                isMoreEventAvailable = true;
                mIsLoadMoreReq = false;
                loadRest(mIsLoadMoreReq, true, false);
            }
        }, 100);
    }

    public void showPopupErrMsg(){
        mFooterErrLoadingPost.setVisibility(View.VISIBLE);
        mFooterNoMorePost.setVisibility(View.GONE);
        mFooterLoading.setVisibility(View.GONE);
    }

    private void loadRest(final boolean isLoadMoreReq, final boolean isRefreshReq, final boolean isAllEventReq) {
        if(isLoadMoreReq){
            mFooterLoading.setVisibility(View.VISIBLE);
            mFooterNoMorePost.setVisibility(View.GONE);
            mFooterErrLoadingPost.setVisibility(View.GONE);
        }else{
            if(isRefreshReq){
                mLoading.setVisibility(View.GONE);
                mFooterErrLoadingPost.setVisibility(View.GONE);
            }else{
                mLoading.setVisibility(View.VISIBLE);
                mFooterErrLoadingPost.setVisibility(View.GONE);
            }
            if(isAllEventReq) {
                mLoading.setVisibility(View.VISIBLE);
                mList.clear();
                mListAdapter.notifyDataSetChanged();
            }
            mEmptyErrMsg.setVisibility(View.GONE);
            mCurrentMealsPage = 1;
            mIsLoadMoreReq = false;
        }
        Log.d(TAG, "URL(POST): " + WAppConstants.URL_GET_ALL_MERCHANTS+":: "+WAppConstants.URL_GET_ALL_MERCHANTS);
        CustomPostRequest jsonAllMealsReq = new CustomPostRequest(Request.Method.POST,
                WAppConstants.URL_GET_ALL_MERCHANTS, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "URL_GET_ALL_MERCHANTS Response: " + response.toString());
                if (response != null) {
                    parseJsonResponse(response, isLoadMoreReq);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(!isRefreshReq){
                    if(isLoadMoreReq){
                        mFooterErrLoadingPost.setVisibility(View.VISIBLE);
                        mFooterNoMorePost.setVisibility(View.GONE);
                        mFooterLoading.setVisibility(View.GONE);
                    }else{
                        mLoading.setVisibility(View.GONE);
                        mEmptyErrMsg.setVisibility(View.VISIBLE);
                        mNoConnIcon.setVisibility(View.VISIBLE);
                        mEmptyErrMsgTv.setText(getResources().getString(R.string.no_conn_retry));
                        mFooterLoading.setVisibility(View.GONE);
                        mFooterNoMorePost.setVisibility(View.GONE);
                        mFooterErrLoadingPost.setVisibility(View.GONE);
                    }
                }else{
                    mPullRefreshLayout.setRefreshing(false);
                    Snackbar.make(mAnchor, "Failed to refresh", Snackbar.LENGTH_SHORT).show();
                    mFooterErrLoadingPost.setVisibility(View.VISIBLE);
                    mFooterNoMorePost.setVisibility(View.GONE);
                    mFooterLoading.setVisibility(View.GONE);
                    isLoadingMore = false;
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                if(isLoadMoreReq){
                    mCurrentMealsPage = mCurrentMealsPage +1;
                    params.put("page", mCurrentMealsPage + "");
                    mCurrentMealsPage = mCurrentMealsPage -1;
                }else{
                    params.put("page", mCurrentMealsPage +"");
                }
                //params.put("typed_location",  mETSearchTxt.getText()+"");
                params.put("typed_location", spinnerArea.getSelectedItem()+"");
                params.put("is_searched", isSearched);
                params.put("user_id", mPrefs.getString(WAppConstants.LOGGED_IN_USER_ID,"1"));
                params.put("sec_code", mPrefs.getString(WAppConstants.LOGGED_IN_USER_SEC_CODE, "ASD"));
                return params;
            }
        };
        if(AppUtils.isNetworkAvailable(this)){
            WAppController.getInstance().addToRequestQueue(jsonAllMealsReq);
        }else{
            if(isRefreshReq){
                mPullRefreshLayout.setRefreshing(false);
                mFooterErrLoadingPost.setVisibility(View.VISIBLE);
                mFooterNoMorePost.setVisibility(View.GONE);
                mFooterLoading.setVisibility(View.GONE);
                isLoadingMore = false;
            }else{
                mLoading.setVisibility(View.GONE);
                if(mList.size() == 0){
                    mEmptyErrMsg.setVisibility(View.VISIBLE);
                    mNoConnIcon.setVisibility(View.VISIBLE);
                    mEmptyErrMsgTv.setText(getResources().getString(R.string.no_conn_retry));
                }else{
                    mFooterLoading.setVisibility(View.GONE);
                    mFooterNoMorePost.setVisibility(View.GONE);
                    mFooterErrLoadingPost.setVisibility(View.VISIBLE);
                }
            }
        }
    }
    private void parseJsonResponse(JSONObject response, boolean isLoadMoreReq) {
        try {
            if(!isLoadMoreReq){
                mList.clear();
                mListAdapter.notifyDataSetChanged();
            }
            JSONArray postJsonArray = response.getJSONArray("merchants_feed");
            if(postJsonArray.length()>0){
                for (int i = 0; i < postJsonArray.length(); i++) {
                    JSONObject feedObj = (JSONObject) postJsonArray.get(i);
                    ResturantBean item = new ResturantBean();
                    item.setMerchantId(feedObj.getInt("merchant_id"));
                    item.setMerchantName(feedObj.getString("merchant_name"));
                    item.setMerchantLoc(feedObj.getString("merchant_location"));
                    item.setMerchantWebsite(feedObj.getString("merchant_website"));
                    item.setMerchantPh(feedObj.getString("merchant_phone"));
                    item.setMerchantDesc(feedObj.getString("merchant_description"));
                    item.setMerchantImageId(feedObj.getInt("merchant_image_id"));
                    item.setMerchantImageAR(feedObj.getDouble("image_aspect_ratio"));
                    mList.add(item);
                }
                //mFooterLoading.setVisibility(View.VISIBLE);
                mFooterNoMorePost.setVisibility(View.GONE);
                mFooterErrLoadingPost.setVisibility(View.GONE);
                mPullRefreshLayout.setRefreshing(false);
            }else{
                isMoreEventAvailable = false;
                mFooterLoading.setVisibility(View.GONE);
                mFooterNoMorePost.setVisibility(View.VISIBLE);
                mFooterErrLoadingPost.setVisibility(View.GONE);
            }
            if(mList.size() < 1){
                mFooterLoading.setVisibility(View.GONE);
                mFooterNoMorePost.setVisibility(View.GONE);
                mFooterErrLoadingPost.setVisibility(View.GONE);
                mLoading.setVisibility(View.GONE);
                mEmptyErrMsg.setVisibility(View.VISIBLE);
                mNoConnIcon.setVisibility(View.GONE);
                mEmptyErrMsgTv.setText("0pss!! No Restuarant found");
            }else{
                mLoading.setVisibility(View.GONE);
                mEmptyErrMsg.setVisibility(View.GONE);
            }
            mListAdapter.notifyDataSetChanged();
            isLoadingMore = false;
            if(isLoadMoreReq) {
                mCurrentMealsPage = mCurrentMealsPage + 1;
            }else{
                //mListView.smoothScrollToPosition(0);
            }
        } catch (JSONException e) {
            isLoadingMore = false;
            mLoading.setVisibility(View.GONE);
            mPullRefreshLayout.setRefreshing(false);
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        isLoadingMore = false;
        //GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }
    @Override
    protected void onStop() {
        super.onStop();
        //GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }
    //private SweetAlertDialog mSADialog;
    @Override
    public void onDestroy(){
        /*if(mBottomSheet != null && mBottomSheet.isShowing()){
            mBottomSheet.dismiss();
        }
        if(mSADialog != null && mSADialog.isShowing()){
            mSADialog.dismiss();
        }*/
        super.onDestroy();
    }
    @Override
    protected void onResume() {
        isLoadingMore = false;
        super.onResume();
    }
    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        //Intent intent = new Intent();
        //intent.putExtra("request_approved", mRequestApproved);
        //setResult(RESULT_OK, intent);
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

}