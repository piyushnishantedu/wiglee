package com.cust.wiglee.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.cust.wiglee.R;
import com.cust.wiglee.app.AppUtils;
import com.cust.wiglee.app.WAppConstants;
import com.cust.wiglee.app.WAppController;
import com.cust.wiglee.listner.ClickTouchListner;
import com.cust.wiglee.view.common.ListAnimImageView;
import com.cust.wiglee.volley.CustomPostRequest;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

@SuppressLint("ClickableViewAccessibility") 
public class ChangePwdActivity extends AppCompatActivity {
	private String TAG = WAppConstants.APP_TAG;
	private SharedPreferences mPrefs;
    private SharedPreferences.Editor mPrefEditor;
	private SwipeRefreshLayout mPullRefreshLayout;
	private EditText mEtCurrntPwd;
	private EditText mEtNewPwd; 
	private EditText mEtReNewPwd;
	private String mStringCurrntPwd = null;
	private String mStringNewPwd = null;
	private String mStringReNewPwd = null;	
	private ListAnimImageView mCurrntPwdToggle;
	private ListAnimImageView mNewPwdToggle;
	private ListAnimImageView mRePwdChk;
	private RelativeLayout mRootLayout;
    private LinearLayout mLoading;
	private boolean mIsChngePwdRequestRunning;	
	private boolean isCheckedCrrntPwd;	
	private boolean isCheckedNewPwd;	
	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);  
        setContentView(R.layout.activity_change_pwd);
        mPrefs = WAppController.getPreferences(this);
        mPrefEditor  = WAppController.getPreferences(this).edit();
        mIsChngePwdRequestRunning = false;	
        isCheckedCrrntPwd = true;	
        isCheckedNewPwd = true;

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_chnge_pwd);
        if (mToolbar != null) {
            mToolbar.setTitle("Change password");
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mToolbar.setNavigationOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
        mLoading = (LinearLayout) findViewById (R.id.ll_loading_change_pwd);
        mRootLayout = (RelativeLayout) findViewById (R.id.rl_chnge_pwd_root);
        mPullRefreshLayout = (SwipeRefreshLayout)findViewById (R.id.ptr_chnge_pwd);
		mPullRefreshLayout.setColorSchemeResources(R.color.holo_blue_bright, R.color.red_btn_bg_color, R.color.holo_red_light, R.color.holo_green_dark);

        mPullRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPullRefreshLayout.setRefreshing(false);
            }
        });
        mEtCurrntPwd = (EditText) findViewById(R.id.et_currnt_pwd);
		mEtNewPwd = (EditText) findViewById(R.id.et_new_pwd);				
		mEtReNewPwd = (EditText) findViewById(R.id.et_re_new_pwd);	 
		Button mChngePwd = (Button) findViewById(R.id.btn_chnge_pwd);
		mChngePwd.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				mStringCurrntPwd = mEtCurrntPwd.getText()+"";	
				mStringNewPwd = mEtNewPwd.getText()+"";		
				mStringReNewPwd = mEtReNewPwd.getText()+"";						
				if(mStringCurrntPwd != null && mStringNewPwd != null && mStringReNewPwd != null &&
						!TextUtils.isEmpty(mStringCurrntPwd)&& !TextUtils.isEmpty(mStringNewPwd)&& !TextUtils.isEmpty(mStringReNewPwd)){							
					if(mStringNewPwd.equals(mStringReNewPwd)){								
						int pwdLength = (mStringNewPwd+"").length();
						 if(pwdLength > 5 && pwdLength < 11){
							 doPwdChangeInDb(mRootLayout,mStringCurrntPwd,mStringNewPwd,mStringReNewPwd);	
							 mPullRefreshLayout.setRefreshing(true);	
						 }else{
							showPopupErrMsg("Password must be between 6 and 10 characters");
						 }													
					}else{								
						showPopupErrMsg("New password does not match with re-entered password");
					}
				}else{
					showPopupErrMsg("All the fields are required.");
				}											
			}
		});
		
		mCurrntPwdToggle = (ListAnimImageView) findViewById(R.id.aiv_chng_pwd_crnt_toggle);
		mCurrntPwdToggle.setOnTouchListener(mCurrntPwdToggle);
		mCurrntPwdToggle.setOnClickTouchListener(new ClickTouchListner() {
	        @Override
	        public void onTouchComplete(){	 
	        	int pos = mEtCurrntPwd.getSelectionEnd();
	        	if(isCheckedCrrntPwd) {
	        		mEtCurrntPwd.setTransformationMethod(null);
	        		mCurrntPwdToggle.setImageResource(R.drawable.ic_visibility_white);
	        		isCheckedCrrntPwd = false;
                } else {
                	mEtCurrntPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());                	
                	mCurrntPwdToggle.setImageResource(R.drawable.ic_visibility_off_white);
                	isCheckedCrrntPwd = true;
                }
	        	mEtCurrntPwd.setSelection(pos);
	        }
	    });		
		
		mRePwdChk = (ListAnimImageView) findViewById(R.id.aiv_chng_pwd_re_new_toggle);
		mEtReNewPwd.addTextChangedListener(new TextWatcher() {
	          public void afterTextChanged(Editable s) {
	        	  mStringNewPwd = mEtNewPwd.getText()+"";	
	        	  mStringReNewPwd = mEtReNewPwd.getText()+"";
	        	  int pwdLength = (mStringNewPwd+"").length();
	        	  if(pwdLength > 5 && pwdLength < 11){
		        		if(mStringNewPwd.equals(mStringReNewPwd)){				        			
		        			mRePwdChk.setVisibility(View.VISIBLE);			        			
		        		}else{
		        			mRePwdChk.setVisibility(View.INVISIBLE);
		        		}	
					}else{
	        			mRePwdChk.setVisibility(View.INVISIBLE);
	        		}	
	          }
	          public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
	          public void onTextChanged(CharSequence s, int start, int before, int count) {}
	       });
		
		mEtNewPwd.addTextChangedListener(new TextWatcher() {
	          public void afterTextChanged(Editable s) {
	        	  mStringNewPwd = mEtNewPwd.getText()+"";	
	        	  mStringReNewPwd = mEtReNewPwd.getText()+"";
	        	  int pwdLength = (mStringNewPwd+"").length();
	        	  if(pwdLength > 5 && pwdLength < 11){
		        		if(mStringNewPwd.equals(mStringReNewPwd)){				        			
		        			mRePwdChk.setVisibility(View.VISIBLE);			        			
		        		}else{
		        			mRePwdChk.setVisibility(View.INVISIBLE);
		        		}	
					}else{
	        			mRePwdChk.setVisibility(View.INVISIBLE);
	        		}	
	          }
	          public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
	          public void onTextChanged(CharSequence s, int start, int before, int count) {}
	       });
		mNewPwdToggle = (ListAnimImageView) findViewById(R.id.aiv_chng_pwd_new_toggle);
		mNewPwdToggle.setOnTouchListener(mNewPwdToggle);
		mNewPwdToggle.setOnClickTouchListener(new ClickTouchListner() {
	        @Override
	        public void onTouchComplete(){
	        	int posNew = mEtNewPwd.getSelectionStart();
	        	int posNewRe = mEtReNewPwd.getSelectionStart();
	        	if(isCheckedNewPwd) {
	        		mEtNewPwd.setTransformationMethod(null);
                	mEtReNewPwd.setTransformationMethod(null);
	        		mNewPwdToggle.setImageResource(R.drawable.ic_visibility_white);
	        		isCheckedNewPwd = false;
                } else {
                	mEtNewPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
	        		mEtReNewPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
                	mNewPwdToggle.setImageResource(R.drawable.ic_visibility_off_white);
                	isCheckedNewPwd = true;
                }
	        	mEtNewPwd.setSelection(posNew);
	        	mEtReNewPwd.setSelection(posNewRe);
	        }
	    });	
    }
	
	protected String doPwdChangeInDb(final RelativeLayout mRootLayout, final String mStringCurrntPwd, final String mStringNewPwd, final String mStringReNewPwd) {
		CustomPostRequest jsonReqFrgtPwd = new CustomPostRequest(Method.POST,
				WAppConstants.URL_CHANGE_PWD, null, new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {
						mIsChngePwdRequestRunning = false;
						Log.d(TAG, "Response change pwd: " + response.toString());
						mPullRefreshLayout.setRefreshing(false);
						if (response != null) {									
							try {										
								String msg = response.getString("status");
                                if("error".equals(msg)){
                                    showPopupErrMsg("Something went wrong!! Please try again.");
                                }else{
                                    showPopupErrMsg(msg);
                                    doShowRelogin();
                                }
							} catch (JSONException e) {e.printStackTrace();}
						}
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						mIsChngePwdRequestRunning = false;
						mIsChngePwdRequestRunning = false;
						mPullRefreshLayout.setRefreshing(false);
						Log.e(TAG, "Error: " + error.getMessage());
						showPopupErrMsg("Something went wrong!! Please try again.");
					}
				}){	
					@Override
			         protected Map<String, String> getParams() throws AuthFailureError {
			            Map<String, String> params = new HashMap<String, String>();	            
			            params.put("user_id", mPrefs.getString(WAppConstants.LOGGED_IN_USER_ID, ""));
			            params.put("entered_current_password", mStringCurrntPwd);
			            params.put("new_password", mStringNewPwd);
			            params.put("confirn_new_password", mStringReNewPwd);
			            params.put("sec_code", mPrefs.getString(WAppConstants.LOGGED_IN_USER_SEC_CODE,""));
			            return params;
			         }
		};		
		if(AppUtils.isNetworkAvailable(this) && !mIsChngePwdRequestRunning){
			WAppController.getInstance().addToRequestQueue(jsonReqFrgtPwd);
			mIsChngePwdRequestRunning = true;
		}else{
			showPopupErrMsg("No connection available. Please try again later.");
		}	
		return null;
	}
	private void showPopupErrMsg(String msg){
        Snackbar.make(findViewById(android.R.id.content), msg, Snackbar.LENGTH_SHORT).show();
	}
	@Override
	public void onBackPressed(){
		finish();
		overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
	}
    private SweetAlertDialog mSADialog;
    @Override
    public void onDestroy(){
        if(mSADialog != null && mSADialog.isShowing()){
            mSADialog.dismiss();
        }
        super.onDestroy();
    }
    private void doShowRelogin() {
        mSADialog = new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE);
        mSADialog.setTitleText("Password changed successfully");
        mSADialog.setContentText("    ");
        mSADialog.setConfirmText("Re-Login");
        mSADialog.showCancelButton(false);
        mSADialog.setCancelable(false);
        mSADialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(final SweetAlertDialog sDialog) {
                sDialog.dismiss();
                doLogout();
            }
        });
        mSADialog.show();
    }

    private void doLogout() {
            mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_ID, null);
            mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_NAME, null);
            mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_EMAIL, null);
            mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_SEC_CODE, null);
            mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_PROF_PIC_ID, null);
            mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_AGE, null);
            mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_GENDER, null);
            mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_ADDRESS, null);
            mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_ABT, null);
            mPrefEditor.putString(WAppConstants.IS_USER_VALIDATED, null);
            mPrefEditor.clear();
            mPrefEditor.commit();
            mPrefEditor.clear();
            ActivityCompat.finishAffinity(ChangePwdActivity.this);
            ChangePwdActivity.this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            Intent loginIntent = new Intent(ChangePwdActivity.this, WLoginActivity.class);
            loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            ChangePwdActivity.this.startActivity(loginIntent);
    }
}
