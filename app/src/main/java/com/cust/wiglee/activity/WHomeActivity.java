package com.cust.wiglee.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.internal.NavigationMenuView;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.cust.wiglee.R;
import com.cust.wiglee.app.AppUtils;
import com.cust.wiglee.app.WAppConstants;
import com.cust.wiglee.app.WAppController;
import com.cust.wiglee.view.common.FontTextView;
import com.cust.wiglee.view.common.font.CustomTypefaceSpan;
import com.cust.wiglee.volley.CircularNetworkImageView;
import com.cust.wiglee.volley.CustomPostRequest;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions.Builder;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class WHomeActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{
    private String TAG = "APP_TAG";
    //private CoordinatorLayout mRoot;
    private TextView mToolBarTitleTv;
    private DrawerLayout mDrawer;
    private ActionBarDrawerToggle mDrawerToggle;

    private LinearLayout mNavheadLoading;
    private LinearLayout mNavheadNoConn;
    private TextView mNavheadTitle;
    private TextView mNavheadSubTitle;
    private CircularNetworkImageView mNavheadAvatar;
    private RelativeLayout mEmptyErrMsg;
    private LinearLayout mLoading;
    private FontTextView mEmptyErrMsgTv;
    private ImageView mNoConnIcon;
    private SharedPreferences mPrefs;
    private SharedPreferences.Editor mPrefEditor;
    //private BottomSheet mBottomSheet;
    //private LinearLayout mAnchor;
    private LinearLayout mOrderMeals;
    private LinearLayout mViewRest;

    /*
        My Edit- Kishan Singh
     */

    GoogleApiClient mGoogleApiClient;
    boolean mSignInClicked;



    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPrefs = WAppController.getPreferences(this);
        mPrefEditor  = WAppController.getPreferences(this).edit();
        setContentView(R.layout.activity_whome);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_home);
        if (mToolbar != null) {
            mToolbar.getMenu().clear();
            mToolbar.setTitle("");
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        //mAnchor = (LinearLayout) findViewById(R.id.root_home);
        mToolBarTitleTv = (TextView) mToolbar.findViewById(R.id.toolbar_home_title);
        setToolBarTitle("Wiglee ");
        //mRoot =  (CoordinatorLayout) findViewById(R.id.root_home);
        mEmptyErrMsg = (RelativeLayout) findViewById(R.id.home_no_conn);
        mEmptyErrMsgTv = (FontTextView) findViewById(R.id.ftv_no_conn_msg_home);
        mNoConnIcon = (ImageView) findViewById(R.id.iv_no_conn_home);
        mLoading = (LinearLayout) findViewById(R.id.loading_home);

        /*
           My code-Kishan Singh
         */

        GoogleSignInOptions gso = new Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        // ATTENTION: This "addApi(AppIndex.API)"was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso).build();

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(
                this, mDrawer, mToolbar, R.string.blank, R.string.blank){
            public void onDrawerClosed(View view) {

            }
            public void onDrawerOpened(View drawerView) {

            }
        };
        mDrawer.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        //disableNavigationViewScrollbars(navigationView);
        View header = navigationView.getHeaderView(0);
        mNavheadLoading = (LinearLayout) header.findViewById(R.id.loading_nav);
        mNavheadNoConn = (LinearLayout) header.findViewById(R.id.no_conn_nav);
        mNavheadNoConn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadProfileDetails();
            }
        });

        mNavheadTitle = (TextView) header.findViewById(R.id.nav_head_title);
        mNavheadTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppUtils.isNetworkAvailable(WHomeActivity.this)) {
                    Intent myProfIntent = new Intent(WHomeActivity.this, MyProfEditActivity.class);
                    WHomeActivity.this.startActivityForResult(myProfIntent,100);
                    WHomeActivity.this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                } else {
                    Snackbar mSnackbar = Snackbar.make(findViewById(R.id.root_home), R.string.no_connection, Snackbar.LENGTH_LONG);
                    mSnackbar.show();
                }
            }
        });
        mNavheadSubTitle = (TextView) header.findViewById(R.id.nav_head_sub_title);
        mNavheadSubTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppUtils.isNetworkAvailable(WHomeActivity.this)) {
                    Intent myProfIntent = new Intent(WHomeActivity.this, MyProfEditActivity.class);
                    WHomeActivity.this.startActivityForResult(myProfIntent,100);
                    WHomeActivity.this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                } else {
                    Snackbar mSnackbar = Snackbar.make(findViewById(R.id.root_home), R.string.no_connection, Snackbar.LENGTH_LONG);
                    mSnackbar.show();
                }
            }
        });
        mNavheadAvatar = (CircularNetworkImageView) header.findViewById(R.id.nav_head_avatar);
        mNavheadAvatar.setImageResource(R.drawable.default_avatar_thumb);
        mNavheadAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!("" + mPrefs.getString(WAppConstants.LOGGED_IN_USER_PROF_PIC_ID, "0")).equalsIgnoreCase("0")) {
                    if (AppUtils.isNetworkAvailable(WHomeActivity.this)) {
                        openProfPicActivity();
                    } else {
                        Snackbar mSnackbar = Snackbar.make(findViewById(R.id.root_home), R.string.no_connection, Snackbar.LENGTH_LONG);
                        mSnackbar.show();
                    }
                }
            }
        });
        mOrderMeals = (LinearLayout) findViewById(R.id.ll_order_meals);
        mViewRest = (LinearLayout) findViewById(R.id.ll_order_lunch);
        mOrderMeals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Intent mealsIntent = new Intent(WHomeActivity.this, TodaysMenuActivity.class);
                WHomeActivity.this.startActivity(mealsIntent);
                WHomeActivity.this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);*/
                Intent mealsIntent = new Intent(WHomeActivity.this, FoodTabbarActivity.class);
                WHomeActivity.this.startActivity(mealsIntent);
                WHomeActivity.this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
        mViewRest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent restIntent = new Intent(WHomeActivity.this, ViewResturantActivity.class);
                WHomeActivity.this.startActivity(restIntent);
                WHomeActivity.this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);*/
            }
        });

        loadProfileDetails();
        disableNavigationViewScrollbars(navigationView);
    }


    private CustomPostRequest jsonGetMyProfDetailsReq;
    public void loadProfileDetails(){
        mNavheadLoading.setVisibility(View.VISIBLE);
        mNavheadNoConn.setVisibility(View.GONE);
        jsonGetMyProfDetailsReq = new CustomPostRequest(Request.Method.POST,
                WAppConstants.URL_MY_PROFILE_DETAILS, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "Response My prof: " + response.toString());
                if (response != null) {
                    try {
                        JSONArray feedArray = response.getJSONArray("user_profile_details");
                        JSONObject profileObj = (JSONObject) feedArray.get(0);
                        mNavheadTitle.setText(profileObj.getString("user_full_name"));
                        mNavheadSubTitle.setText(profileObj.getString("user_email"));
                        mNavheadAvatar.setImageResource(R.drawable.default_avatar);
                        mNavheadAvatar.setImageUrl(String.format(WAppConstants.URL_PROF_PIC, profileObj.getInt("user_prof_pic_id"), AppUtils.dpToPx(100), AppUtils.dpToPx(100)),
                                WAppController.getInstance().getImageLoader());
                        mNavheadAvatar.setErrorImageResId(R.drawable.default_avatar);
                        mNavheadAvatar.setDefaultImageResId(R.drawable.default_avatar);
                        mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_ID, profileObj.getInt("user_id") + "");
                        mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_NAME, profileObj.getString("user_full_name"));
                        mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_MOBILE_NO, profileObj.getString("user_mobile_number"));
                        mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_EMAIL, profileObj.getString("user_email"));
                        mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_PROF_PIC_ID, profileObj.getInt("user_prof_pic_id")+"");
                        mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_DOB, profileObj.getString("user_dob"));
                        mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_GENDER, profileObj.getString("user_gender"));
                        mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_ADDRESS, profileObj.getString("user_address"));
                        mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_ADDRESS_OFFICE, profileObj.getString("user_second_address"));
                        mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_ABT, profileObj.getString("user_abt"));
                        mPrefEditor.commit();
                        String secToken = profileObj.getString("sec_code");
                        if(!mPrefs.getString(WAppConstants.LOGGED_IN_USER_SEC_CODE,"asd").equals(secToken)){
                            doShowRelogin();
                        }
                        mNavheadLoading.setVisibility(View.GONE);
                        mNavheadNoConn.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                mNavheadLoading.setVisibility(View.GONE);
                mNavheadNoConn.setVisibility(View.VISIBLE);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", mPrefs.getString(WAppConstants.LOGGED_IN_USER_ID,""));
                params.put("sec_code", mPrefs.getString(WAppConstants.LOGGED_IN_USER_SEC_CODE,""));
                return params;
            }
        };
        WAppController.getInstance().addToRequestQueue(jsonGetMyProfDetailsReq);
    }

    private void doShowRelogin() {
        mSADialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
        mSADialog.setTitleText("Password changed");
        mSADialog.setContentText("It seems you have changed your account password from a different device. Please login again");
        mSADialog.setConfirmText("Re-Login");
        mSADialog.showCancelButton(false);
        mSADialog.setCancelable(false);
        mSADialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(final SweetAlertDialog sDialog) {
                sDialog.dismiss();
                doLogout();
            }
        });
        mSADialog.show();
    }
    private void openProfPicActivity() {
        Intent profPicIntent = new Intent(this, ProfilePicActivity.class);
        profPicIntent.putExtra("image_url", String.format(WAppConstants.URL_PROF_PIC, mPrefs.getString(WAppConstants.LOGGED_IN_USER_PROF_PIC_ID, "0"), 800, 800));
        profPicIntent.putExtra("play_exit_anim", false);
        this.startActivity(profPicIntent);
        this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_my_profile) {
            Intent myProfIntent = new Intent(this, MyProfEditActivity.class);
            this.startActivityForResult(myProfIntent,100);
            this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        } if (id == R.id.nav_address) {
            Intent myAddressIntent = new Intent(this, MyAddressActivity.class);
            this.startActivity(myAddressIntent);
            this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        } if (id == R.id.nav_change_pwd) {
            Intent changePwdIntent = new Intent(this, ChangePwdActivity.class);
            this.startActivity(changePwdIntent);
            this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        } else if (id == R.id.nav_my_cart) {
            if(AppUtils.isNetworkAvailable(WHomeActivity.this)){
                Intent myCartIntent = new Intent(WHomeActivity.this, MyCartActivity.class);
                WHomeActivity.this.startActivity(myCartIntent);
                WHomeActivity.this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }else{
                Snackbar mSnackbar = Snackbar.make(findViewById(R.id.root_home), R.string.no_connection, Snackbar.LENGTH_LONG);
                mSnackbar.show();
            }
        } else if (id == R.id.nav_orders) {
            Intent myOrderIntent = new Intent(this, MyOrderActivity.class);
            this.startActivity(myOrderIntent);
            this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        } else if (id == R.id.nav_logout) {
            doLogout();
        } /*else if (id == R.id.nav_help) {
            Intent webIntent = new Intent(this, WWebViewActivity.class);
            webIntent.putExtra("passed_url", "file:///android_asset/wiglee_help.html");
            webIntent.putExtra("passed_title", getString(R.string.nav_head_help));
            this.startActivity(webIntent);
            this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        } else if (id == R.id.nav_about) {
            Intent webIntent = new Intent(this, WWebViewActivity.class);
            webIntent.putExtra("passed_url", "file:///android_asset/wiglee_about.html");
            webIntent.putExtra("passed_title", getString(R.string.nav_head_help));
            this.startActivity(webIntent);
            this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }*/ else if (id == R.id.nav_contact_us) {
            Intent webIntent = new Intent(this, WWebViewActivity.class);
            webIntent.putExtra("passed_url", "file:///android_asset/wiglee_contact.html");
            webIntent.putExtra("passed_title", getString(R.string.nav_head_contact_us));
            this.startActivity(webIntent);
            this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }
        mDrawer.closeDrawer(GravityCompat.START);
        return true;
    }
    private SweetAlertDialog mSADialog;
    @Override
    public void onDestroy(){
        if(mSADialog != null && mSADialog.isShowing()){
            mSADialog.dismiss();
        }
        super.onDestroy();
    }
    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
        }else {
            super.onBackPressed();
        }
    }
    private void doLogout() {
        mLoading.setVisibility(View.VISIBLE);
        mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_ID, null);
        mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_NAME, null);
        mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_EMAIL, null);
        mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_SEC_CODE, null);
        mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_PROF_PIC_ID, null);
        mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_AGE, null);
        mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_GENDER, null);
        mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_ADDRESS, null);
        mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_ADDRESS_OFFICE, null);
        mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_ABT, null);
        mPrefEditor.putString(WAppConstants.IS_USER_VALIDATED, null);

        if (mGoogleApiClient.isConnected()) {
            Log.i("Google logout", "Yes");
            Auth.GoogleSignInApi.signOut(mGoogleApiClient);
            mGoogleApiClient.disconnect();
            //mGoogleApiClient.connect();
        }
        if (FacebookSdk.isInitialized()){

            LoginManager.getInstance().logOut();
        }


        mPrefEditor.clear();
        mPrefEditor.commit();
        Intent loginIntent = new Intent(WHomeActivity.this,WLoginActivity.class);
        this.startActivity(loginIntent);
        finish();
        this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 100: {// ShowComment Activity
                if (resultCode == RESULT_OK) {
                    loadProfileDetails();
                }
                break;
            }
        }
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }
    private void setToolBarTitle(String title) {
        SpannableStringBuilder sb = new SpannableStringBuilder(title);
        sb.setSpan(new CustomTypefaceSpan(this, "Roboto-Bold.ttf"), 0, sb.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        sb.setSpan(new ForegroundColorSpan(Color.parseColor("#ffffff")), 0, sb.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        sb.setSpan(new AbsoluteSizeSpan((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 26,
                this.getResources().getDisplayMetrics())), 0, sb.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mToolBarTitleTv.setText(sb);
    }

    private void disableNavigationViewScrollbars(NavigationView navigationView) {
        if (navigationView != null) {
            NavigationMenuView navigationMenuView = (NavigationMenuView) navigationView.getChildAt(0);
            if (navigationMenuView != null) {
                navigationMenuView.setVerticalScrollBarEnabled(false);
            }
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        /*mSignInClicked = false;

        // updateUI(true);
        Plus.PeopleApi.loadVisible(mGoogleApiClient, null).setResultCallback(
                new ResultCallback<People.LoadPeopleResult>() {
                    @Override
                    public void onResult(People.LoadPeopleResult loadPeopleResult) {
                        //TODO IMPLEMENT
                    }
                });*/

    }

    @Override
    public void onConnectionSuspended(int i) {
        //mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }


}