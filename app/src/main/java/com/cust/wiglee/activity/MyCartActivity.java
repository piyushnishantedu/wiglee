package com.cust.wiglee.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.cust.wiglee.R;
import com.cust.wiglee.adapters.MyCartListAdapter;
import com.cust.wiglee.adapters.TodaysMenuListAdapter;
import com.cust.wiglee.app.AppUtils;
import com.cust.wiglee.app.WAppConstants;
import com.cust.wiglee.app.WAppController;
import com.cust.wiglee.bean.CartBean;
import com.cust.wiglee.bean.MenuBean;
import com.cust.wiglee.view.appcompatbckport.PullRefreshLayout;
import com.cust.wiglee.view.appcompatbckport.SwipeRefreshHintLayout;
import com.cust.wiglee.view.appcompatbckport.SwipeRefreshLayoutLinear;
import com.cust.wiglee.view.common.FontTextView;
import com.cust.wiglee.volley.CustomPostRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyCartActivity extends AppCompatActivity implements MyCartListAdapter.AdapterCallback, SwipeRefreshLayoutLinear.OnRefreshListener{
    private String TAG = WAppConstants.APP_TAG;
    private static final String MEALS_LIST = "meals_list";
    private PullRefreshLayout mPullRefreshLayout;
    private ListView mListView;
    private RelativeLayout mEmptyErrMsg;
    private LinearLayout mLoading;
    private FontTextView mEmptyErrMsgTv;
    private ImageView mNoConnIcon;
    private ArrayList<CartBean> mCartList;
    private MyCartListAdapter mMyCartListAdapter;
    private SharedPreferences mPrefs;
    public Button mBtnChkOut;
    //private BottomSheet mBottomSheet;
    private FrameLayout mAnchor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPrefs = WAppController.getPreferences(this);
        setContentView(R.layout.activity_my_cart);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_my_cart);
        if (mToolbar != null) {
            mToolbar.getMenu().clear();
            mToolbar.setTitle("");
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            TextView mTvTitle = (TextView) mToolbar.findViewById(R.id.toolbar_my_cart_title);
            mTvTitle.setText("My Cart");
            /*ImageView mIvCart = (ImageView) mToolbar.findViewById(R.id.iv_cart);
            mIvCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });*/
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
        mAnchor = (FrameLayout) findViewById(R.id.my_cart_anchor);
        mEmptyErrMsg = (RelativeLayout) findViewById(R.id.my_cart_no_conn);
        mEmptyErrMsgTv = (FontTextView) findViewById(R.id.ftv_no_conn_msg_my_cart);
        mNoConnIcon = (ImageView) findViewById(R.id.iv_no_conn_my_cart);
        mLoading = (LinearLayout) findViewById(R.id.loading_my_cart);
        mListView = (ListView) findViewById(R.id.my_cart_list);
        mCartList = new ArrayList<CartBean>();
        mMyCartListAdapter = new MyCartListAdapter(this, mCartList,mAnchor);
        mListView.setAdapter(mMyCartListAdapter);
        mPullRefreshLayout = (PullRefreshLayout) findViewById(R.id.ptr_layout_my_cart);
        mPullRefreshLayout.setOnRefreshListener(this);
        mPullRefreshLayout.setRefreshListView(mListView);
        mPullRefreshLayout.setColorScheme(R.color.holo_purple_dark, R.color.holo_blue_bright, R.color.holo_red_light, R.color.holo_green_dark);
        SwipeRefreshHintLayout mSwipeRefreshHintLayout = (SwipeRefreshHintLayout) findViewById(R.id.swipe_hint_my_cart);
        mSwipeRefreshHintLayout.setSwipeLayoutTarget(mPullRefreshLayout);

        mBtnChkOut = (Button) findViewById(R.id.btn_chk_out);
        mBtnChkOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               String allQnty="";
               String allCartId="";
                if( mCartList.size() >1) {
                    for (int i = 0; i < mCartList.size(); i++) {
                        if (i == mCartList.size() - 1) {
                            allQnty = allQnty + mCartList.get(i).getQuantity();
                            allCartId = allCartId + mCartList.get(i).getCartId();
                        } else {
                            allQnty = allQnty + mCartList.get(i).getQuantity() + "_";
                            allCartId = allCartId +  mCartList.get(i).getCartId() + "_";
                        }
                    }
                }else{
                    allQnty =  mCartList.get(0).getQuantity()+"";
                    allCartId =  mCartList.get(0).getCartId()+"";
                }
                doChkOut(allQnty,allCartId);
            }
        });

        if (savedInstanceState != null) {
            mLoading.setVisibility(View.GONE);
            mCartList = savedInstanceState.getParcelableArrayList(MEALS_LIST);
            mMyCartListAdapter.setFeeds(mCartList);
            if(mCartList.size() == 0){
                loadCart(false);
            }
        } else {
            loadCart(false);
        }
        mEmptyErrMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppUtils.isNetworkAvailable(MyCartActivity.this)) {
                    mEmptyErrMsg.setVisibility(View.GONE);
                    mLoading.setVisibility(View.VISIBLE);
                    loadCart(false);
                }
            }
        });
    }

    @Override
    public void onMethodCallbackUpdateCart() {
        upDateCartView();
    }
    public void upDateCartView() {
        if(mCartList.size() < 1){
            mLoading.setVisibility(View.GONE);
            mEmptyErrMsg.setVisibility(View.VISIBLE);
            mNoConnIcon.setVisibility(View.GONE);
            mEmptyErrMsgTv.setText("Empty cart");
        }else{
            mLoading.setVisibility(View.GONE);
            mEmptyErrMsg.setVisibility(View.GONE);
        }
    }

    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                loadCart(true);
            }
        }, 100);
    }
    private void loadCart(final boolean isRefreshReq) {
            if(isRefreshReq){
                mLoading.setVisibility(View.GONE);
            }else{
                mLoading.setVisibility(View.VISIBLE);
            }
            mEmptyErrMsg.setVisibility(View.GONE);

        CustomPostRequest jsonAllMealsReq = new CustomPostRequest(Request.Method.POST,
                WAppConstants.URL_GET_MY_CART, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "My Cart Response: " + response.toString());
                if (response != null) {
                    if(isRefreshReq){
                        mCartList.clear();
                        mMyCartListAdapter.notifyDataSetChanged();
                    }
                    parseCartJsonResponse(response);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(!isRefreshReq){
                        mLoading.setVisibility(View.GONE);
                        mEmptyErrMsg.setVisibility(View.VISIBLE);
                        mNoConnIcon.setVisibility(View.VISIBLE);
                        mEmptyErrMsgTv.setText(getResources().getString(R.string.no_conn_retry));
                }else{
                    mPullRefreshLayout.setRefreshing(false);
                    Snackbar.make(mAnchor, "Failed to refresh", Snackbar.LENGTH_SHORT).show();
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", mPrefs.getString(WAppConstants.LOGGED_IN_USER_ID,"1"));
                params.put("sec_code", mPrefs.getString(WAppConstants.LOGGED_IN_USER_SEC_CODE, "ASD"));
                return params;
            }
        };
        WAppController.getInstance().addToRequestQueue(jsonAllMealsReq);
    }
    private void parseCartJsonResponse(JSONObject response) {
        try {
            JSONArray postJsonArray = response.getJSONArray("user_cart_feed");
            if(postJsonArray.length()>0){
                for (int i = 0; i < postJsonArray.length(); i++) {
                    JSONObject feedObj = (JSONObject) postJsonArray.get(i);
                    CartBean item = new CartBean();
                    item.setMealId(feedObj.getInt("meal_id"));
                    item.setCartId(feedObj.getInt("cart_id"));
                    item.setMealName(feedObj.getString("meal_name"));
                    item.setMealDesc(feedObj.getString("meal_description"));
                    item.setMealType(feedObj.getString("meal_type"));
                    item.setMealPrice(feedObj.getString("meal_price"));
                    item.setMealImageId(feedObj.getString("meal_image_id"));
                    item.setQuantity(feedObj.getInt("quantity")==0?1:feedObj.getInt("quantity"));
                    item.setProcessFlag(feedObj.getString("process_flag"));
                    mCartList.add(item);
                }
                mPullRefreshLayout.setRefreshing(false);
            }
            if(mCartList.size() < 1){
                mLoading.setVisibility(View.GONE);
                mEmptyErrMsg.setVisibility(View.VISIBLE);
                mNoConnIcon.setVisibility(View.GONE);
                mEmptyErrMsgTv.setText("Cart Empty!!");
            }else{
                mLoading.setVisibility(View.GONE);
                mEmptyErrMsg.setVisibility(View.GONE);
            }
            mMyCartListAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            mLoading.setVisibility(View.GONE);
            mPullRefreshLayout.setRefreshing(false);
            e.printStackTrace();
        }
    }


    private void doChkOut(final String allQnty, final String allCartId) {
        mLoading.setVisibility(View.VISIBLE);
        CustomPostRequest jsonReq = new CustomPostRequest(Request.Method.POST,
                WAppConstants.URL_DO_CHK_OUT, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "Chk out response : "+response);
                if (response != null) {
                    try {
                        String result = response.getString("status");
                        if(result.equals("success")){
                            mLoading.setVisibility(View.GONE);
                            Intent orderIntent = new Intent(MyCartActivity.this, PlaceOrderActivity.class);
                            MyCartActivity.this.startActivity(orderIntent);
                            MyCartActivity.this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        }else{
                            mLoading.setVisibility(View.GONE);
                            Snackbar mSnackbar = Snackbar.make(mAnchor, getString(R.string.some_wrong_later), Snackbar.LENGTH_LONG);
                            mSnackbar.show();
                        }
                    } catch (JSONException e) {e.printStackTrace();}
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                mLoading.setVisibility(View.GONE);
                Snackbar mSnackbar = Snackbar.make(mAnchor, getString(R.string.some_wrong_later), Snackbar.LENGTH_LONG);
                mSnackbar.show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("all_cart_id", allCartId);
                params.put("all_quantity", allQnty);
                params.put("user_id", mPrefs.getString(WAppConstants.LOGGED_IN_USER_ID, ""));
                params.put("sec_code", mPrefs.getString(WAppConstants.LOGGED_IN_USER_SEC_CODE,""));
                return params;
            }
        };
        WAppController.getInstance().addToRequestQueue(jsonReq);
    }



    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(MEALS_LIST, mCartList);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }
    @Override
    protected void onStart() {
        super.onStart();
    }
    @Override
    protected void onStop() {
        super.onStop();
    }
    //private SweetAlertDialog mSADialog;
    @Override
    public void onDestroy(){
        /*if(mBottomSheet != null && mBottomSheet.isShowing()){
            mBottomSheet.dismiss();
        }
        if(mSADialog != null && mSADialog.isShowing()){
            mSADialog.dismiss();
        }*/
        super.onDestroy();
    }
    @Override
    protected void onResume() {
        super.onResume();
    }
    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        //Intent intent = new Intent();
        //intent.putExtra("request_approved", mRequestApproved);
        //setResult(RESULT_OK, intent);
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

}