package com.cust.wiglee.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewAnimator;
import com.cocosw.bottomsheet.BottomSheet;
import com.cust.wiglee.R;
import com.cust.wiglee.app.AppUtils;
import com.cust.wiglee.app.WAppConstants;
import com.cust.wiglee.app.WAppController;
import com.cust.wiglee.volley.ProfImageView;
import com.fenchtose.nocropper.CropperView;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;
import cn.pedant.SweetAlert.SweetAlertDialog;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

public class MyProfEditActivity extends AppCompatActivity {
    private String TAG = WAppConstants.APP_TAG;
    private SharedPreferences mPrefs;
    private SharedPreferences.Editor mPrefEditor;
    private FrameLayout mRootView;
    private LinearLayout mLoadingLayout;
    private ScrollView mContainerScrollView;
    private TextSwitcher titleSwitcher;
    private TextSwitcher errorSwitcher;
    private TextSwitcher detailsSwitcher;
    private ViewAnimator inputSwitcher;
    private TextView mTvToolBarNxt;
    private TextView mStepSkip;
    private TextView mPictureSelect;
    private FrameLayout mPictureHolder;
    private ProfImageView mProfImageView;
    private AppCompatEditText mFullName;
    private Spinner mSpinnerGender;
    private AppCompatEditText mAbtMe;
    private String[] mGenderList;
    private BottomSheet mBottomSheet;
    private SweetAlertDialog mSADialog;
    private Bitmap mPicBitmap;
    private File mUploadFile = null;
    private int stepCount = 4;
    private int stepIndex = 0;
    private boolean error;
    private boolean isPicAdded;
    private boolean isTaskRunning;
    private String hasAbtMe;
    private List<String> titleList;
    private List<String> errorList;
    private List<String> detailsList;
    ArrayAdapter<String> mDataAdapterGender;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isPicAdded = false;
        isTaskRunning = false;
        setContentView(R.layout.activity_my_prof_edit);
        mPrefs = WAppController.getPreferences(this);
        mPrefEditor  = WAppController.getPreferences(this).edit();
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_edit_prof);
        if (mToolbar != null) {
            mToolbar.getMenu().clear();
            mToolbar.setTitle("");
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            TextView mTvTitle = (TextView) mToolbar.findViewById(R.id.toolbar_edit_prof_title);
            mTvTitle.setText("Edit your Profile");
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(stepIndex == 0)
                        onBackPressed();
                    else
                        previousStep();
                }
            });
        }
        mRootView = (FrameLayout) findViewById(R.id.edit_prof_anchor);
        mTvToolBarNxt = (TextView) mToolbar.findViewById(R.id.toolbar_edit_prof_nxt);
        mTvToolBarNxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!validateStep()) {
                    if (stepIndex + 1 == stepCount) {
                        AppUtils.hideSoftKeyboard(mFullName);
                        doSubmitSave();
                    } else {
                        nextStep();
                    }
                }
            }
        });
        mLoadingLayout = (LinearLayout) findViewById(R.id.ll_loading_edit_prof);
        mContainerScrollView = (ScrollView) findViewById(R.id.container_sv);
        titleSwitcher = (TextSwitcher) findViewById(R.id.title_switcher);
        titleSwitcher.setInAnimation(getAnimation(R.anim.slide_in_to_bottom, true));
        titleSwitcher.setOutAnimation(getAnimation(R.anim.slide_out_to_top, false));

        errorSwitcher = (TextSwitcher) findViewById(R.id.error_switcher);
        errorSwitcher.setInAnimation(getAnimation(android.R.anim.slide_in_left, true));
        errorSwitcher.setOutAnimation(getAnimation(android.R.anim.slide_out_right, false));

        detailsSwitcher = (TextSwitcher) findViewById(R.id.details_switcher);
        detailsSwitcher.setInAnimation(getAnimation(R.anim.alpha_in, true));
        detailsSwitcher.setOutAnimation(getAnimation(R.anim.alpha_out, false));

        inputSwitcher = (ViewAnimator) findViewById(R.id.input_switcher);
        inputSwitcher.setInAnimation(getAnimation(R.anim.alpha_in, true));
        inputSwitcher.setOutAnimation(getAnimation(R.anim.alpha_out, false));
        mStepSkip = (TextView) findViewById(R.id.step_skip);
        mStepSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validateStep()) {
                    if (stepIndex + 1 == stepCount) {
                        AppUtils.hideSoftKeyboard(mFullName);
                        doSubmitSave();
                    } else {
                        nextStep();
                    }
                }
            }
        });

        titleList = new ArrayList<String>();
        errorList = new ArrayList<String>();
        detailsList = new ArrayList<String>();
        titleList.add("Profile Picture");
        titleList.add("Full Name");
        titleList.add("Gender");
        titleList.add("About Me");

        errorList.add("Please add a profile picture.");
        errorList.add("Please add your full name");
        errorList.add("Please select your gender");
        errorList.add("Please add something about your self");

        detailsList.add("Please add a profile picture.");
        detailsList.add("Please add your full name");
        detailsList.add("Please select your gender");
        detailsList.add("Please add something about your self");

        mPictureSelect = (TextView) findViewById(R.id.tv_image);
        if("0".equals(mPrefs.getString(WAppConstants.LOGGED_IN_USER_PROF_PIC_ID, "0"))){
            mPictureSelect.setText(" Add profile picture");
        }else{
            mPictureSelect.setText(" Edit profile picture");
        }
        mPictureSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGalleryOrCamera();
            }
        });
        mPictureHolder = (FrameLayout) findViewById(R.id.fl_image_holder);
        mProfImageView = (ProfImageView) findViewById(R.id.iv_image);
        mProfImageView.setScaleType(ImageView.ScaleType.FIT_XY);
        int padding = AppUtils.dpToPx(0);
        mProfImageView.setPadding(padding, padding, padding, padding);
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) mProfImageView.getLayoutParams();
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int widthPic = displaymetrics.widthPixels;
        int mPostImageWidth =  widthPic - AppUtils.dpToPx(40);
        params.height = (int)mPostImageWidth;
        params.setMargins(padding, padding, padding, padding);
        mProfImageView.setLayoutParams(params);
        mProfImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGalleryOrCamera();
            }
        });

        mGenderList = getResources().getStringArray(R.array.gender_list_value);
        mSpinnerGender = (Spinner) findViewById(R.id.sp_gender);
        mDataAdapterGender = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, mGenderList);
        mDataAdapterGender.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerGender.setAdapter(mDataAdapterGender);
        mSpinnerGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg) {
            }
        });
        mAbtMe = (AppCompatEditText) findViewById(R.id.et_abt_me);
        mFullName = (AppCompatEditText) findViewById(R.id.et_full_name);
        updateStep();
        initUiValues();
    }
    private void initUiValues(){
        mProfImageView.setImageUrl(String.format(WAppConstants.URL_PROF_PIC, mPrefs.getString(WAppConstants.LOGGED_IN_USER_PROF_PIC_ID, "0"), 600, 600), WAppController.getInstance().getImageLoader());
        mProfImageView.setErrorImageResId(R.drawable.default_avatar);
        mProfImageView.setDefaultImageResId(R.drawable.default_avatar);
        mProfImageView.setResponseObserver(new ProfImageView.ResponseObserver() {
            @Override
            public void onError() {

            }

            @Override
            public void onSuccess() {
            }
        });
        mFullName.setText(mPrefs.getString(WAppConstants.LOGGED_IN_USER_NAME, ""));
        mAbtMe.setText(mPrefs.getString(WAppConstants.LOGGED_IN_USER_ABT,""));
        //mHobby.setText(mPrefs.getString(WAppConstants.LOGGED_IN_USER_HOBBEY,""));
    }
    private Animation getAnimation(int animationResId, boolean isInAnimation){
        final Interpolator interpolator;
        if(isInAnimation){
            interpolator = new DecelerateInterpolator(1.0f);
        }else{
            interpolator = new AccelerateInterpolator(1.0f);
        }
        Animation animation = AnimationUtils.loadAnimation(this, animationResId);
        animation.setInterpolator(interpolator);
        return animation;
    }

    private void nextStep(){
        stepIndex++;
        updateStep();
    }

    private void previousStep(){
        stepIndex--;
        updateStep();
    }

    private void updateStep(){
        if(stepIndex<stepCount) {
            inputSwitcher.setDisplayedChild(stepIndex);
            titleSwitcher.setText(titleList.get(stepIndex));
            detailsSwitcher.setText(detailsList.get(stepIndex));
            if (stepIndex + 1 == stepCount) {
                mTvToolBarNxt.setText("Save");
            }else{
                mTvToolBarNxt.setText("Next");
            }
        }else{
            stepIndex--;
        }
        updateViews();
    }
    private boolean isGenderFrstShow = true;
    private boolean updateViews(){
        switch(stepIndex) {
            case 0:
                AppUtils.hideSoftKeyboard(mFullName);
                mStepSkip.setVisibility(View.VISIBLE);
                if(isPicAdded) {
                    mPictureHolder.setVisibility(View.VISIBLE);
                }else{
                    mPictureHolder.setVisibility(View.VISIBLE);
                    if("0".equals(mPrefs.getString(WAppConstants.LOGGED_IN_USER_PROF_PIC_ID, "0"))){
                        mPictureSelect.setText(" Add profile picture");
                        openGalleryOrCamera();
                    }else{
                        mPictureSelect.setText(" Edit profile picture");
                    }
                }
                break;
            case 1:
                mStepSkip.setVisibility(View.VISIBLE);
                mPictureHolder.setVisibility(View.GONE);
                mFullName.requestFocus();
                showSoftInput(mFullName);
                mFullName.post(new Runnable() {
                    @Override
                    public void run() {
                        int posAdd = mFullName.getText().length();
                        mFullName.setSelection(posAdd);
                    }
                });
                break;
            case 2:
                mStepSkip.setVisibility(View.VISIBLE);
                AppUtils.hideSoftKeyboard(mFullName);
                mPictureHolder.setVisibility(View.GONE);
                if(isGenderFrstShow) {
                    isGenderFrstShow = false;
                    String gender = mPrefs.getString(WAppConstants.LOGGED_IN_USER_GENDER,"Male");
                    Log.d(TAG, "gender set  "+ gender);
                    int spinnerPositionG = mDataAdapterGender.getPosition(gender);
                    mSpinnerGender.setSelection(spinnerPositionG);
                }
                break;
            case 3:
                showSoftInput(mAbtMe);
                mAbtMe.post(new Runnable() {
                    @Override
                    public void run() {
                        int posAdd = mAbtMe.getText().length();
                        mAbtMe.setSelection(posAdd);
                    }
                });
                mStepSkip.setVisibility(View.INVISIBLE);
                mAbtMe.requestFocus();
                mPictureHolder.setVisibility(View.GONE);
                break;
            default:
        }
        return error;
    }

    private boolean validateStep(){
        error = false;
        switch(stepIndex) {
            case 0:
                errorSwitcher.setText("");
                error = false;
                break;
            case 1:
                String name = mFullName.getText()+"";
                if (TextUtils.isEmpty(name)) {
                    errorSwitcher.setText("");
                    error = false;
                    break;
                }else {
                    if(!Pattern.matches("[a-zA-Z \\./-]*", name)){
                        errorSwitcher.setText(getString(R.string.sign_up_username_err_wrong));
                        error = true;
                        break;
                    }
                }
            case 2:
                errorSwitcher.setText("");
                error = false;
                break;
            case 3:
                errorSwitcher.setText("");
                error = false;
                break;
            default:
                error = true;
        }
        return error;
    }
    protected void openGalleryOrCamera() {
        mBottomSheet = new BottomSheet.Builder(this).title("Select Picture")
                .sheet(R.menu.pic_select_list).listener(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case R.id.bs_gallery:
                                EasyImage.openGallery(MyProfEditActivity.this,0);
                                break;
                            case R.id.bs_camera:
                                EasyImage.openCamera(MyProfEditActivity.this,0);
                                //EasyImage.openChooser(MyProfActivity.this, "Pick photo");
                                break;
                        }
                    }
                }).show();
    }
    private void showSoftInput(View v){
        Context context = v.getContext();
        if(context != null){
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            v.requestFocus();
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        }
    }

    @Override
    protected void onActivityResult(int requestCode,int resultCode, Intent data) {
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type ) {
            }

            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                onPhotoReturned(imageFile);

            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                //Cancel handling, you might wanna remove taken photo if it was canceled
                if (source == EasyImage.ImageSource.CAMERA) {
                    File photoFile = EasyImage.lastlyTakenButCanceledPhoto(MyProfEditActivity.this);
                    if (photoFile != null) photoFile.delete();
                }
            }
        });
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void onPhotoReturned(File photoFile) {
        mPicBitmap =  BitmapFactory.decodeFile(photoFile.getAbsolutePath());
        double width = mPicBitmap.getWidth();
        double height = mPicBitmap.getHeight();
        float scale = (float)height/(float)width;
        if(width > 720){
            int newHeight = (int) ((float)(720)*(float)(scale));
            int newWidth = (int)(720);
            mPicBitmap = Bitmap.createScaledBitmap(mPicBitmap, newWidth, newHeight, true);
        }else{
            mPicBitmap = Bitmap.createScaledBitmap(mPicBitmap, (int)width, (int)height, true);
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        mPicBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
        mPicBitmap = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
        mProfImageView.setImageBitmap(mPicBitmap);
        mProfImageView.setScaleType(ImageView.ScaleType.FIT_XY);
        mPictureHolder.setVisibility(View.VISIBLE);
        isPicAdded = true;
        cropperPopup();
    }
    private PopupWindow mCropperPopup;
    private ImageView mCloseBtn;
    private CropperView mCropIV;
    private ImageView mSnapBtn;
    private ImageView mrotateBtn;
    private TextView mDoneBtn;
    private void cropperPopup() {
        try{
            LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View layout = inflater.inflate(R.layout.popup_photo_cropper,(ViewGroup) findViewById(R.id.root_view));
            mCropperPopup = new PopupWindow(layout, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, true);
            mCropperPopup.setAnimationStyle(R.style.PopupAnimation);
            mCropperPopup.setBackgroundDrawable(new ColorDrawable());
            mCropperPopup.setOutsideTouchable(false);
            mCropperPopup.setFocusable(true);
            mCropperPopup.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {

                }
            });
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    try {
                        if (Build.VERSION.SDK_INT >= 19) {
                            mCropperPopup.showAsDropDown(findViewById(R.id.cropper_anchor), 0, 0, Gravity.TOP);
                        } else {
                            mCropperPopup.showAsDropDown(findViewById(R.id.cropper_anchor), 0, 0);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, 40L);
            mCloseBtn = (ImageView) layout.findViewById(R.id.toolbar_pic_cropper_close);
            mCloseBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCropperPopup.dismiss();
                }
            });
            mDoneBtn = (TextView) layout.findViewById(R.id.toolbar_pic_cropper_done);
            mDoneBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cropImage();
                }
            });
            mCropIV = (CropperView) layout.findViewById(R.id.cv_cropper);
            mCropIV.setMinZoom(1f);
            mSnapBtn = (ImageView) layout.findViewById(R.id.snap_button);
            mrotateBtn = (ImageView) layout.findViewById(R.id.rotate_button);
            mSnapBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCropIV.cropToCenter();
                }
            });
            mrotateBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    rotateImage();
                }
            });
            loadNewImage();
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setStatusBarColor(Color.parseColor("#303F9F"));
            }*/
        } catch (Exception e) {e.printStackTrace();}
    }
    private void loadNewImage() {
        mCropIV.setImageBitmap(mPicBitmap);
    }
    private void cropImage() {
        mPicBitmap = mCropIV.getCroppedBitmap();
        mProfImageView.setImageBitmap(mPicBitmap);
        mCropperPopup.dismiss();
    }
    private void rotateImage() {
        if (mPicBitmap == null) {
            return;
        }
        mPicBitmap = AppUtils.rotateBitmap(mPicBitmap, 90);
        mCropIV.setImageBitmap(mPicBitmap);
    }
    private static final int REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION = 1;
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permission was granted
                EasyImage.openCamera(this, 0);
            } else {
                // permission denied, boo!
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }
    @Override
    public void onBackPressed()	{
        if(mCropperPopup != null && mCropperPopup.isShowing()) {
            mCropperPopup.dismiss();
            return;
        }
        if(stepIndex == 0) {
            AppUtils.hideSoftKeyboard(mFullName);
            if(!checkDiscard()){
                mSADialog = new SweetAlertDialog(this);
                mSADialog.setTitleText("Discard changes?");
                mSADialog.setContentText("Do you want to leave this page!");
                mSADialog.setConfirmText("Yes");
                mSADialog.showCancelButton(true);
                mSADialog.setCancelText("No");
                mSADialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismiss();
                    }
                });
                mSADialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        setResult(RESULT_CANCELED);
                        sDialog.dismiss();
                        finish();
                        overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
                    }
                });
                mSADialog.show();
            }else{
                setResult(RESULT_CANCELED);
                finish();
                overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
            }
        }else {
            previousStep();
        }
    }
    @Override
    public void onDestroy(){
        if(mBottomSheet != null && mBottomSheet.isShowing()){
            mBottomSheet.dismiss();
        }
        if(mSADialog != null)
            mSADialog.dismiss();
        if(mCropperPopup != null && mCropperPopup.isShowing())
            mCropperPopup.dismiss();
        EasyImage.clearConfiguration(this);
        super.onDestroy();
    }

    protected boolean checkDiscard() {
        boolean shouldDiscard = true;
        if(!TextUtils.isEmpty(mFullName.getText()+"")){
            shouldDiscard = false;
        }
        if(!TextUtils.isEmpty(mAbtMe.getText()+"")){
            shouldDiscard = false;
        }
        if(!(mPicBitmap == null)){
            shouldDiscard = false;
        }
        return shouldDiscard;
    }

    private void doSubmitSave(){
        if (AppUtils.isNetworkAvailable(MyProfEditActivity.this)) {
            mTvToolBarNxt.setEnabled(false);
            postSave();
        } else {
            Snackbar mSnackbar = Snackbar.make(mRootView, R.string.no_connection, Snackbar.LENGTH_LONG);
            mSnackbar.show();
        }
    }
    String responseStr = "";
    String mEventId = "";
    protected void postSave() {
        try {
            if(!isTaskRunning){
                isTaskRunning = true;
                saveDetailsOk(new Callback() {
                    @Override
                    public void onResponse(final com.squareup.okhttp.Response response) {
                        try {
                            responseStr = response.body().string();
                            Log.d(TAG, "responseStr " + responseStr);
                        } catch (final IOException e) {
                            MyProfEditActivity.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    mTvToolBarNxt.setEnabled(true);
                                    deleteFile();
                                    Snackbar mSnackbar = Snackbar.make(mRootView, R.string.failed_to_save, Snackbar.LENGTH_LONG);
                                    mSnackbar.show();
                                    isTaskRunning = false;
                                    mLoadingLayout.setVisibility(View.GONE);
                                    e.printStackTrace();
                                }
                            });
                        }
                        MyProfEditActivity.this.runOnUiThread(new Runnable() {
                            public void run() {
                                isTaskRunning = false;
                                mLoadingLayout.setVisibility(View.GONE);
                                mTvToolBarNxt.setEnabled(true);
                                if (response.isSuccessful()) {
                                    if (responseStr.contains("success")) {
                                        deleteFile();
                                        setResult(RESULT_OK);
                                        finish();
                                    } else {
                                        deleteFile();
                                        Snackbar mSnackbar = Snackbar.make(mRootView, R.string.failed_to_save, Snackbar.LENGTH_LONG);
                                        mSnackbar.show();
                                    }
                                } else {
                                    deleteFile();
                                    Snackbar mSnackbar = Snackbar.make(mRootView, R.string.failed_to_save, Snackbar.LENGTH_LONG);
                                    mSnackbar.show();
                                }
                            }
                        });
                    }

                    @Override
                    public void onFailure(Request req, IOException exp) {
                        MyProfEditActivity.this.runOnUiThread(new Runnable() {
                            public void run() {
                                mTvToolBarNxt.setEnabled(true);
                                deleteFile();
                                isTaskRunning = false;
                                mLoadingLayout.setVisibility(View.GONE);
                                Snackbar mSnackbar = Snackbar.make(mRootView, R.string.failed_to_save, Snackbar.LENGTH_LONG);
                                mSnackbar.show();
                            }
                        });
                    }
                });
            }
            mLoadingLayout.setVisibility(View.VISIBLE);
        }catch(Exception e) {
            mTvToolBarNxt.setEnabled(true);
            e.printStackTrace();}
    }

    public static final MediaType MEDIA_TYPE_JPEG  = MediaType.parse("image/jpeg");
    private Call saveDetailsOk(Callback callback){
        OkHttpClient client = new OkHttpClient();
        String isImageUploaded = "N";
        String fileName = "";
        if(mPicBitmap != null){
            int min = 1;
            int max = 10000;
            Random rand = new Random();
            int intRand = rand.nextInt(max - min + 1) + min;
            fileName = "/ImgUp_"+intRand+".jpeg";
            String uploadPath = Environment.getExternalStorageDirectory() + "/Bumped/upload/";
            File uploadDir = new File(uploadPath);
            if(!uploadDir.exists())
                uploadDir.mkdirs();
            mUploadFile = new File(uploadDir, fileName);
            FileOutputStream fOut = null;
            try {
                fOut = new FileOutputStream(mUploadFile);
            } catch (FileNotFoundException e) {e.printStackTrace();}
            mPicBitmap.compress(Bitmap.CompressFormat.JPEG, 80, fOut);
            if(fOut != null)
                try {
                    fOut.flush();
                    fOut.close();
                } catch (IOException e) {e.printStackTrace();}
            isImageUploaded = "Y";
        }else{
            isImageUploaded = "N";
        }
        /*if(isPicAdded){
            isImageUploaded = "Y";
        }else{
            isImageUploaded = "N";
        }*/
        Log.d(TAG, "Req wth out image isPicAdded"+ isPicAdded);
        RequestBody formBody = null;
        hasAbtMe = "N";
        if(TextUtils.isEmpty(mAbtMe.getText()+"")){
            hasAbtMe = "N";
        }else{
            hasAbtMe = "Y";
        }
        String hasFullName = "N";
        if(TextUtils.isEmpty(mFullName.getText()+"")){
            hasFullName = "N";
        }else{
            hasFullName = "Y";
        }
        if ("Y".equals(isImageUploaded)){
            formBody = new MultipartBuilder().type(MultipartBuilder.FORM)
                    .addFormDataPart("user_id", mPrefs.getString(WAppConstants.LOGGED_IN_USER_ID, "0"))
                    .addFormDataPart("sec_code", mPrefs.getString(WAppConstants.LOGGED_IN_USER_SEC_CODE, ""))
                    .addFormDataPart("has_fullname", hasFullName)
                    .addFormDataPart("fullname", mFullName.getText()+"")
                    .addFormDataPart("gender", mSpinnerGender.getSelectedItem()+"".toLowerCase())
                    .addFormDataPart("has_abt_me", hasAbtMe)
                    .addFormDataPart("abt_me", mAbtMe.getText()+"")
                    .addFormDataPart("is_img_uploaded", isImageUploaded)
                    .addFormDataPart("prof_image_file", fileName, RequestBody.create(MEDIA_TYPE_JPEG, mUploadFile))
                    .build();

            Log.d(TAG, "Req wth  image user_id"+ mPrefs.getString(WAppConstants.LOGGED_IN_USER_ID, "0"));
            Log.d(TAG, "Req wth  image sec_token"+ mPrefs.getString(WAppConstants.LOGGED_IN_USER_SEC_CODE, ""));
            Log.d(TAG, "Req wth  image has_fullname"+ hasFullName);
            Log.d(TAG, "Req wth  image fullname"+ mFullName.getText());
            Log.d(TAG, "Req wth  image gender"+ mSpinnerGender.getSelectedItem()+"".toLowerCase());
            Log.d(TAG, "Req wth  image hasAbtMe"+ hasAbtMe);
            Log.d(TAG, "Req wth  image mAbtMe"+ mAbtMe.getText()+"");
            Log.d(TAG, "Req wth  image isImageUploaded"+ isImageUploaded);



        }else{
            formBody = new MultipartBuilder().type(MultipartBuilder.FORM)
                    .addFormDataPart("user_id", mPrefs.getString(WAppConstants.LOGGED_IN_USER_ID, "0"))
                    .addFormDataPart("sec_code", mPrefs.getString(WAppConstants.LOGGED_IN_USER_SEC_CODE, ""))
                    .addFormDataPart("has_fullname", hasFullName)
                    .addFormDataPart("fullname", mFullName.getText()+"")
                    .addFormDataPart("gender", mSpinnerGender.getSelectedItem()+"")
                    .addFormDataPart("has_abt_me", hasAbtMe)
                    .addFormDataPart("abt_me", mAbtMe.getText()+"")
                    .addFormDataPart("is_img_uploaded", isImageUploaded)
                    .build();


            Log.d(TAG, "Req wth out image user_id"+ mPrefs.getString(WAppConstants.LOGGED_IN_USER_ID, "0"));
            Log.d(TAG, "Req wth out image sec_token"+ mPrefs.getString(WAppConstants.LOGGED_IN_USER_SEC_CODE, ""));
            Log.d(TAG, "Req wth out image has_fullname"+ hasFullName);
            Log.d(TAG, "Req wth out image fullname"+ mFullName.getText());
            Log.d(TAG, "Req wth out image gender"+ mSpinnerGender.getSelectedItem());
            Log.d(TAG, "Req wth out image hasAbtMe"+ hasAbtMe);
            Log.d(TAG, "Req wth out image mAbtMe"+ mAbtMe.getText());
            Log.d(TAG, "Req wth out image isImageUploaded"+ isImageUploaded);


        }
        Request request = new Request.Builder()
                .url(WAppConstants.URL_DO_EDIT_PROF)
                .post(formBody)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
        return call;
    }
    private void deleteFile() {
        try{
            if(mUploadFile != null)
                mUploadFile.delete();
        }catch(Exception e){e.printStackTrace();}
    }
}
