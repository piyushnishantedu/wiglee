package com.cust.wiglee.activity;

import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cust.wiglee.R;
import com.cust.wiglee.adapters.AreaSpinnerAdapter;
import com.cust.wiglee.app.AppUtils;
import com.cust.wiglee.app.WAppConstants;
import com.cust.wiglee.app.WAppController;
import com.cust.wiglee.volley.CustomPostRequest;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.CallbackManager.Factory;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphRequest.GraphJSONObjectCallback;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.greenhalolabs.emailautocompletetextview.EmailAutoCompleteTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
//My Import

//End
public class WSignUpActivity extends AppCompatActivity  implements OnConnectionFailedListener{
    private static final String TAG = WAppConstants.APP_TAG;
    private SharedPreferences mPrefs;
    private Editor mPrefEditor;
    //private AppCompatEditText mSignUpName;
    private EmailAutoCompleteTextView mSignUpEmail;
   // private IntlPhoneInput mSignUpPhoneNo;
   private AppCompatEditText userMobileNumber;
    //private AppCompatEditText mSignUpPwd;
    private RelativeLayout mLoading;
    private Button mEmailSignUpBtn;
    private TextView mTvTnC;
    //private SweetAlertDialog mSADialog;
    private boolean iscodeValidate = false;
    private boolean isLoginWithSocialsite = false;
    private HashMap<String,String>socialLogedInUserDetails = new HashMap<String,String>();
    //FaceBook and google
    public GoogleApiClient GoogleApiClient;
    private boolean SignInClicked;
    private boolean IntentInProgress;
    private ConnectionResult ConnectionResult;
    private SignInButton googlesigninButton;
    public static final int SIGN_IN_REQUEST_CODE= 0;
    public static final int ERROR_DIALOG_REQUEST_CODE = 11;
    private CallbackManager callbackManager;
   // private ImageView fb_login;
    private LoginButton facebookloginButton;
    private static final int RC_SIGN_IN = 9001;
    String user_name = null;
    String user_id = null;
    String email_id = null;
    //End
    private AppCompatEditText mETValidationCode;
    private String otpCode = "8986";
    // Creating Facebook CallbackManager Value
    private FrameLayout mAnchor;

    //My Code
    ArrayList<String> mAreaList = new ArrayList<String>();
    private Spinner spinnerArea;
    private AreaSpinnerAdapter mSpinAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPrefs = WAppController.getPreferences(this);
        //Initialize facebook sdk
        //int rand =  new Random().nextInt(90000000)+10000000;
        //otpCode = String.valueOf(rand);
        //Log.i("rand",String.valueOf(rand));

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = Factory.create();
        //End
        setContentView(R.layout.activity_wsign_up);
        mPrefs = WAppController.getPreferences(this);
        mPrefEditor = WAppController.getPreferences(this).edit();
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_sign_up);
        if (mToolbar != null) {
            mToolbar.getMenu().clear();
            mToolbar.setTitle("");
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mToolbar.setNavigationIcon(R.drawable.ic_clear_white);
            mToolbar.setNavigationOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    //onBackPressed();
                    callLogin();
                }
            });
        }
       // mSignUpName = (AppCompatEditText) findViewById(R.id.et_sign_up_name);
        userMobileNumber = (AppCompatEditText) findViewById(R.id.et_user_mobile);
        //My Code
        mETValidationCode = (AppCompatEditText) findViewById(R.id.et_validate_email);
        mETValidationCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                if (arg0.length() == 4) {
                    //mValidateBtn.setEnabled(true);
                    //doValidation();
                    validateOtp(otpCode);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {
            }
        });


        //End
        //Hide User Name
        //textView1.setVisibility(View.VISIBLE);
        //mSignUpName.setVisibility(View.GONE);

        mSignUpEmail = (EmailAutoCompleteTextView) findViewById(R.id.eact_sign_up_email);

       // userMobileNumber.onCheckIsTextEditor();
        userMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                if (arg0.length() == 10) {
                    String number = userMobileNumber.getText()+"".trim();
                    String numberWithIndiaCode = "+91"+userMobileNumber.getText()+"".trim();
                    Log.i("Number",numberWithIndiaCode );
                    Log.i("code", otpCode);
                    sendOtpToMobileNumber(otpCode);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {
            }
        });


       // mSignUpPhoneNo = (IntlPhoneInput) findViewById(R.id.eact_sign_up_phn);

        //My Code for Phone number listener

       /* mSignUpPhoneNo.setOnValidityChange(new IntlPhoneInputListener() {
            public void done(View view, boolean isValid) {

                if (isValid) {
                    Log.i("Number", mSignUpPhoneNo.getNumber());
                    Log.i("code", otpCode);
                    sendOtpToMobileNumber(otpCode);
                }
            }
        });*/
        //End

        //mSignUpPwd = (AppCompatEditText) findViewById(R.id.et_sign_up_password);
       /* mSignUpPwd.setOnEditorActionListener(new OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE || EditorInfo.IME_ACTION_UNSPECIFIED == actionId) {
                    if (validate()) {
                        AppUtils.hideSoftKeyboard(mSignUpName);
                        mLoading.setVisibility(View.VISIBLE);
                        doRegister();
                    }
                    return true;
                }
                return false;
            }
        });*/
       // AppUtils.hideSoftKeyboard(mSignUpName);
        mAnchor = (FrameLayout) findViewById(R.id.sign_up_anchor);
        mLoading = (RelativeLayout) findViewById(R.id.ll_loading_sign_up);
        mEmailSignUpBtn = (Button) findViewById(R.id.btn_sign_up_email);
        mEmailSignUpBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {
                    //AppUtils.hideSoftKeyboard(mSignUpName);
                    mLoading.setVisibility(View.VISIBLE);
                    doRegister();
                }
            }
        });
        mTvTnC = (TextView) findViewById(R.id.tv_tnc);
        SpannableString ss = new SpannableString("By clicking sign up, you agree to our Terms & Conditions and Privacy Policy.");
        ClickableSpan clickableSpanTnc = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent webIntent = new Intent(WSignUpActivity.this, WWebViewActivity.class);
                webIntent.putExtra("passed_url", "file:///android_asset/terms_conditions.html");
                webIntent.putExtra("passed_title", getString(R.string.terms_conditions));
                WSignUpActivity.this.startActivity(webIntent);
                WSignUpActivity.this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        };
        ClickableSpan clickableSpanPP = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent webIntent = new Intent(WSignUpActivity.this, WWebViewActivity.class);
                webIntent.putExtra("passed_url", "file:///android_asset/privacy_policy.html");
                webIntent.putExtra("passed_title", getString(R.string.privacy_policy));
                WSignUpActivity.this.startActivity(webIntent);
                WSignUpActivity.this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        };

        ss.setSpan(new ForegroundColorSpan(Color.parseColor("#5bb85d")), 38, 56, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(clickableSpanTnc, 38, 56, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        ss.setSpan(new ForegroundColorSpan(Color.parseColor("#5bb85d")), 61, 75, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(clickableSpanPP, 61, 75, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        mTvTnC.setText(ss);
        mTvTnC.setMovementMethod(LinkMovementMethod.getInstance());
        //FaceBook And Google Login
        // GoogleApiClient = buildGoogleAPIClient();
        /*

            My Code- Kishan Singh
         */
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        // ATTENTION: This "addApi(AppIndex.API)"was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        GoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


        googlesigninButton = (SignInButton) findViewById(R.id.sign_in_button);
        googlesigninButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String userMobile = userMobileNumber.getText()+"".trim();
                if (userMobile.length() < 10){
                    Snackbar.make(mAnchor,R.string.sign_up_phn_err_wrong,Snackbar.LENGTH_LONG).show();
                }
                else{
                    signIn();
                }

//                if (!GoogleApiClient.isConnecting()) {
//                    SignInClicked = true;
//                    processSignInError();
//
//                }
                //SignInClicked = true;
                //handelGoogleLogin();
            }
        });
        // fb_login = (ImageView) findViewById(R.id.fb_login);
        facebookloginButton = (LoginButton) findViewById(R.id.login_button);
        facebookloginButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().logInWithReadPermissions(WSignUpActivity.this, Arrays.asList("public_profile", "email"));
            }
        });
        // FacebookSdk.sdkInitialize(getApplicationContext());

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {

                final AccessToken accessToken = loginResult.getAccessToken();
                GraphRequest request = GraphRequest.newMeRequest(
                        accessToken,
                        new GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {
                                JSONObject responseData = response.getJSONObject();
                                try {
                                    Log.i("fb response", responseData.toString());
                                    if (responseData.has("id") && responseData.getInt("id") > 0) {
                                        // JSONObject data = responseData.getJSONObject("")
                                        String email = responseData.getString("email");
                                        String firstname = responseData.getString("first_name");
                                        String lastname = responseData.getString("last_name");

                                        afterFbLogin(accessToken.getUserId(), email, firstname, lastname);
                                    } else {
                                        Toast toast = Toast.makeText(getApplicationContext(),
                                                getString(R.string.technical_issue),
                                                Toast.LENGTH_LONG);
                                        toast.show();

                                    }
                                } catch (JSONException e) {
                                    Toast toast = Toast.makeText(getApplicationContext(),
                                            getString(R.string.technical_issue),
                                            Toast.LENGTH_LONG);
                                    toast.show();

                                    e.printStackTrace();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,link,email,first_name,last_name");
                request.setParameters(parameters);
                request.executeAsync();

            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });
        //End Code

        //My Code
        spinnerArea = (Spinner) findViewById(R.id.spinner_area);
        mSpinAdapter = new AreaSpinnerAdapter(getApplicationContext(), mAreaList, true);
        spinnerArea.setAdapter(mSpinAdapter);
        spinnerArea.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapter, View v, int position, long id) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });

        addItemsToSpinner();


        //Hide the View
        hideView();

    }
    //Facebook And Google login.

    private void handelGoogleLogin(){
        if (AppUtils.isNetworkAvailable(this)) {
            GoogleApiClient.connect();
        } else {
            Toast toast = Toast.makeText(this,"Network Error", Toast.LENGTH_LONG);
            toast.show();
        }
    }
//Kishan code New
    /*private void signIn() {
        if (AppUtils.isNetworkAvailable(this)) {
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(GoogleApiClient);
            GoogleApiClient.connect();
            startActivityForResult(signInIntent, RC_SIGN_IN);
        } else {
            Toast toast = Toast.makeText(this,"Network Error", Toast.LENGTH_LONG);
            toast.show();
        }
    }*/
private void signIn() {
    Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(GoogleApiClient);
    startActivityForResult(signInIntent, RC_SIGN_IN);
}

   /* private GoogleApiClient buildGoogleAPIClient() {
        return new GoogleApiClient.Builder(this).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API, Plus.PlusOptions.builder().build())
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .build();
    }*/
   // @Override
   /* protected void onStart() {
        super.onStart();
        if (AppUtils.isNetworkAvailable(this)) {
            //GoogleApiClient.connect();
        } else {
            Toast toast = Toast.makeText(this,"Network Error", Toast.LENGTH_LONG);
            toast.show();
        }
    }*/

   // @Override
    /*protected void onStop() {
        super.onStop();
        if (GoogleApiClient.isConnected())
            GoogleApiClient.disconnect();
    }
**/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
       /* if (requestCode == SIGN_IN_REQUEST_CODE) {
            if (resultCode != RESULT_OK) {
                SignInClicked = false;
            }

            IntentInProgress = false;
          //System.out.print(!GoogleApiClient.isConnecting());

            if (!GoogleApiClient.isConnecting()) {
                GoogleApiClient.connect();
            }

        }
        else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }*/
        //Kishan New Code
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
        else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }

        }
    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            if (AppUtils.isGetAccountPermissionGranted(WSignUpActivity.this)) {
                GoogleSignInAccount acct = result.getSignInAccount();
                user_name = acct.getDisplayName();
                user_id = acct.getId();
                email_id = acct.getEmail();
                aftergooglelogin(user_name, user_id, email_id);
            }

        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case AppUtils.MY_PERMISSIONS_REQUEST_GET_ACCOUNT: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request}
        }
    }

  /*  @Override
    public void onConnected(Bundle bundle) {
        SignInClicked = false;
        Log.i("glogin","onConnected");
        if (AppUtils.isGetAccountPermissionGranted(WSignUpActivity.this)){
            gettingUserDetails();
        }


    }*/

   /* public void gettingUserDetails() {
        Log.i("glogin","gettingUserDetails");
        Person signedInUser = Plus.PeopleApi.getCurrentPerson(GoogleApiClient);
        String user_profile_pic = null;
        String user_name = null;
        String user_id = null;
        Integer gender;
        String email_id = null;
        String new_gender= null;
        String birthday= null;

        if (signedInUser != null) {

            if (signedInUser.hasDisplayName()) {
                user_name = signedInUser.getDisplayName();
            }
            if (signedInUser.hasId()) {
                user_id = signedInUser.getId();
            }

            if (signedInUser.hasImage()) {
                user_profile_pic = signedInUser.getImage().getUrl();

            }
            if (signedInUser.hasBirthday()) {
                birthday = signedInUser.getBirthday();
            }

            email_id = Plus.AccountApi.getAccountName(GoogleApiClient);
            gender = signedInUser.getGender();
            if (gender==0){
                new_gender= "Male";
            }
            if (gender==1){
                new_gender= "Female";
            }
            if (gender==2){
                new_gender= "Other";
            }

        }


       // aftergooglelogin(user_name, user_id, new_gender, email_id, user_profile_pic, birthday);


    }*/


    /*@Override
    public void onConnectionSuspended(int i) {
        GoogleApiClient.connect();
    }*/

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (!connectionResult.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(connectionResult.getErrorCode(), this,
                    ERROR_DIALOG_REQUEST_CODE).show();
            return;
        }
        if (!IntentInProgress) {
            ConnectionResult = connectionResult;

            if (SignInClicked) {
                processSignInError();
            }
        }
    }

    private void processSignInError() {
        if (ConnectionResult != null && ConnectionResult.hasResolution()) {


            try {
                IntentInProgress = true;
                ConnectionResult.startResolutionForResult(this,
                        SIGN_IN_REQUEST_CODE);

            } catch (SendIntentException e) {
                IntentInProgress = false;
                GoogleApiClient.connect();
            }
        }
    }

      private void aftergooglelogin(String user_name, String user_id, String email_id){
        Log.i("glogin","aftergooglelogin");
        if (!socialLogedInUserDetails.isEmpty()){
            socialLogedInUserDetails.clear();
        }
        isLoginWithSocialsite = !isLoginWithSocialsite;
        socialLogedInUserDetails.put("fullname",user_name);
        socialLogedInUserDetails.put("email",email_id);
        socialLogedInUserDetails.put("password", otpCode);
        socialLogedInUserDetails.put("password_confirm", otpCode);
       // socialLogedInUserDetails.put("mobile_number", mSignUpPhoneNo.getNumber() + "");
          socialLogedInUserDetails.put("mobile_number","+91"+userMobileNumber.getText()+"".trim());
        Log.i("gmaildata", email_id);
       // if(mPrefs.getString(WAppConstants.LOGGED_IN_USER_ID, "").isEmpty()){
            mLoading.setVisibility(View.VISIBLE);
            doRegister();
       // }

    }

    private void afterFbLogin(String userId, String email, String firstname, String lastname) {
        if (!socialLogedInUserDetails.isEmpty()){
            socialLogedInUserDetails.clear();
        }
        isLoginWithSocialsite = !isLoginWithSocialsite;
        socialLogedInUserDetails.put("fullname",firstname + " "+lastname);
        socialLogedInUserDetails.put("email",email);
        socialLogedInUserDetails.put("password", otpCode);
        socialLogedInUserDetails.put("password_confirm",otpCode);
        //socialLogedInUserDetails.put("mobile_number", mSignUpPhoneNo.getNumber() + "");
        //socialLogedInUserDetails.put("mobile_number","+911111111222");
        socialLogedInUserDetails.put("mobile_number","+91"+userMobileNumber.getText()+"".trim());
        Log.i("fbdata", email);
        //if(mPrefs.getString(WAppConstants.LOGGED_IN_USER_ID, "").isEmpty()){
            mLoading.setVisibility(View.VISIBLE);
            doRegister();
        //}
    }
    //End code
    /**
     * Add Are to Spinner
     */
    public void addItemsToSpinner() {
        mAreaList.add("Kadubeesanahalli");
        mAreaList.add("Hsr");
        mAreaList.add("Bellandur");
        mAreaList.add("Sarajpur Road");
        mAreaList.add("Marathali");
        mSpinAdapter.notifyDataSetChanged();
        /*CustomPostRequest jsosReq = new CustomPostRequest(Request.Method.POST,
                WAppConstants.URL_GET_ALL_AREAS, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "URL_GET_ALL_AREAS Response: " + response.toString());
                if (response != null) {
                    JSONArray postJsonArray = null;
                    try {
                        postJsonArray = response.getJSONArray("area_array");
                        if(postJsonArray.length()>0) {
                            for (int i = 0; i < postJsonArray.length(); i++) {
                                String area = (String) postJsonArray.get(i);
                                mAreaList.add(area);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mSpinAdapter.notifyDataSetChanged();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", mPrefs.getString(WAppConstants.LOGGED_IN_USER_ID,"1"));
                params.put("sec_code", mPrefs.getString(WAppConstants.LOGGED_IN_USER_SEC_CODE, "ASD"));
                return params;
            }
        };
        WAppController.getInstance().addToRequestQueue(jsosReq);*/
    }
    //End code

    //My Code
   /* public void addItemsToSpinner() {
        CustomPostRequest jsosReq = new CustomPostRequest(Request.Method.POST,
                WAppConstants.URL_GET_ALL_AREAS, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "URL_GET_ALL_AREAS Response: " + response.toString());
                if (response != null) {
                    JSONArray postJsonArray = null;
                    try {
                        postJsonArray = response.getJSONArray("area_array");
                        if(postJsonArray.length()>0) {
                            for (int i = 0; i < postJsonArray.length(); i++) {
                                String area = (String) postJsonArray.get(i);
                                mAreaList.add(area);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mSpinAdapter.notifyDataSetChanged();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", mPrefs.getString(WAppConstants.LOGGED_IN_USER_ID,"1"));
                params.put("sec_code", mPrefs.getString(WAppConstants.LOGGED_IN_USER_SEC_CODE, "ASD"));
                return params;
            }
        };
        WAppController.getInstance().addToRequestQueue(jsosReq);
    }*/
    //My End

    private boolean validate() {
        boolean valid = true;
        //String name = mSignUpName.getText()+"";
       /* if (TextUtils.isEmpty(name)) {
            Snackbar.make(mAnchor,R.string.sign_up_username_err_empty,Snackbar.LENGTH_LONG).show();
            valid = false;
            return valid;
        }else {
            if(!Pattern.matches("[a-zA-Z \\./-]*", name)){
                Snackbar.make(mAnchor,R.string.sign_up_username_err_wrong,Snackbar.LENGTH_LONG).show();
                valid = false;
                return valid;
            }
        }*/
        String eMail = mSignUpEmail.getText()+"";
        if (TextUtils.isEmpty(eMail)) {
            Snackbar.make(mAnchor,R.string.sign_up_email_err_empty,Snackbar.LENGTH_LONG).show();
            valid = false;
            return valid;
        } else {
            if(!Patterns.EMAIL_ADDRESS.matcher(eMail).matches()){
                Snackbar.make(mAnchor,R.string.sign_up_email_err_wrong,Snackbar.LENGTH_LONG).show();
                valid = false;
                return valid;
            }
        }
        String userMobile = userMobileNumber.getText()+"".trim();
        if (userMobile.length() < 10){
            Snackbar.make(mAnchor,R.string.sign_up_phn_err_wrong,Snackbar.LENGTH_LONG).show();
            valid = false;
            return valid;
        }
      /*  if(mSignUpPhoneNo.isValid()) {

        }else{
            Snackbar.make(mAnchor,R.string.sign_up_phn_err_wrong,Snackbar.LENGTH_LONG).show();
            valid = false;
            return valid;
        }*/
       /* String password = mSignUpPwd.getText()+"";
        if (TextUtils.isEmpty(password)) {
            Snackbar.make(mAnchor,R.string.sign_up_pwd_err_empty,Snackbar.LENGTH_LONG).show();
            valid = false;
            return valid;
        } else {
            int pwdLength = password.length();
            if(!(pwdLength > 5 && pwdLength < 13)){
                Snackbar.make(mAnchor,R.string.sign_up_pwd_err_wrong,Snackbar.LENGTH_LONG).show();
                valid = false;
                return valid;
            }
        }*/
        return valid;
    }
    private void doRegister() {
        //sendOtpToMobileNumber("123456dfhshsvstegwiukegqwe7t0983bdjdasjva");
        Log.i("in doregister","Yes");
        CustomPostRequest jsonReqUserReg = new CustomPostRequest(Method.POST,
                WAppConstants.URL_DO_SIGN_UP, null, new Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
               // mLoading.setVisibility(View.VISIBLE);
                Log.d(TAG, "Signup Response: " + response.toString());
                if (response != null) {
                    try {
                        String status = response.getString("status");
                        if(status.equals("success")){
                           mLoading.setVisibility(View.GONE);
                           String userId = response.getString("user_id");
                           String userName = response.getString("full_name");
                           String userEmail = response.getString("email_id");
                           String userSecToken = response.getString("sec_code");
                           // System.out.print("userTocken="+userSecToken);

                           mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_ID, userId);
                           mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_NAME, userName);
                           mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_EMAIL, userEmail);
                           mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_SEC_CODE, userSecToken);
                            //My Code
                            mPrefEditor.putBoolean(WAppConstants.IS_USER_VALIDATED, true);
                            //End Code
                           mPrefEditor.commit();

                            startHomeActivity();
                           //startEmailvalActivity();
                          //sendOtpToMobileNumber(userSecToken);
                        }else{
                            String errorMsg = response.getString("error_msg");
                            Snackbar.make(mAnchor,errorMsg,Snackbar.LENGTH_LONG).show();
                            mLoading.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {e.printStackTrace();}
                }
            }
        }, new ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Snackbar.make(mAnchor,R.string.some_wrong_later,Snackbar.LENGTH_LONG).show();
                mLoading.setVisibility(View.GONE);
                Log.e(TAG, "Error: " + error.getMessage());
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                if (isLoginWithSocialsite){
                    params = socialLogedInUserDetails;
                } else{
                    //params.put("fullname", mSignUpName.getText()+"");
                    params.put("fullname", mSignUpEmail.getText()+"");
                    params.put("email", mSignUpEmail.getText()+"".toLowerCase(Locale.ENGLISH));
                   // params.put("password", mSignUpPwd.getText()+"");
                    params.put("password",otpCode);
                    //params.put("password_confirm",  mSignUpPwd.getText()+"");
                    params.put("password_confirm",otpCode);
                    //params.put("mobile_number", mSignUpPhoneNo.getNumber()+"");
                    params.put("mobile_number","+91"+userMobileNumber.getText()+"".trim());
                }

                return params;
            }
        };
        if(AppUtils.isNetworkAvailable(this)){
            WAppController.getInstance().addToRequestQueue(jsonReqUserReg);
        }else{
            Snackbar.make(mAnchor,R.string.no_connection_later,Snackbar.LENGTH_LONG).show();
            mLoading.setVisibility(View.GONE);
        }
    }

    private void startEmailvalActivity() {
        finish();
        Intent validationIntent = new Intent(this, EmailValidationActivity.class);
        this.startActivity(validationIntent);
        this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    /**
     * Send otp Code to User Mobile Number
     * @param secretCode
     */
    private void sendOtpToMobileNumber(final String secretCode){

        //showView();
        if(AppUtils.isNetworkAvailable(this)){
            //WAppController.getInstance().addToRequestQueue(jsonReqUserReg);
            showView();
            Toast.makeText(getApplicationContext(),"Code is sent on your mobile number"+ userMobileNumber.getText(),Toast.LENGTH_LONG).show();
            mLoading.setVisibility(View.GONE);
            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {


                    StringRequest stringRequest = new StringRequest(Request.Method.POST, WAppConstants.SMS_SEND_URL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    //showView();
                                    //startEmailvalActivity();
                                    //Toast.makeText(MainActivity.this,response,Toast.LENGTH_LONG).show();
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // Toast.makeText(MainActivity.this, error.toString(),Toast.LENGTH_LONG).show();
                                    Log.i("msgerror", String.valueOf(error));
                                }
                            }) {
                        @Override
                        protected Map<String, String> getParams() {
                            String phonenumber = userMobileNumber.getText() + "".trim();
                            Log.i("sendNumber", phonenumber);
                            //phonenumber = phonenumber.replaceAll("\\+", "");
                            Map<String, String> sendSmsmap = new HashMap<String, String>();
                            sendSmsmap.put("uname", WAppConstants.SMS_SENDER_USER_NAME);
                            sendSmsmap.put("pass", WAppConstants.SMS_SENDER_USER_PASSWORD);
                            sendSmsmap.put("send", "VRWGLE");
                            sendSmsmap.put("dest", phonenumber);
                            sendSmsmap.put("msg", "Hi!!! Welcome to Wiglee. Your verification code is " + secretCode);
                            sendSmsmap.put("concat", "1");
                            sendSmsmap.put("vp", "180");

                            return sendSmsmap;
                        }

                    };

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringRequest);


                }
            });
        }else{
            Snackbar.make(mAnchor,R.string.no_connection_later,Snackbar.LENGTH_LONG).show();
            mLoading.setVisibility(View.GONE);
        }


    }
    //My Code
    private void validateOtp(String optpNumber){
       String otpNumberString =  mETValidationCode.getText()+"".trim();
        if (optpNumber.equals(otpNumberString)){
            iscodeValidate = true;
            showView();
        }
        else{
            Snackbar.make(mAnchor,"Code is not matched",Snackbar.LENGTH_LONG).show();
        }

    }

    /**
     * Hide view when activity start
     */
    private void hideView(){
        mETValidationCode.setVisibility(View.GONE);
        mSignUpEmail.setVisibility(View.GONE);
       // mSignUpPwd.setVisibility(View.GONE);
        facebookloginButton.setVisibility(View.GONE);
        googlesigninButton.setVisibility(View.GONE);
        mEmailSignUpBtn.setVisibility(View.VISIBLE);
    }

    /**
     * Show The view
     */
    private void showView(){
        mETValidationCode.setVisibility(View.VISIBLE);
        if (iscodeValidate){
            mSignUpEmail.setVisibility(View.VISIBLE);
           // mSignUpPwd.setVisibility(View.VISIBLE);
            facebookloginButton.setVisibility(View.VISIBLE);
            googlesigninButton.setVisibility(View.VISIBLE);
            mEmailSignUpBtn.setVisibility(View.VISIBLE);

            iscodeValidate = false;
        }
    }
    private void startHomeActivity() {
        mLoading.setVisibility(View.GONE);
        finish();
        Intent homeIntent = new Intent(this, WHomeActivity.class);
        this.startActivity(homeIntent);
        this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    //End
    private void callLogin(){
        Intent loginIntent = new Intent(WSignUpActivity.this,WLoginActivity.class);
        this.startActivity(loginIntent);
        finish();
        this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        GoogleApiClient.connect();
        /*Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "WSignUp Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.cust.wiglee.activity/http/host/path")
        );
        //AppIndex.AppIndexApi.start(GoogleApiClient, viewAction);*/
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        /*Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "WSignUp Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.cust.wiglee.activity/http/host/path")
        );
        AppIndex.AppIndexApi.end(GoogleApiClient, viewAction);*/
        GoogleApiClient.disconnect();
    }
}
