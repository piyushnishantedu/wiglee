package com.cust.wiglee.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.cust.wiglee.R;
import com.cust.wiglee.app.AppUtils;
import com.cust.wiglee.app.WAppConstants;
import com.cust.wiglee.app.WAppController;
import com.cust.wiglee.volley.CustomPostRequest;
import com.greenhalolabs.emailautocompletetextview.EmailAutoCompleteTextView;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

public class ForgetPwdActivity extends AppCompatActivity {
    private String TAG = WAppConstants.APP_TAG;
    private SharedPreferences mPrefs;
    private SharedPreferences.Editor mPrefEditor;
    private TextView mTopMsg;
    private EmailAutoCompleteTextView mETEmail;
    private RelativeLayout mLoading;
    private Button mResetBtn;
    private FrameLayout mAnchor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPrefs = WAppController.getPreferences(this);
        mPrefEditor = WAppController.getPreferences(this).edit();
        setContentView(R.layout.activity_forget_pwd);
        Intent intent = getIntent();
        String mEmail = intent.getStringExtra("email");
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_forget_pwd);
        if (mToolbar != null) {
            mToolbar.getMenu().clear();
            mToolbar.setTitle("");
            //mToolbar.setNavigationIcon(R.drawable.ic_clear_white);
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
        mTopMsg  = (TextView) findViewById(R.id.tv_top_msg);
        mETEmail = (EmailAutoCompleteTextView) findViewById(R.id.eact_forget_pwd);
        mETEmail.setText(mEmail);
        mResetBtn = (Button) findViewById(R.id.btn_forget_pwd);
        mResetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doResetForgetPwd();
            }
        });
        mLoading = (RelativeLayout) findViewById(R.id.ll_loading_forget_pwd);
        mAnchor = (FrameLayout) findViewById(R.id.forget_pwd_anchor);
    }

    private void doResetForgetPwd() {
        if(validate()) {
            AppUtils.hideSoftKeyboard(mETEmail);
            mResetBtn.setEnabled(false);
            doResetForgetPwdCall();
        }
    }
    CustomPostRequest jsonReqResetPwd;
    private void doResetForgetPwdCall() {
        jsonReqResetPwd = new CustomPostRequest(Request.Method.POST,
                WAppConstants.URL_DO_RESET_PWD, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "Reset Pwd Response: " + response.toString());
                if (response != null) {
                    try {
                        String status = response.getString("status");
                        String msg = response.getString("msg");
                        mResetBtn.setEnabled(true);
                        mLoading.setVisibility(View.GONE);
                        if(status.equals("success")){
                            Snackbar.make(mAnchor, msg, Snackbar.LENGTH_LONG).show();
                        }else{
                            Snackbar.make(mAnchor, msg, Snackbar.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {e.printStackTrace();}
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mResetBtn.setEnabled(true);
                mLoading.setVisibility(View.GONE);
                Log.e(TAG, "Error: " + error.getMessage());
                Snackbar.make(mAnchor, R.string.some_wrong_later, Snackbar.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("sent_email", mETEmail.getText()+"".trim());
                return params;
            }
        };
        if(AppUtils.isNetworkAvailable(ForgetPwdActivity.this)){
            WAppController.getInstance().addToRequestQueue(jsonReqResetPwd);
            mLoading.setVisibility(View.VISIBLE);
        }else{
            Snackbar mSnackbar = Snackbar.make(mAnchor, R.string.no_connection, Snackbar.LENGTH_LONG);
            mSnackbar.show();
        }
    }
    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
    private boolean validate() {
        boolean valid = true;
        String eMail = mETEmail.getText()+"";
        if (TextUtils.isEmpty(eMail)) {
            Snackbar.make(mAnchor, R.string.user_id_valid, Snackbar.LENGTH_LONG).show();
            valid = false;
            return valid;
        } else {
            if(!android.util.Patterns.EMAIL_ADDRESS.matcher(eMail).matches()){
                Snackbar.make(mAnchor, R.string.sign_up_email_err_wrong, Snackbar.LENGTH_LONG).show();
                valid = false;
                return valid;
            }
        }
        return valid;
    }
}
