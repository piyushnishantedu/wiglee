package com.cust.wiglee.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.cust.wiglee.R;
import com.cust.wiglee.app.AppUtils;
import com.cust.wiglee.app.WAppConstants;
import com.cust.wiglee.app.WAppController;
import com.cust.wiglee.volley.CustomPostRequest;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

public class EmailValidationActivity extends AppCompatActivity {
    private String TAG = WAppConstants.APP_TAG;
    private SharedPreferences mPrefs;
    private SharedPreferences.Editor mPrefEditor;
    private TextView mTopMsg;
    private AppCompatEditText mETValidationCode;
    private RelativeLayout mLoading;
    private Button mValidateBtn;
    private FrameLayout mAnchor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPrefs = WAppController.getPreferences(this);
        mPrefEditor = WAppController.getPreferences(this).edit();
        setContentView(R.layout.activity_email_validation);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_email_val);
        if (mToolbar != null) {
            mToolbar.getMenu().clear();
            mToolbar.setTitle("");
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mToolbar.setNavigationIcon(R.drawable.ic_clear_white);
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
        mTopMsg  = (TextView) findViewById(R.id.tv_top_msg);
        String topMsg=String.format("We have sent you a verification code in the registered email address: %s .Please enter the verification code below", mPrefs.getString(WAppConstants.LOGGED_IN_USER_EMAIL, ""));
        mTopMsg.setText(topMsg);
        mETValidationCode  = (AppCompatEditText) findViewById(R.id.et_validate_email);
        mValidateBtn = (Button) findViewById(R.id.btn_validate_email);
        mValidateBtn.setEnabled(false);
        mValidateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doValidation();
            }
        });
        mETValidationCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                if (arg0.length() == 8) {
                    mValidateBtn.setEnabled(true);
                    doValidation();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {
            }
        });
        mLoading = (RelativeLayout) findViewById(R.id.ll_loading_validate_email);
        mAnchor = (FrameLayout) findViewById(R.id.email_val_anchor);
    }

    private void doValidation() {
        AppUtils.hideSoftKeyboard(mETValidationCode);
        mValidateBtn.setEnabled(false);
        doValidationCall();
    }
    CustomPostRequest jsonReqUseVal;
    private void doValidationCall() {

       jsonReqUseVal = new CustomPostRequest(Request.Method.POST,
                WAppConstants.URL_DO_EMAIL_VALIDATION, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "Email Validation Response: " + response.toString());
                mLoading.setVisibility(View.GONE);
                if (response != null) {
                    try {
                        String status = response.getString("status");
                        if(status.equals("success")){
                            mLoading.setVisibility(View.GONE);
                            mValidateBtn.setEnabled(false);
                            mPrefEditor.putBoolean(WAppConstants.IS_USER_VALIDATED, true);
                            mPrefEditor.commit();
                            startHomeActivity();
                        }else{
                            Snackbar.make(mAnchor,"Wrong verification code", Snackbar.LENGTH_LONG).show();
                            mPrefEditor.putBoolean(WAppConstants.IS_USER_VALIDATED, false);
                            mPrefEditor.commit();
                            mValidateBtn.setEnabled(true);
                        }
                    } catch (JSONException e) {e.printStackTrace();}
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mValidateBtn.setEnabled(true);
                mLoading.setVisibility(View.GONE);
                Log.e(TAG, "Email Validation Error: " + error.getMessage());
                Snackbar.make(mAnchor,"Something went wrong!", Snackbar.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", mPrefs.getString(WAppConstants.LOGGED_IN_USER_ID, ""));
                params.put("svn", mETValidationCode.getText()+"".trim());
                params.put("sec_code", mPrefs.getString(WAppConstants.LOGGED_IN_USER_SEC_CODE, ""));
                return params;
            }
        };
        if(AppUtils.isNetworkAvailable(EmailValidationActivity.this)){
            WAppController.getInstance().addToRequestQueue(jsonReqUseVal);
            mLoading.setVisibility(View.VISIBLE);
        }else{
            Snackbar mSnackbar = Snackbar.make(mAnchor, R.string.no_connection, Snackbar.LENGTH_LONG);
            mSnackbar.setAction("Retry", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    WAppController.getInstance().addToRequestQueue(jsonReqUseVal);
                    mLoading.setVisibility(View.VISIBLE);
                }
            });
            mSnackbar.setActionTextColor(Color.RED);
            mSnackbar.show();
        }
    }

    private void startHomeActivity() {
        finish();
        Intent homeIntent = new Intent(this, WHomeActivity.class);
        this.startActivity(homeIntent);
        this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}
