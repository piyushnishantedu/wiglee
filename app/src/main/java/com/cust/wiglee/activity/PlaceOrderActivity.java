package com.cust.wiglee.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.cust.wiglee.R;
import com.cust.wiglee.adapters.AreaSpinnerAdapter;
import com.cust.wiglee.adapters.PlaceOrderListAdapter;
import com.cust.wiglee.app.AppUtils;
import com.cust.wiglee.app.WAppConstants;
import com.cust.wiglee.app.WAppController;
import com.cust.wiglee.bean.PlaceOrderBean;
import com.cust.wiglee.view.common.FontTextView;
import com.cust.wiglee.volley.CustomPostRequest;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;

import net.rimoto.intlphoneinput.IntlPhoneInput;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PlaceOrderActivity extends AppCompatActivity {
    private String TAG = WAppConstants.APP_TAG;
    private static final String MEALS_LIST = "meals_list";
    private ListView mListView;
    private RelativeLayout mEmptyErrMsg;
    private LinearLayout mLoading;
    private RelativeLayout mSuccessLayout;
    private FontTextView mSuccessMsgTv;
    private Button mSuccessContinue;

    private FontTextView mEmptyErrMsgTv;
    private ImageView mNoConnIcon;
    private ArrayList<PlaceOrderBean> mList;
    private PlaceOrderListAdapter mListAdapter;
    private SharedPreferences mPrefs;
    private FrameLayout mAnchor;

    private LinearLayout mFooter;
    private TextView mFooterTotal;
    //Discount Variable
    private TextView mFooterDiscount;
    private IntlPhoneInput mFooterMobNo;
    private TextView mFooterAddress;
    private AppCompatEditText mFooterAddressEdit;
    private TextView mFooterAddressOfc;
    private AppCompatEditText mFooterAddressEditOfc;
    private TextView mFooterAddressEditBtn;
    private AppCompatCheckBox mCBAddressHome;
    private AppCompatCheckBox mCBAddressOffice;
    private Spinner spinnerArea;
    public Button mFooterBtnOrder;
    private int totalOrderValue = 0;
    private boolean isOrderPlaced = false;
    private AreaSpinnerAdapter mSpinAdapter;
    ArrayList<String> mAreaList = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPrefs = WAppController.getPreferences(this);
        setContentView(R.layout.activity_place_order);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_place_order);
        if (mToolbar != null) {
            mToolbar.getMenu().clear();
            mToolbar.setTitle("");
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            TextView mTvTitle = (TextView) mToolbar.findViewById(R.id.toolbar_place_order_title);
            mTvTitle.setText("Place Order");
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
        mAnchor = (FrameLayout) findViewById(R.id.place_order_anchor);
        mEmptyErrMsg = (RelativeLayout) findViewById(R.id.place_order_no_conn);
        mEmptyErrMsgTv = (FontTextView) findViewById(R.id.ftv_no_conn_msg_place_order);
        mNoConnIcon = (ImageView) findViewById(R.id.iv_no_conn_place_order);
        mLoading = (LinearLayout) findViewById(R.id.loading_place_order);
        mSuccessLayout = (RelativeLayout) findViewById(R.id.success_place_order);
        mSuccessMsgTv = (FontTextView) findViewById(R.id.ftv_order_success_desc);
        mSuccessContinue = (Button) findViewById(R.id.btn_continue);
        mSuccessContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent startGlobalActivity = new Intent(PlaceOrderActivity.this, WHomeActivity.class);
                startGlobalActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(startGlobalActivity);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        mListView = (ListView) findViewById(R.id.place_order_list);
        mList = new ArrayList<PlaceOrderBean>();
        mListAdapter = new PlaceOrderListAdapter(this, mList,mAnchor);
        mListView.setAdapter(mListAdapter);
       loadOrder(false);
        mEmptyErrMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppUtils.isNetworkAvailable(PlaceOrderActivity.this)) {
                    mEmptyErrMsg.setVisibility(View.GONE);
                    mLoading.setVisibility(View.VISIBLE);
                    loadOrder(false);
                }
            }
        });

        mFooter = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.footer_place_order, null);
        mFooterTotal = (TextView) mFooter.findViewById(R.id.contact_dtls_total_val);
        mFooterDiscount = (TextView) mFooter.findViewById(R.id.contact_dtls_dis_val);
        mFooterMobNo = (IntlPhoneInput) mFooter.findViewById(R.id.eact_mob_no);

        spinnerArea = (Spinner) mFooter.findViewById(R.id.spinner_area);
        mSpinAdapter = new AreaSpinnerAdapter(getApplicationContext(), mAreaList, true);
        spinnerArea.setAdapter(mSpinAdapter);
        spinnerArea.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapter, View v,int position, long id) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });

        addItemsToSpinner();

        mCBAddressHome = (AppCompatCheckBox) mFooter.findViewById(R.id.cb_my_address_home);
        mCBAddressOffice = (AppCompatCheckBox) mFooter.findViewById(R.id.cb_my_address_ofc);
        mCBAddressHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // you might keep a reference to the CheckBox to avoid this class cast
                boolean checked = ((AppCompatCheckBox) v).isChecked();
                if (checked) {
                    mCBAddressHome.setChecked(true);
                    mCBAddressOffice.setChecked(false);
                } else {
                    mCBAddressHome.setChecked(false);
                    mCBAddressOffice.setChecked(true);
                }
            }
        });
        mCBAddressOffice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // you might keep a reference to the CheckBox to avoid this class cast
                boolean checked = ((AppCompatCheckBox) v).isChecked();
                if (checked) {
                    mCBAddressHome.setChecked(false);
                    mCBAddressOffice.setChecked(true);
                } else {
                    mCBAddressHome.setChecked(true);
                    mCBAddressOffice.setChecked(false);
                }
            }
        });

        mFooterAddress = (TextView) mFooter.findViewById(R.id.tv_current_address);
        mFooterAddressEdit = (AppCompatEditText) mFooter.findViewById(R.id.et_edit_address);
        mFooterAddressOfc = (TextView) mFooter.findViewById(R.id.tv_current_address_ofc);
        mFooterAddressEditOfc = (AppCompatEditText) mFooter.findViewById(R.id.et_edit_address_ofc);
        mListView.addFooterView(mFooter, null, false);

        mFooterMobNo. setNumber(mPrefs.getString(WAppConstants.LOGGED_IN_USER_MOBILE_NO,""));
       // mFooterAddress.setText(mPrefs.getString(WAppConstants.LOGGED_IN_USER_ADDRESS,"").equals("")?"Add your address" : mPrefs.getString(WAppConstants.LOGGED_IN_USER_ADDRESS,"")+"");
        mFooterAddress.setText(mPrefs.getString(WAppConstants.LOGGED_IN_USER_ADDRESS,"")+"");
        mFooterAddressEdit.setText(mPrefs.getString(WAppConstants.LOGGED_IN_USER_ADDRESS,"")+"");
        mFooterAddressOfc.setText(mPrefs.getString(WAppConstants.LOGGED_IN_USER_ADDRESS_OFFICE,"")+"");
        mFooterAddressEditOfc.setText(mPrefs.getString(WAppConstants.LOGGED_IN_USER_ADDRESS_OFFICE, "") + "");
        mFooterAddressEdit.post(new Runnable() {
            @Override
            public void run() {
                int posAdd = mFooterAddressEdit.getText().length();
                mFooterAddressEdit.setSelection(posAdd);
                int posAddOfc = mFooterAddressEditOfc.getText().length();
                mFooterAddressEditOfc.setSelection(posAddOfc);
            }
        });
        //CODE to Enable Edit address
        mFooterAddress.setVisibility(View.GONE);
        mFooterAddressEdit.setVisibility(View.VISIBLE);
        mFooterAddressOfc.setVisibility(View.GONE);
        mFooterAddressEditOfc.setVisibility(View.VISIBLE);
        mFooterAddressEdit.requestFocus();
        AppUtils.showSoftInput(mFooterAddressEdit);
        //END

        mFooterAddressEditBtn = (TextView) mFooter.findViewById(R.id.tv_current_address_edit);
        mFooterAddressEditBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*mFooterAddress.setVisibility(View.GONE);
                mFooterAddressEdit.setVisibility(View.VISIBLE);
                mFooterAddressOfc.setVisibility(View.GONE);
                mFooterAddressEditOfc.setVisibility(View.VISIBLE);
                mFooterAddressEdit.requestFocus();
                AppUtils.showSoftInput(mFooterAddressEdit);*/
            }
        });

        mFooterBtnOrder = (Button) mFooter.findViewById(R.id.btn_place_order);
        mFooterBtnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    AppUtils.hideSoftKeyboard(mFooterAddressEdit);
                    String allQnty = "";
                    String allCartId = "";
                    String mealId = "";
                    if (mList.size() > 1) {
                        for (int i = 0; i < mList.size(); i++) {
                            if (i == mList.size() - 1) {
                                allQnty = allQnty + mList.get(i).getQuantity();
                                allCartId = allCartId + mList.get(i).getCartId();
                                mealId = mealId + mList.get(i).getMealId();
                            } else {
                                allQnty = allQnty + mList.get(i).getQuantity() + "_";
                                allCartId = allCartId + mList.get(i).getCartId() + "_";
                                mealId = mealId + mList.get(i).getMealId() + "_";
                            }
                        }
                    } else {
                        allQnty = mList.get(0).getQuantity() + "";
                        allCartId = mList.get(0).getCartId() + "";
                        mealId = mList.get(0).getMealId() + "";
                    }
                    doOrder(allQnty, allCartId, mealId);
                }
            }
        });
        //Code for getting selected place details
        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                Log.i("TAG", "Place: " + place.getName());
                mFooterAddressEdit.setText(place.getAddress());
               // mFooterAddressEditOfc.setText(place.getAddress());

            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i("Tag", "An error occurred: " + status);
            }
        });



        
    }

    public void addItemsToSpinner() {
        CustomPostRequest jsosReq = new CustomPostRequest(Request.Method.POST,
                WAppConstants.URL_GET_ALL_AREAS, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "URL_GET_ALL_AREAS Response: " + response.toString());
                if (response != null) {
                    JSONArray postJsonArray = null;
                    try {
                        postJsonArray = response.getJSONArray("area_array");
                        if(postJsonArray.length()>0) {
                            for (int i = 0; i < postJsonArray.length(); i++) {
                                String area = (String) postJsonArray.get(i);
                                mAreaList.add(area);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mSpinAdapter.notifyDataSetChanged();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", mPrefs.getString(WAppConstants.LOGGED_IN_USER_ID,"1"));
                params.put("sec_code", mPrefs.getString(WAppConstants.LOGGED_IN_USER_SEC_CODE, "ASD"));
                return params;
            }
        };
        WAppController.getInstance().addToRequestQueue(jsosReq);
    }

    private void loadOrder(final boolean isRefreshReq) {
            if(isRefreshReq){
                mLoading.setVisibility(View.GONE);
            }else{
                mLoading.setVisibility(View.VISIBLE);
            }
            mEmptyErrMsg.setVisibility(View.GONE);

        CustomPostRequest jsonAllMealsReq = new CustomPostRequest(Request.Method.POST,
                WAppConstants.URL_GET_PLACE_ORDER, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "GET_PLACE_ORDER Response: " + response.toString());
                if (response != null) {
                    parseOrderJsonResponse(response);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(!isRefreshReq){
                        mLoading.setVisibility(View.GONE);
                        mEmptyErrMsg.setVisibility(View.VISIBLE);
                        mNoConnIcon.setVisibility(View.VISIBLE);
                        mEmptyErrMsgTv.setText(getResources().getString(R.string.no_conn_retry));
                }else{
                    Snackbar.make(mAnchor, "Failed to refresh", Snackbar.LENGTH_SHORT).show();
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", mPrefs.getString(WAppConstants.LOGGED_IN_USER_ID,"1"));
                params.put("sec_code", mPrefs.getString(WAppConstants.LOGGED_IN_USER_SEC_CODE, "ASD"));
                return params;
            }
        };
        WAppController.getInstance().addToRequestQueue(jsonAllMealsReq);
    }
    private void parseOrderJsonResponse(JSONObject response) {
        try {
            JSONArray postJsonArray = response.getJSONArray("user_order_feed");
            if(postJsonArray.length()>0){
                for (int i = 0; i < postJsonArray.length(); i++) {
                    JSONObject feedObj = (JSONObject) postJsonArray.get(i);
                    PlaceOrderBean item = new PlaceOrderBean();
                    item.setMealId(feedObj.getInt("meal_id"));
                    item.setCartId(feedObj.getInt("cart_id"));
                    item.setMealName(feedObj.getString("meal_name"));
                    item.setMealPrice(feedObj.getInt("meal_price"));
                    item.setMealImageId(feedObj.getString("meal_image_id"));
                    item.setQuantity(feedObj.getInt("quantity"));
                    item.setProcessFlag(feedObj.getString("process_flag"));
                    mList.add(item);
                }
            }
            if(mList.size() < 1){
                mLoading.setVisibility(View.GONE);
                mEmptyErrMsg.setVisibility(View.VISIBLE);
                mNoConnIcon.setVisibility(View.GONE);
                mEmptyErrMsgTv.setText("0pss!! No item found");
            }else{
                mLoading.setVisibility(View.GONE);
                mEmptyErrMsg.setVisibility(View.GONE);
            }
            mListAdapter.notifyDataSetChanged();
            for (int i = 0; i < mList.size(); i++) {
                totalOrderValue = totalOrderValue + mList.get(i).getMealTotal();
            }
            //mFooterTotal.setText(String.format(getString(R.string.string_price),totalOrderValue+""));

            double discount = totalOrderValue*WAppConstants.discountPercentage/100;

            mFooterDiscount.setText(String.format(getString(R.string.string_price),(int) discount+""));
            totalOrderValue = totalOrderValue - (int) discount;

            mFooterTotal.setText(String.format(getString(R.string.string_price),totalOrderValue+""));

        } catch (JSONException e) {
            mLoading.setVisibility(View.GONE);
            e.printStackTrace();
        }
    }

    private void doOrder(final String allQnty, final String allCartId, final String mealId) {
        mLoading.setVisibility(View.VISIBLE);
        CustomPostRequest jsonReq = new CustomPostRequest(Request.Method.POST,
                WAppConstants.URL_DO_ORDER, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "Order response : "+response);
                if (response != null) {
                    try {
                        String result = response.getString("status");
                        if(result.equals("success")){
                            isOrderPlaced = true;
                            mLoading.setVisibility(View.GONE);
                            String orderId = response.getString("order_id");
                            mSuccessLayout.setVisibility(View.VISIBLE);
                            mSuccessMsgTv.setText(String.format(getString(R.string.order_success_msg),orderId));
                        }else{
                            mLoading.setVisibility(View.GONE);
                            Snackbar mSnackbar = Snackbar.make(mAnchor, getString(R.string.some_wrong_later), Snackbar.LENGTH_LONG);
                            mSnackbar.show();
                        }
                    } catch (JSONException e) {e.printStackTrace();}
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                mLoading.setVisibility(View.GONE);
                Snackbar mSnackbar = Snackbar.make(mAnchor, getString(R.string.some_wrong_later), Snackbar.LENGTH_LONG);
                mSnackbar.show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("all_cart_id", allCartId);
                params.put("all_quantity", allQnty);
                params.put("all_meal_id", mealId);
                params.put("mobile_no", mFooterMobNo.getNumber()+"");
                if(mCBAddressHome.isChecked()){
                    params.put("address", TextUtils.isEmpty(mFooterAddressEdit.getText())?mFooterAddress.getText()+"": mFooterAddressEdit.getText()+"");
                }else{
                    params.put("address", TextUtils.isEmpty(mFooterAddressEditOfc.getText())?mFooterAddressOfc.getText()+"": mFooterAddressEditOfc.getText()+"");
                }
                params.put("total_price", totalOrderValue+"");
                params.put("area", spinnerArea.getSelectedItem()+"");
                Log.e(TAG, "all_cart_id: " + allCartId);
                Log.e(TAG, "allQnty: " + allQnty);
                Log.e(TAG, "mealId: " + mealId);
                Log.e(TAG, "mobile_no: " + mFooterMobNo.getNumber()+"");
                Log.e(TAG, "address: " + (TextUtils.isEmpty(mFooterAddressEdit.getText())?mFooterAddress.getText()+"": mFooterAddressEdit.getText()+""));


                params.put("user_id", mPrefs.getString(WAppConstants.LOGGED_IN_USER_ID, ""));
                params.put("sec_code", mPrefs.getString(WAppConstants.LOGGED_IN_USER_SEC_CODE,""));
                return params;
            }
        };
        WAppController.getInstance().addToRequestQueue(jsonReq);
    }

    private boolean validate() {
        boolean valid = true;
        if(mFooterAddressEdit.getVisibility() == View.VISIBLE) {
            String address = mFooterAddressEdit.getText()+"";
            if (TextUtils.isEmpty(address)) {
                Snackbar.make(mAnchor, R.string.address_err_empty, Snackbar.LENGTH_LONG).show();
                valid = false;
                return valid;
            }
        }else{
            String address = mFooterAddress.getText()+"";
            if (TextUtils.isEmpty(address)) {
                Snackbar.make(mAnchor, R.string.address_err_empty, Snackbar.LENGTH_LONG).show();
                valid = false;
                return valid;
            }
        }
        if(mFooterMobNo.isValid()) {

        }else{
            Snackbar.make(mAnchor,R.string.sign_up_phn_err_wrong,Snackbar.LENGTH_LONG).show();
            valid = false;
            return valid;
        }
        //Code to check minimum Order price is 100
        int totalmealPrice = 0;
        for (int i = 0; i < mList.size(); i++) {
            totalmealPrice = totalmealPrice + mList.get(i).getMealTotal();
        }
        if (totalmealPrice < 100){
            Snackbar.make(mAnchor,"Please make order more than ₹100 ",Snackbar.LENGTH_LONG).show();
            valid = false;
            return  valid;
        }
        return valid;
    }
    @Override
    protected void onStart() {
        super.onStart();
    }
    @Override
    protected void onStop() {
        super.onStop();
    }
    //private SweetAlertDialog mSADialog;
    @Override
    public void onDestroy(){
        /*if(mBottomSheet != null && mBottomSheet.isShowing()){
            mBottomSheet.dismiss();
        }
        if(mSADialog != null && mSADialog.isShowing()){
            mSADialog.dismiss();
        }*/
        super.onDestroy();
    }
    @Override
    protected void onResume() {
        super.onResume();
    }
    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        if(isOrderPlaced){
            finish();
            Intent startGlobalActivity = new Intent(PlaceOrderActivity.this, WHomeActivity.class);
            startGlobalActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(startGlobalActivity);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }else {
            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }
    }

}