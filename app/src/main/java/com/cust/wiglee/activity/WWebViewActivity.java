package com.cust.wiglee.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;
import com.cust.wiglee.R;


public class WWebViewActivity extends AppCompatActivity {
    private String mUrl;
    private String mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        mUrl = intent.getStringExtra("passed_url");
        mTitle = intent.getStringExtra("passed_title");
        setContentView(R.layout.activity_wweb_view);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_wev_view);
        if (mToolbar != null) {
            mToolbar.getMenu().clear();
            mToolbar.setTitle("");
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            TextView mTvTitle = (TextView) mToolbar.findViewById(R.id.toolbar_wev_view_title);
            mTvTitle.setText(mTitle);
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
        WebView myWebView = (WebView) findViewById(R.id.web_view);
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.loadUrl(mUrl);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}
