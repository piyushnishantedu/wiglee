package com.cust.wiglee.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.cust.wiglee.R;
import com.cust.wiglee.adapters.FoodTypePagerAdapter;
import com.cust.wiglee.listner.FoodItemClickistener;
import com.cust.wiglee.modals.FoodItemModal;

public class FoodTabbarActivity extends AppCompatActivity  implements FoodItemClickistener {
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private PagerAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_tabbar);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_view_meals);
        if (mToolbar != null) {
            mToolbar.getMenu().clear();
            mToolbar.setTitle("");
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

           /* RelativeLayout mIvCart = (RelativeLayout) mToolbar.findViewById(R.id.toolbar_cart);
            mIvCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent myCartIntent = new Intent(FoodTabbarActivity.this, MyCartActivity.class);
                    FoodTabbarActivity.this.startActivity(myCartIntent);
                    FoodTabbarActivity.this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });*/



            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }



         tabLayout = (TabLayout) findViewById(R.id.foodcategory_tabbar);
         tabLayout.addTab(tabLayout.newTab().setText("Veg"),true);
        tabLayout.addTab(tabLayout.newTab().setText("Non Veg"),true);
        tabLayout.addTab(tabLayout.newTab().setText("Both"),true);

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager = (ViewPager) findViewById(R.id.foodcategory_viewpager);

         adapter = new FoodTypePagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        //tabLayout.setupWithViewPager(viewPager);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }
    @Override
    public void onfoodItemClick(int position, FoodItemModal foodItemObject) {
        Log.i("food posi and name",""+foodItemObject.getMfodname()+" "+foodItemObject.getMfoodDetail());
    }

}
