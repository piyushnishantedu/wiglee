package com.cust.wiglee.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.cust.wiglee.R;
import com.cust.wiglee.adapters.MyOrderListAdapter;
import com.cust.wiglee.app.AppUtils;
import com.cust.wiglee.app.WAppConstants;
import com.cust.wiglee.app.WAppController;
import com.cust.wiglee.bean.MyOrderBean;
import com.cust.wiglee.view.common.FontTextView;
import com.cust.wiglee.volley.CustomPostRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyOrderActivity extends AppCompatActivity {
    private String TAG = WAppConstants.APP_TAG;
    private static final String MY_ORDER_LIST = "my_order_list";
    private ListView mListView;
    private RelativeLayout mEmptyErrMsg;
    private LinearLayout mLoading;
    private FontTextView mEmptyErrMsgTv;
    private ImageView mNoConnIcon;
    private ArrayList<MyOrderBean> mList;
    private MyOrderListAdapter mListAdapter;
    private SharedPreferences mPrefs;
    private FrameLayout mAnchor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPrefs = WAppController.getPreferences(this);
        setContentView(R.layout.activity_my_order);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_my_order);
        if (mToolbar != null) {
            mToolbar.getMenu().clear();
            mToolbar.setTitle("");
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            TextView mTvTitle = (TextView) mToolbar.findViewById(R.id.toolbar_my_order_title);
            mTvTitle.setText("My Orders");
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
        mAnchor = (FrameLayout) findViewById(R.id.my_order_anchor);
        mEmptyErrMsg = (RelativeLayout) findViewById(R.id.my_order_no_conn);
        mEmptyErrMsgTv = (FontTextView) findViewById(R.id.ftv_no_conn_msg_my_order);
        mNoConnIcon = (ImageView) findViewById(R.id.iv_no_conn_my_order);
        mLoading = (LinearLayout) findViewById(R.id.loading_my_order);
        mListView = (ListView) findViewById(R.id.my_order_list);
        mList = new ArrayList<MyOrderBean>();
        mListAdapter = new MyOrderListAdapter(this, mList,mAnchor);
        mListView.setAdapter(mListAdapter);
       loadOrder(false);
        mEmptyErrMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppUtils.isNetworkAvailable(MyOrderActivity.this)) {
                    mEmptyErrMsg.setVisibility(View.GONE);
                    mLoading.setVisibility(View.VISIBLE);
                    loadOrder(false);
                }
            }
        });
        
    }

    private void loadOrder(final boolean isRefreshReq) {
            if(isRefreshReq){
                mLoading.setVisibility(View.GONE);
            }else{
                mLoading.setVisibility(View.VISIBLE);
            }
            mEmptyErrMsg.setVisibility(View.GONE);


        CustomPostRequest jsonReq = new CustomPostRequest(Request.Method.POST,
                WAppConstants.URL_GET_MY_ORDER, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "GET_MY_ORDER Response: " + response.toString());
                if (response != null) {
                    parseOrderJsonResponse(response);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(!isRefreshReq){
                        mLoading.setVisibility(View.GONE);
                        mEmptyErrMsg.setVisibility(View.VISIBLE);
                        mNoConnIcon.setVisibility(View.VISIBLE);
                        mEmptyErrMsgTv.setText(getResources().getString(R.string.no_conn_retry));
                }else{
                    Snackbar.make(mAnchor, "Failed to refresh", Snackbar.LENGTH_SHORT).show();
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", mPrefs.getString(WAppConstants.LOGGED_IN_USER_ID,"1"));
                params.put("sec_code", mPrefs.getString(WAppConstants.LOGGED_IN_USER_SEC_CODE, ""));

                return params;
            }
        };
        WAppController.getInstance().addToRequestQueue(jsonReq);
    }
    private void parseOrderJsonResponse(JSONObject response) {
        try {
            JSONArray postJsonArray = response.getJSONArray("order_details");
            if(postJsonArray.length()>0){
                for (int i = 0; i < postJsonArray.length(); i++) {
                    JSONObject feedObj = (JSONObject) postJsonArray.get(i);
                    MyOrderBean item = new MyOrderBean();
                    item.setOrderId(feedObj.getInt("order_id"));
                    item.setOrderDate(feedObj.getString("order_date"));
                    item.setOrderStatus(feedObj.getString("status"));
                    item.setOrderPrice(feedObj.getString("total_price"));
                    mList.add(item);
                }
            }
            if(mList.size() < 1){
                mLoading.setVisibility(View.GONE);
                mEmptyErrMsg.setVisibility(View.VISIBLE);
                mNoConnIcon.setVisibility(View.GONE);
                mEmptyErrMsgTv.setText("0pss!! No orders found");
            }else{
                mLoading.setVisibility(View.GONE);
                mEmptyErrMsg.setVisibility(View.GONE);
            }
            mListAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            mLoading.setVisibility(View.GONE);
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }
    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }
    @Override
    protected void onResume() {
        super.onResume();
    }
    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

}