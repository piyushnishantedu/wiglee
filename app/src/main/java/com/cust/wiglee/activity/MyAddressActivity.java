package com.cust.wiglee.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.cust.wiglee.R;
import com.cust.wiglee.app.AppUtils;
import com.cust.wiglee.app.WAppConstants;
import com.cust.wiglee.app.WAppController;
import com.cust.wiglee.volley.CustomPostRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MyAddressActivity extends AppCompatActivity {
    private String TAG = WAppConstants.APP_TAG;
    private SharedPreferences mPrefs;
    private SharedPreferences.Editor mPrefEditor;
    private TextView mMyCurrentAddress;
    private AppCompatEditText mMyEditedAddress;
    private TextView mMyCurrentAddressOfc;
    private AppCompatEditText mMyEditedAddressOfc;
    private RelativeLayout mLoading;
    private Button mSaveAddressBtn;
    private ImageView mEditAddressBtn;
    private FrameLayout mAnchor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPrefs = WAppController.getPreferences(this);
        mPrefEditor = WAppController.getPreferences(this).edit();
        setContentView(R.layout.activity_my_address);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_my_address);
        if (mToolbar != null) {
            mToolbar.getMenu().clear();
            mToolbar.setTitle("");
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mToolbar.setNavigationIcon(R.drawable.ic_clear_white);
            mEditAddressBtn = (ImageView) findViewById(R.id.iv_edit_address);
            mEditAddressBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mMyCurrentAddress.setVisibility(View.GONE);
                    mMyCurrentAddressOfc.setVisibility(View.GONE);
                    mSaveAddressBtn.setVisibility(View.VISIBLE);
                    mMyEditedAddress.setVisibility(View.VISIBLE);
                    mMyEditedAddressOfc.setVisibility(View.VISIBLE);
                    mMyEditedAddress.requestFocus();
                    AppUtils.showSoftInput(mMyEditedAddress);
                }
            });
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
        mMyCurrentAddress  = (TextView) findViewById(R.id.tv_current_address);
        mMyCurrentAddressOfc  = (TextView) findViewById(R.id.tv_current_address_ofc);
        mMyCurrentAddress.setText(mPrefs.getString(WAppConstants.LOGGED_IN_USER_ADDRESS,"").equals("")?"No address saved" : mPrefs.getString(WAppConstants.LOGGED_IN_USER_ADDRESS,""));
        mMyCurrentAddressOfc.setText(mPrefs.getString(WAppConstants.LOGGED_IN_USER_ADDRESS_OFFICE,"").equals("")?"No address saved" : mPrefs.getString(WAppConstants.LOGGED_IN_USER_ADDRESS_OFFICE,""));


        mMyEditedAddress  = (AppCompatEditText) findViewById(R.id.et_edit_address);
        mMyEditedAddressOfc  = (AppCompatEditText) findViewById(R.id.et_edit_address_ofc);
        mMyEditedAddress.setText(mPrefs.getString(WAppConstants.LOGGED_IN_USER_ADDRESS,""));
        mMyEditedAddressOfc.setText(mPrefs.getString(WAppConstants.LOGGED_IN_USER_ADDRESS_OFFICE,""));
        mMyEditedAddress.post(new Runnable() {
            @Override
            public void run() {
                int posAdd = mMyEditedAddress.getText().length();
                mMyEditedAddress.setSelection(posAdd);
                int posAddOfc = mMyEditedAddressOfc.getText().length();
                mMyEditedAddressOfc.setSelection(posAddOfc);
            }
        });
        mSaveAddressBtn = (Button) findViewById(R.id.btn_save_address);

        mSaveAddressBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppUtils.hideSoftKeyboard(mMyEditedAddress);
                doSaveAddress();
            }
        });

        mLoading = (RelativeLayout) findViewById(R.id.ll_loading_my_address);
        mAnchor = (FrameLayout) findViewById(R.id.my_address_anchor);
    }

    CustomPostRequest jsonReqSaveAddress;
    private void doSaveAddress() {
        jsonReqSaveAddress = new CustomPostRequest(Request.Method.POST,
                WAppConstants.URL_DO_MY_SAVE_ADDRESS, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "Save Address Response: " + response.toString());
                mLoading.setVisibility(View.GONE);
                if (response != null) {
                    try {
                        String status = response.getString("status");
                        if(status.equals("success")){
                            Snackbar.make(mAnchor,"Address saved", Snackbar.LENGTH_LONG).show();
                            finish();
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        }else{
                            Snackbar.make(mAnchor,"Something went wrong!", Snackbar.LENGTH_LONG).show();
                            mSaveAddressBtn.setEnabled(true);
                        }
                    } catch (JSONException e) {e.printStackTrace();}
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mSaveAddressBtn.setEnabled(true);
                mLoading.setVisibility(View.GONE);

                Log.e(TAG, "Error: " + error.getMessage());
                Snackbar.make(mAnchor,"Something went wrong!", Snackbar.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", mPrefs.getString(WAppConstants.LOGGED_IN_USER_ID, ""));
                params.put("address", mMyEditedAddress.getText()+"".trim());
                params.put("second_address", mMyEditedAddressOfc.getText()+"".trim());
                mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_ADDRESS, mMyEditedAddress.getText()+"".trim());
                mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_ADDRESS_OFFICE, mMyEditedAddressOfc.getText()+"".trim());
                mPrefEditor.commit();
                params.put("sec_code", mPrefs.getString(WAppConstants.LOGGED_IN_USER_SEC_CODE, ""));
                return params;
            }
        };
        if(AppUtils.isNetworkAvailable(MyAddressActivity.this)){
            WAppController.getInstance().addToRequestQueue(jsonReqSaveAddress);
            mLoading.setVisibility(View.VISIBLE);
        }else{
            Snackbar mSnackbar = Snackbar.make(mAnchor, R.string.no_connection, Snackbar.LENGTH_LONG);
            mSnackbar.setAction("Retry", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    WAppController.getInstance().addToRequestQueue(jsonReqSaveAddress);
                    mLoading.setVisibility(View.VISIBLE);
                }
            });
            mSnackbar.setActionTextColor(Color.RED);
            mSnackbar.show();
        }
    }

    @Override
    public void onBackPressed() {
        AppUtils.hideSoftKeyboard(mMyEditedAddress);
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    @Override
    public void onDestroy(){
        AppUtils.hideSoftKeyboard(mMyEditedAddress);
        super.onDestroy();
    }
}
