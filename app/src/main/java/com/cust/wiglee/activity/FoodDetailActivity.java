package com.cust.wiglee.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.cust.wiglee.R;
import com.cust.wiglee.app.WAppConstants;
import com.cust.wiglee.app.WAppController;
import com.cust.wiglee.modals.FoodItemDetail;
import com.cust.wiglee.modals.FoodItemDetailOption;
import com.cust.wiglee.modals.FoodOptionAnswer;
import com.cust.wiglee.modals.FoodOptionQuestion;
import com.cust.wiglee.volley.CustomPostRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FoodDetailActivity extends AppCompatActivity implements OnClickListener{
    private TextView mTvCartBadge;
    private int mCartCount =0;
    private LinearLayout foodOptionLLayout;
    private RadioGroup foodOptionGroup;
    private String foodItemID;
    private SharedPreferences mPrefs;
    private String TAG = WAppConstants.APP_TAG;
    private LinearLayout mLoading;
    private FoodItemDetail foodItemDetail;
    private TextView foodItemName;
    private LinearLayout foodOptionQuestionAnswerLLOut;
    private List<FoodOptionQuestion> foodOptionQuestionList;
    private RelativeLayout relativeLayoutForParams;

   // private List<FoodItemDetail> fooditemDetailList = new ArrayList<>();
    //private FoodDetailAdapter foodDetailAdaptor;
   // private RecyclerView foodListDetailRecylerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_detail);
        Toolbar foodDetailtoolbar = (Toolbar) findViewById(R.id.food_detail_toolbar);
        setSupportActionBar(foodDetailtoolbar);
        Bundle bundle = getIntent().getExtras();
        foodItemName = (TextView) findViewById(R.id.food_name);
        if (foodDetailtoolbar != null) {
            foodDetailtoolbar.getMenu().clear();
            foodDetailtoolbar.setTitle("");
            setSupportActionBar(foodDetailtoolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            TextView mTvTitle = (TextView) foodDetailtoolbar.findViewById(R.id.food_title);
            mTvTitle.setText(bundle.getString("foodName"));
            RelativeLayout mIvCart = (RelativeLayout) foodDetailtoolbar.findViewById(R.id.toolbar_cart);
            mIvCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent myCartIntent = new Intent(FoodDetailActivity.this, MyCartActivity.class);
                    FoodDetailActivity.this.startActivity(myCartIntent);
                    FoodDetailActivity.this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });
            mTvCartBadge = (TextView) foodDetailtoolbar.findViewById(R.id.tv_cart_count);


            foodDetailtoolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
        relativeLayoutForParams = (RelativeLayout) findViewById(R.id.food_image_title_layout);
        foodItemID = bundle.getString("fooditemID");
        foodItemName.setText(bundle.getString("foodName"));
        foodItemDetail = new FoodItemDetail();

        foodOptionLLayout = (LinearLayout) findViewById(R.id.food_options_llayout);
        foodOptionQuestionAnswerLLOut =  (LinearLayout) findViewById(R.id.food_option_question_amswer_layout);
        foodOptionGroup = new RadioGroup(this);
        foodOptionQuestionList = new ArrayList<>();
        /**
         * Code to implement Recyler View
         */
        /*foodListDetailRecylerView = (RecyclerView) findViewById(R.id.food_option_recycler_view);
        foodDetailAdaptor = new FoodDetailAdapter(fooditemDetailList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        foodListDetailRecylerView.setLayoutManager(mLayoutManager);
        foodListDetailRecylerView.setItemAnimator(new DefaultItemAnimator());
        foodListDetailRecylerView.setAdapter(foodDetailAdaptor);*/
        getFoodItemDetail(false);
    }


    private void createRadioButton(List<FoodItemDetailOption> foodItemOptions) {
        final RadioButton[] rb = new RadioButton[foodItemOptions.size()];

        Log.i("foodOption",""+foodItemOptions.size());
        foodOptionGroup.setOrientation(RadioGroup.VERTICAL);//or RadioGroup.VERTICAL

        for(int i=0; i<foodItemOptions.size(); i++){
            rb[i]  = new RadioButton(this);
            FoodItemDetailOption foodOption = foodItemOptions.get(i);


            rb[i].setText(foodOption.getFoodOptionName());
            rb[i].setId(Integer.parseInt(foodOption.getFoodOptionid()));
            rb[i].setOnClickListener(this);
            foodOptionGroup.addView(rb[i]);
        }
        foodOptionLLayout.addView(foodOptionGroup);//you add the whole RadioGroup to the layout

    }

    private void getFoodItemDetail(final boolean isRefreshReq){
        CustomPostRequest jsonReq = new CustomPostRequest(Request.Method.POST,
                WAppConstants.URL_MEAL_DETAILS, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Log.i("mealOption",""+response.toString());
                Log.d(TAG, "GET MEAL DETAILS " + response.toString());
                if (response != null) {
                    Log.i("success","Yesy");
                    parseFoodItemDetailsResponse(response);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error",""+error);
                if(!isRefreshReq){
                   // mLoading.setVisibility(View.GONE);
//                    mEmptyErrMsg.setVisibility(View.VISIBLE);
//                    mNoConnIcon.setVisibility(View.VISIBLE);
//                    mEmptyErrMsgTv.setText(getResources().getString(R.string.no_conn_retry));
                }else{
                    //Snackbar.make("", "Failed to refresh", Snackbar.LENGTH_SHORT).show();
                    //Snackbar.
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("user_id", mPrefs.getString(WAppConstants.LOGGED_IN_USER_ID,"1"));
//                params.put("sec_code", mPrefs.getString(WAppConstants.LOGGED_IN_USER_SEC_CODE, ""));
//                params.put("meal_type","n");
                params.put("user_id", "78");
                params.put("sec_code","ecd5c7fffd40c33035bec8cc4ec7febe");
                params.put("meal_id",foodItemID);
                return params;
            }
        };
        WAppController.getInstance().addToRequestQueue(jsonReq);
    }

    @Override
    public void onClick(View v) {
        int selectedId = foodOptionGroup .getCheckedRadioButtonId();

        // find the radio button by returned id
        RadioButton radioButton = (RadioButton) findViewById(selectedId);

        getFoodOptionQuestionList(false);

        Toast.makeText(FoodDetailActivity.this,
                radioButton.getText(), Toast.LENGTH_SHORT).show();
    }

   /* private void showQuestionPopUp(){

        if (foodOptionQuestionList == null){
            foodOptionQuestionList = new ArrayList<>();

        }
        else if (foodOptionQuestionList.size() > 0){
            foodOptionQuestionList.clear();
        }
        Log.i("in popup","yess");
        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.food_option_question_popup, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.editTextDialogUserInput);*/

        // set dialog message
       /* alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // get user input and set it to result
                                // edit text
                               // result.setText(userInput.getText());
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

        getFoodOptionQuestionList(false);


    }*/

    private void getFoodOptionQuestionList(final boolean isRefreshReq){
        CustomPostRequest jsonReq = new CustomPostRequest(Request.Method.POST,
                WAppConstants.URL_GET_MEAL_QUESTION_LIST, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Log.i("mealOption",""+response.toString());
                Log.d(TAG, "GET MEAL QUESTION LIST " + response.toString());
                if (response != null) {
                    Log.i("success","Yesy");
                    parseFoodOptionQuestionList(response);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error",""+error);
                if(!isRefreshReq){
                    // mLoading.setVisibility(View.GONE);
//                    mEmptyErrMsg.setVisibility(View.VISIBLE);
//                    mNoConnIcon.setVisibility(View.VISIBLE);
//                    mEmptyErrMsgTv.setText(getResources().getString(R.string.no_conn_retry));
                }else{
                    //Snackbar.make("", "Failed to refresh", Snackbar.LENGTH_SHORT).show();
                    //Snackbar.
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("user_id", mPrefs.getString(WAppConstants.LOGGED_IN_USER_ID,"1"));
//                params.put("sec_code", mPrefs.getString(WAppConstants.LOGGED_IN_USER_SEC_CODE, ""));
//                params.put("meal_type","n");
                params.put("user_id", "78");
                params.put("sec_code","ecd5c7fffd40c33035bec8cc4ec7febe");
                params.put("meal_id","38");
                params.put("meal_option_id","1");
                return params;
            }
        };
        WAppController.getInstance().addToRequestQueue(jsonReq);

    }

    private void parseFoodOptionQuestionList(JSONObject response){
        try {
            JSONArray postJsonArray = response.getJSONArray("data");
            if(postJsonArray.length()>0) {
                for (int i = 0; i < postJsonArray.length(); i++) {
                    JSONObject foodquestion = (JSONObject) postJsonArray.get(i);
                    String foodQuestionID = foodquestion.getString("meal_questions_id");
                    String foodQuestionText = foodquestion.getString("meal_questions_text");
                    FoodOptionQuestion foodOptionObject = new FoodOptionQuestion(foodQuestionID,foodQuestionText);
                    List<FoodOptionAnswer> foodanswerList = new ArrayList<>();
                    JSONArray foodQuestionAnswerArray = foodquestion.getJSONArray("answerlist");
                    for (int j = 0; j < foodQuestionAnswerArray.length(); j++) {
                        JSONObject foodquestionAnswer = (JSONObject) foodQuestionAnswerArray.get(i);
                        FoodOptionAnswer foodOptionAnswerObject = new FoodOptionAnswer(foodquestionAnswer.getString("answer_id"),foodquestionAnswer.getString("answer_text"),foodquestionAnswer.getString("meal_price"));
                        foodanswerList.add(foodOptionAnswerObject);
                    }
                    foodOptionObject.setFoodOptionAnswer(foodanswerList);
                    foodOptionQuestionList.add(foodOptionObject);

                }
            }
            createUserInterfaceForFoodOptionQuestionAndAnswer();

        }catch(JSONException e){
            e.printStackTrace();

        }


    }

    private void createUserInterfaceForFoodOptionQuestionAndAnswer() {
        // LayoutParams ll = (LinearLayout.LayoutParams) foodOptionQuestionAnswerLLOut.getLayoutParams();
        if (foodOptionQuestionAnswerLLOut.getChildCount() > 0) {
            foodOptionQuestionAnswerLLOut.removeAllViews();
        }
        TextView foodOptionQuestiontext [] = new TextView[foodOptionQuestionList.size()];
        int foodQuestionANswerSize = 1;
        int count = 0;
        for (int i = 0; i<foodOptionQuestionList.size(); i++){
            FoodOptionQuestion questionAnswer = foodOptionQuestionList.get(i);
            for (int j = 0;j<questionAnswer.getFoodOptionAnswer().size();j++){
                foodQuestionANswerSize++;
            }
        }

        RelativeLayout foodOptionANswerRLLayout [] = new RelativeLayout[foodQuestionANswerSize];
        TextView foodOptionAnswerPrice [] = new TextView[foodQuestionANswerSize];

        for (int i = 0; i < foodOptionQuestionList.size(); i++) {
            FoodOptionQuestion questionAnswer = foodOptionQuestionList.get(i);

         foodOptionQuestiontext[i] = new TextView(this);
        foodOptionQuestiontext[i].setText(questionAnswer.getFoodOptionquestionText());
        //foodOptionQuestiontext.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 0.1f));
        foodOptionQuestiontext[i].setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        foodOptionQuestiontext[i].setPadding(10, 0, 10, 0);
        foodOptionQuestiontext[i].setId(Integer.parseInt("100"));

            foodOptionQuestionAnswerLLOut.addView(foodOptionQuestiontext[i]);

        for (int j=0;j<questionAnswer.getFoodOptionAnswer().size();j++){
            FoodOptionAnswer foodOptionAnswerObj = questionAnswer.getFoodOptionAnswer().get(j);
            //Code for Relative layout
            foodOptionANswerRLLayout[count] = new RelativeLayout(this);
            // RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)relativeLayoutForParams.getLayoutParams();
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

            //  params.setMargins(-10,0,-10,0);
            foodOptionANswerRLLayout[count].setLayoutParams(params);

            foodOptionAnswerPrice[count] = new TextView(this);
            foodOptionAnswerPrice[count].setId(Integer.parseInt("102"));
            // RelativeLayout.LayoutParams foodpricellparams = (RelativeLayout.LayoutParams)relativeLayoutForParams.getLayoutParams();
            RelativeLayout.LayoutParams foodpricellparams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

            foodpricellparams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            foodpricellparams.addRule(RelativeLayout.CENTER_IN_PARENT);
            foodOptionAnswerPrice[count].setPadding(5, 0, 15, 0);

            foodOptionAnswerPrice[count].setLayoutParams(foodpricellparams);
            foodOptionAnswerPrice[count].setText("$100");
            foodOptionANswerRLLayout[count].addView(foodOptionAnswerPrice[i]);

            RadioGroup foodQuestionAnswerGroup = new RadioGroup(this);
            foodQuestionAnswerGroup.setOrientation(RadioGroup.VERTICAL);
            RadioButton b1 = new RadioButton(this);
            //RelativeLayout.LayoutParams foodanswerRadioButtonllparams = (RelativeLayout.LayoutParams)relativeLayoutForParams.getLayoutParams();
            RelativeLayout.LayoutParams foodanswerRadioButtonllparams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

            foodanswerRadioButtonllparams.addRule(RelativeLayout.LEFT_OF, foodOptionAnswerPrice[i].getId());
            b1.setLayoutParams(foodanswerRadioButtonllparams);
            b1.setText("This is radio button");
            b1.setId(Integer.parseInt("103"));
            foodQuestionAnswerGroup.addView(b1);
            foodOptionANswerRLLayout[count].addView(foodQuestionAnswerGroup);
            foodOptionQuestionAnswerLLOut.addView(foodOptionANswerRLLayout[count]);
            count++;
        }













    }
       // showQuestionPopUp();

       /* View linearLayout =  findViewById(R.id.info);
        //LinearLayout layout = (LinearLayout) findViewById(R.id.info);

        TextView valueTV = new TextView(this);
        valueTV.setText("hallo hallo");
        valueTV.setId(5);
        valueTV.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT));

        ((LinearLayout) linearLayout).addView(valueTV);*/



    }


    private void parseFoodItemDetailsResponse(JSONObject response){

        try{
            JSONObject responseDataObject = response.getJSONObject("data");
            JSONArray foodOptionArray = responseDataObject.getJSONArray("mealoption");

            List<FoodItemDetailOption> foodOptionList = new ArrayList<FoodItemDetailOption>();
            foodItemDetail.setFoodItemId(responseDataObject.getString("meal_id"));
            foodItemDetail.setFoodItemName(responseDataObject.getString("meal_name"));
            foodItemDetail.setFoodItemDetails(responseDataObject.getString("meal_description"));
            foodItemDetail.setFoodType(responseDataObject.getString("meal_type"));
            foodItemDetail.setFoodPrice(responseDataObject.getString("meal_price"));
            for (int i = 0; i < foodOptionArray.length(); i++) {
                JSONObject feedObj = (JSONObject) foodOptionArray.get(i);
                FoodItemDetailOption foodOption = new FoodItemDetailOption(feedObj.getString("meal_option"),feedObj.getString("meal_option_id"));
                foodOptionList.add(foodOption);
            }
            foodItemDetail.setFoodOptions(foodOptionList);
            createRadioButton(foodOptionList);


        }catch(JSONException e){
            e.printStackTrace();
        }
    }
}
