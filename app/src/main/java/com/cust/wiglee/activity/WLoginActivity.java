package com.cust.wiglee.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.cust.wiglee.R;
import com.cust.wiglee.app.AppUtils;
import com.cust.wiglee.app.WAppConstants;
import com.cust.wiglee.app.WAppController;
import com.cust.wiglee.view.common.font.CustomTypefaceSpan;
import com.cust.wiglee.view.indicators.AVLoadingIndicatorView;
import com.cust.wiglee.volley.CustomPostRequest;
import com.greenhalolabs.emailautocompletetextview.EmailAutoCompleteTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class WLoginActivity extends AppCompatActivity {
    private static final String TAG = WAppConstants.APP_TAG;
    private SharedPreferences mPrefs;
    private SharedPreferences.Editor mPrefEditor;
    private AVLoadingIndicatorView mLoading;
    private Button mLogIn;
    private LinearLayout mLogInForm;
    private EmailAutoCompleteTextView mLogInEmail;
    private AppCompatEditText mLogInPwd;
    private TextView mTvFrgtPwd;
    //private TextView mTvDontHaveAccount;
    private Button msignupButton;
    private SweetAlertDialog mSADialog;
    static boolean mActive = false;
    private RelativeLayout mAnchor;
    private ScrollView mBody;
    private boolean isSavedUser = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wlogin);
        mPrefs = WAppController.getPreferences(this);
        mPrefEditor  = WAppController.getPreferences(this).edit();

        if((mPrefs.getString(WAppConstants.LOGGED_IN_USER_ID,null))!=null &&
                (mPrefs.getString(WAppConstants.LOGGED_IN_USER_SEC_CODE,null)!=null)){
            if(mPrefs.getBoolean(WAppConstants.IS_USER_VALIDATED, false)){
                isSavedUser = true;
                //mHandler.sendEmptyMessageDelayed(MSG_CALL_HOME, DELAY);
                startHomeActivity();
            }else{
                finish();
                Intent validationIntent = new Intent(this, EmailValidationActivity.class);
                this.startActivity(validationIntent);
                this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        }

        mBody = (ScrollView) findViewById(R.id.scroll);
        if(!isSavedUser){
            mBody.setVisibility(View.VISIBLE);
        }
        mAnchor = (RelativeLayout) findViewById(R.id.login_root);
        TextView mTitleTv = (TextView) findViewById(R.id.login_title);
        SpannableStringBuilder sb = new SpannableStringBuilder("wiglee");
        sb.setSpan(new CustomTypefaceSpan(this, "Roboto-Bold.ttf"), 0, sb.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        sb.setSpan(new ForegroundColorSpan(Color.parseColor("#ffffff")), 0, sb.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        sb.setSpan(new AbsoluteSizeSpan((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 42,
                this.getResources().getDisplayMetrics())), 0, sb.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mTitleTv.setText(sb);
        mLogInForm = (LinearLayout) findViewById(R.id.form_layout);
        mLoading = (AVLoadingIndicatorView) findViewById(R.id.avl_login);
        mLogInEmail = (EmailAutoCompleteTextView) findViewById(R.id.eact_user_name);
        mLogInPwd = (AppCompatEditText) findViewById(R.id.et_password);
        mLogInPwd.setOnEditorActionListener(new AppCompatEditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO|| EditorInfo.IME_ACTION_UNSPECIFIED == actionId) {
                    if(validate()){
                        AppUtils.hideSoftKeyboard(mLogInEmail);
                        mLoading.setVisibility(View.VISIBLE);
                        mLogInForm.setVisibility(View.INVISIBLE);
                        mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_ID, null);
                        mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_NAME, null);
                        mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_EMAIL, null);
                        mPrefEditor.putBoolean(WAppConstants.IS_USER_VALIDATED, false);
                        mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_SEC_CODE, "");
                        mPrefEditor.commit();
                        doLogin();
                    }
                    return true;
                }
                return false;
            }
        });
        mLogIn = (Button) findViewById(R.id.btn_log_in);
        mLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validate()){
                    AppUtils.hideSoftKeyboard(mLogInEmail);
                    mLoading.setVisibility(View.VISIBLE);
                    mLogInForm.setVisibility(View.INVISIBLE);
                    mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_ID, null);
                    mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_NAME, null);
                    mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_EMAIL, null);
                    mPrefEditor.putBoolean(WAppConstants.IS_USER_VALIDATED, false);
                    mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_SEC_CODE, "");
                    mPrefEditor.commit();
                    doLogin();
                }
            }
        });
        mTvFrgtPwd = (TextView) findViewById(R.id.tv_frgt_pwd);
        SpannableString ssFrgtPwd = new SpannableString("Forgot your password?");
        ClickableSpan clickableSpanFrgtPwd = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                mTvFrgtPwd.clearFocus();
                mLogInPwd.requestFocus();
                getWindow().getDecorView().clearFocus();
                startForgetPwdActivity();
            }
        };
        ssFrgtPwd.setSpan(new ForegroundColorSpan(Color.parseColor("#FFFFFF")), 10, 21, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        ssFrgtPwd.setSpan(clickableSpanFrgtPwd, 0, 21, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        mTvFrgtPwd.setText(ssFrgtPwd);
        mTvFrgtPwd.setMovementMethod(LinkMovementMethod.getInstance());
        //Code for go to signup screen when sign up button clicked
        msignupButton = (Button)findViewById(R.id.btn_signup);
        msignupButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                finish();
                Intent signupIntent = new Intent(WLoginActivity.this, WSignUpActivity.class);
                startActivity(signupIntent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
        /*mTvDontHaveAccount = (TextView) findViewById(R.id.tv_dont_have_acc);
        SpannableString ss = new SpannableString("Don't have an account? Sign Up ");
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                finish();
                Intent signupIntent = new Intent(WLoginActivity.this, WSignUpActivity.class);
                startActivity(signupIntent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        };
        ss.setSpan(new ForegroundColorSpan(Color.parseColor("#5bb85d")), 22, 30, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(clickableSpan, 22, 30, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 22, 30, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        mTvDontHaveAccount.setText(ss);
        mTvDontHaveAccount.setMovementMethod(LinkMovementMethod.getInstance());*/
    }

    private boolean validate() {
        boolean valid = true;
        String eMail = mLogInEmail.getText()+"";
        if (TextUtils.isEmpty(eMail)) {
            Snackbar.make(mAnchor,R.string.user_id_valid,Snackbar.LENGTH_LONG).show();
            valid = false;
            return valid;
        } else {
            if(!android.util.Patterns.EMAIL_ADDRESS.matcher(eMail).matches()){
                Snackbar.make(mAnchor,R.string.sign_up_email_err_wrong,Snackbar.LENGTH_LONG).show();
                valid = false;
                return valid;
            }
        }
        String password = mLogInPwd.getText()+"";
        if (TextUtils.isEmpty(password)) {
            Snackbar.make(mAnchor,R.string.pwd_valid,Snackbar.LENGTH_LONG).show();
            valid = false;
            return valid;
        } else {
            int pwdLength = password.length();
            if(!(pwdLength > 5 && pwdLength < 13)){
                Snackbar.make(mAnchor,R.string.sign_up_pwd_err_wrong,Snackbar.LENGTH_LONG).show();
                valid = false;
                return valid;
            }
        }
        return valid;
    }

    private void doLogin() {
        //mUserStoredEmail = mPrefs.getString(AppConstants.LOGGED_IN_USER_EMAIL, null);
        //mUserStoredPassword = mPrefs.getString(AppConstants.LOGGED_IN_USER_PASSWORD, null);
        CustomPostRequest jsonReqUseLogin = new CustomPostRequest(Request.Method.POST,
                WAppConstants.URL_DO_LOGIN, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "Login Response: " + response.toString());
                if (response != null) {
                    try {
                        String loginStatus = response.getString("login_status");
                        String msg = null;
                        if(loginStatus.trim().equals("1") || loginStatus.trim().equals("2")){
                            JSONObject userObj = response.getJSONObject("user_details");
                            String userId = userObj.getString("user_id");
                            //String userEmail = userObj.getString("username");
                            String userName = userObj.getString("fullname");
                            String secCode = userObj.getString("user_sec_string");
                            Log.d(TAG, "Login Response secToken: " + secCode);
                            mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_ID, userId);
                            mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_NAME, userName);
                            //mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_EMAIL, userEmail);
                            mPrefEditor.putString(WAppConstants.LOGGED_IN_USER_SEC_CODE, secCode);
                            mPrefEditor.commit();
                            if(loginStatus.trim().contains("1")){
                                mPrefEditor.putBoolean(WAppConstants.IS_USER_VALIDATED, true);
                                mPrefEditor.commit();
                                mHandler.sendEmptyMessageDelayed(MSG_CALL_HOME, DELAY);
                            }else{
                                Log.d(TAG, "Login Response: call home " + loginStatus);
                                startEmailvalActivity();
                                mLoading.setVisibility(View.INVISIBLE);
                                mLogInForm.setVisibility(View.VISIBLE);
                            }
                        }else if(loginStatus.equals("0")){
                            mTvFrgtPwd.setVisibility(View.VISIBLE);
                            msg = "Wrong username or password";
                            Snackbar mSnackbar = Snackbar.make(mAnchor, msg, Snackbar.LENGTH_LONG);
                            mSnackbar.show();
                        }else if(loginStatus.equals("3")){
                            msg = "Sending verification code failed. ";
                            Snackbar mSnackbar = Snackbar.make(mAnchor,msg, Snackbar.LENGTH_LONG);
                            mSnackbar.setAction("Resend", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    reSendVerifiCode();
                                }
                            });
                            mSnackbar.setActionTextColor(Color.RED);
                            mSnackbar.show();
                        }else{
                            msg = "Something went wrong. Try again later.";
                            Snackbar mSnackbar = Snackbar.make(mAnchor, msg, Snackbar.LENGTH_LONG);
                            mSnackbar.show();
                        }
                        mLoading.setVisibility(View.INVISIBLE);
                        mLogInForm.setVisibility(View.VISIBLE);
                    } catch (JSONException e) {e.printStackTrace();}
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(mActive){
                    Log.e(TAG, "Login Error" + error);
                    Log.e(TAG, "Error: " + error.getMessage());
                    if(!(error instanceof NoConnectionError)){
                        mLoading.setVisibility(View.INVISIBLE);
                        mLogInForm.setVisibility(View.VISIBLE);
                        try{
                            mSADialog = new SweetAlertDialog(WLoginActivity.this, SweetAlertDialog.ERROR_TYPE);
                            mSADialog.setTitleText("Opss..");
                            mSADialog.setContentText("Something went wrong. Please try again later.");
                            mSADialog.setConfirmText("Retry");
                            mSADialog.setCancelable(false);
                            mSADialog.showCancelButton(true);
                            mSADialog.setCancelText("Close");
                            mSADialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    mSADialog.dismiss();
                                    //finish();
                                }
                            });
                            mSADialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                                @Override
                                public boolean onKey (DialogInterface dialog, int keyCode, KeyEvent event) {
                                    if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP && !event.isCanceled()) {
                                        mSADialog.dismiss();
                                        finish();
                                        return true;
                                    }
                                    return false;
                                }
                            });
                            mSADialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    mLoading.setVisibility(View.VISIBLE);
                                    mLogInForm.setVisibility(View.INVISIBLE);
                                    sDialog.dismiss();
                                    doLogin();
                                }
                            }).show();
                        }catch(Exception e){e.printStackTrace();}
                    }else{
                        mLoading.setVisibility(View.INVISIBLE);
                        mLogInForm.setVisibility(View.VISIBLE);
                        mSADialog = new SweetAlertDialog(WLoginActivity.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
                        mSADialog.setCustomImage(R.drawable.ic_no_conn);
                        mSADialog.setTitleText("No connection.");
                        mSADialog.setContentText("No connection available. Please try again later.");
                        mSADialog.setConfirmText("Retry");
                        mSADialog.setCancelable(false);
                        mSADialog.showCancelButton(true);
                        mSADialog.setCancelText("Close");
                        mSADialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                mSADialog.dismiss();
                                //finish();
                            }
                        });
                        mSADialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                            @Override
                            public boolean onKey (DialogInterface dialog, int keyCode, KeyEvent event) {
                                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP && !event.isCanceled()) {
                                    mSADialog.dismiss();
                                    finish();
                                    return true;
                                }
                                return false;
                            }
                        });
                        mSADialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                mLoading.setVisibility(View.VISIBLE);
                                mLogInForm.setVisibility(View.INVISIBLE);
                                sDialog.dismiss();
                                doLogin();
                            }
                        }).show();
                    }
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", mLogInEmail.getText()+"");
                params.put("password", mLogInPwd.getText()+"");
                return params;
            }
        };
        if(AppUtils.isNetworkAvailable(this)){
            WAppController.getInstance().addToRequestQueue(jsonReqUseLogin);
        }else{
            mLoading.setVisibility(View.INVISIBLE);
            mLogInForm.setVisibility(View.VISIBLE);
            mSADialog = new SweetAlertDialog(WLoginActivity.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
            mSADialog.setCustomImage(R.drawable.ic_no_conn);
            mSADialog.setTitleText("No connection.");
            mSADialog.setContentText("No connection available. Please try again later.");
            mSADialog.setConfirmText("Retry");
            mSADialog.setCancelable(false);
            mSADialog.showCancelButton(true);
            mSADialog.setCancelText("Close");
            mSADialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sDialog) {
                    mSADialog.dismiss();
                    //finish();
                }
            });
            mSADialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey (DialogInterface dialog, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP && !event.isCanceled()) {
                        mSADialog.dismiss();
                        finish();
                        return true;
                    }
                    return false;
                }
            });
            mSADialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sDialog) {
                    mLoading.setVisibility(View.VISIBLE);
                    sDialog.dismiss();
                    doLogin();
                }
            }).show();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mActive = true;
    }
    @Override
    public void onStop(){
        mActive = false;
        super.onStop();
    }
    @Override
    public void onDestroy(){
        mHandler.removeMessages(MSG_CALL_HOME);
        if(mSADialog != null)
            mSADialog.dismiss();
        super.onDestroy();
    }
    @Override
    public void onBackPressed(){
        mHandler.removeMessages(MSG_CALL_HOME);
        if(mSADialog!=null){
            if(mSADialog.isShowing()){
                mSADialog.dismiss();
                finish();
            }
        }
        overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
        super.onBackPressed();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("KEY_USER_ID", mLogInEmail.getText()+"");
        outState.putString("KEY_USER_PWD", mLogInPwd.getText()+"");
        mActive = false;
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mActive = true;
    }
private final static int MSG_CALL_HOME= 1001;
private final static long DELAY = 0;
static class LoginHandler extends Handler {
    WeakReference<WLoginActivity> mLoginAct;
    LoginHandler(WLoginActivity aLoginAct) {
        mLoginAct = new WeakReference<WLoginActivity>(aLoginAct);
    }
    @Override
    public void handleMessage(Message message) {
        WLoginActivity localActivity = mLoginAct.get();
        switch (message.what) {
            case MSG_CALL_HOME:
                if(localActivity!=null)
                    localActivity.startHomeActivity();
                break;
        }
    }
}
LoginHandler mHandler = new LoginHandler(WLoginActivity.this);
    public void startHomeActivity(){
        WLoginActivity.this.runOnUiThread(new Runnable() {
            public void run() {
                finish();
                Intent startGlobalActivity = new Intent(WLoginActivity.this, WHomeActivity.class);
                startGlobalActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(startGlobalActivity);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
    }
    private void startEmailvalActivity() {
        finish();
        Intent validationIntent = new Intent(this, EmailValidationActivity.class);
        this.startActivity(validationIntent);
        this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
    private void startForgetPwdActivity() {
        Intent forgetPwdIntent = new Intent(this, ForgetPwdActivity.class);
        forgetPwdIntent.putExtra("email",mLogInEmail.getText()+"");
        this.startActivity(forgetPwdIntent);
        this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
    private void reSendVerifiCode() {


    }
}