package com.cust.wiglee.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.android.volley.toolbox.ImageLoader;
import com.cust.wiglee.R;
import com.cust.wiglee.app.ImmersiveUtil;
import com.cust.wiglee.app.WAppController;
import com.cust.wiglee.view.common.PullBackLayout;
import com.cust.wiglee.view.common.ZoomImageView;

public class ProfilePicActivity extends AppCompatActivity implements PullBackLayout.Callback{
    private String mFeedImageUrl;
    private LinearLayout mLoadingLayout;
    private PullBackLayout mPullBackLayout;
    private RelativeLayout mRootLayout;
    private ColorDrawable background;
    private boolean playExitAnim;
    private ImageLoader mImageLoader;
    private ZoomImageView mPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            getWindow().setStatusBarColor(Color.parseColor("#000000"));
        }
        super.onCreate(savedInstanceState);
        playExitAnim = false;
        Intent intent = getIntent();
        mFeedImageUrl = intent.getStringExtra("image_url");
        playExitAnim = intent.getBooleanExtra("play_exit_anim", true);
        setContentView(R.layout.activity_profile_pic);
        mRootLayout = (RelativeLayout)findViewById(R.id.rl_prof_pic_root);
        if (Build.VERSION.SDK_INT >= 16){
            background = new ColorDrawable(Color.BLACK);
            mRootLayout.setBackground(background);
        }else{
            mRootLayout.setBackgroundColor(Color.BLACK);
        }
        mPullBackLayout = (PullBackLayout)findViewById(R.id.puller_prof_pic);
        mPullBackLayout.setCallback(this);
        mLoadingLayout = (LinearLayout) findViewById(R.id.ll_puller_prof_pic_loading);
        mImageLoader = WAppController.getInstance().getImageLoader();
        mPhoto = (ZoomImageView)findViewById(R.id.ziv_prof_pic);
        mPhoto.setImageUrl(mFeedImageUrl, mImageLoader);
        mPhoto.setResponseObserver(new ZoomImageView.ResponseObserver() {
            @Override
            public void onError() {
                mPhoto.setImageUrl(mFeedImageUrl, mImageLoader);
            }
            @Override
            public void onSuccess() {
            }
        });
        mPhoto.setMaxScale(10);
        mPhoto.setDoubleTapZoomScale(5);
    }
    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onBackPressed()	{
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if(playExitAnim){
                ActivityCompat.finishAfterTransition(this);
            }else{
                finish();
                overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
            }
        }else{
            finish();
            overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
        }
    }
    @Override
    public void onPullComplete() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if(playExitAnim){
                ActivityCompat.finishAfterTransition(this);
            }else{
                finish();
                overridePendingTransition(0,R.anim.activity_slide_down);
            }
        }else{
            finish();
            overridePendingTransition(0,R.anim.activity_slide_down);
        }
    }
    @Override
    public void onPullStart() {
        fadeOut();
        showSystemUi();
    }
    @Override
    public void onPull(float progress) {
        progress = Math.min(1f, progress * 3f);
        mLoadingLayout.setAlpha(1f -progress);
        if (Build.VERSION.SDK_INT >= 16){
            background.setAlpha((int) (0xff * (1f - progress)));
        }else{
            mRootLayout.setAlpha(1f-progress);
        }
    }
    @Override
    public void onPullCancel() {
        fadeIn();
    }
    void fadeIn() {
        showSystemUi();
    }
    void fadeOut() {
        hideSystemUi();
    }
    @Override
    public void supportFinishAfterTransition() {
        showSystemUi();
        super.supportFinishAfterTransition();
    }
    private void showSystemUi() {
        ImmersiveUtil.exit(mRootLayout);
    }
    private void hideSystemUi() {
        ImmersiveUtil.enter(mRootLayout);
    }
}
