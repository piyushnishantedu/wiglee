package com.cust.wiglee.listner;

import com.cust.wiglee.modals.FoodItemModal;

/**
 * Created by piyushnishant on 20/09/16.
 */
public interface FoodItemClickistener {
    void onfoodItemClick(int position,FoodItemModal foodItemObject);

}
